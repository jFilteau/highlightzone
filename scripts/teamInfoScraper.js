import * as $ from "jquery";
import { TEAM_IDS } from "../constants/teamIds.js";
import { PLAYER_POSITIONS } from "../constants/playerPositions.js";


class TeamPageScraper {
    getPitchersTable(){
        var table = $(".tablewrap[data-pos-type=\"P\"]"); 
        return table.length === 0 ? false : table;
    }
    
    getHittersTable(){
        var table = $(".tablewrap[data-pos-type=\"B\"]"); 
        return table.length === 0 ? false : table;
    }

    getPlayerHtmlElementsWhoAreHitters(){
        var hitter_html_elements;
        var hitter_table = this.getHittersTable();

        if (!hitter_table){
            return false;
        }
       
        hitter_html_elements = $(hitter_table).find("div.ysf-player-name");

        return hitter_html_elements.length === 0 ? false : hitter_html_elements;
    }

    getPlayerHtmlElementsWhoArePitchers(){
        var pitcher_html_elements;
        var pitcher_table = this.getPitchersTable();

        if (!pitcher_table){
            return false;
        }
       
        pitcher_html_elements = $(pitcher_table).find("div.ysf-player-name");

        return pitcher_html_elements.length === 0 ? false : pitcher_html_elements;
    }

    getPlayerElementRequiredData(player, position){
        var player_id = this.getPlayerIdFromElement(player);
        var player_name = this.getPlayerNameFromElement(player);
        var team_name_abbreviation = this.getPlayerTeamNameAbbreviationFromElement(player);
        var team_api_id = this.getTeamMlbApiId(team_name_abbreviation);

        return (!player_id || !player_name || !team_api_id || !position) ? false :
            { player_id : player_id, data: { team_id: team_api_id, name: player_name, position: position } };
    }

    generateRequiredDataObjectForAllPlayerElements(elements, position){
        var self = this;
        var players = {};

        if (!(elements instanceof $)){
            return players;
        }

        $(elements).each((index, element) => {
            let required_data = self.getPlayerElementRequiredData(element, position);

            if (required_data){
                players[required_data["player_id"]] = required_data["data"];
            }
        });

        return players;
    }


    getPlayersOnFantasyTeam(){
        var combined_pitcher_and_hitter_data;
        var required_data_object_for_pitchers;
        var required_data_object_for_hitters;
        var hitters = this.getPlayerHtmlElementsWhoAreHitters();
        var pitchers = this.getPlayerHtmlElementsWhoArePitchers();

        if (!(hitters && pitchers)){
            return false;
        }

        required_data_object_for_hitters = this.generateRequiredDataObjectForAllPlayerElements(hitters, PLAYER_POSITIONS.hitter);
        required_data_object_for_pitchers = this.generateRequiredDataObjectForAllPlayerElements(pitchers, PLAYER_POSITIONS.pitcher);
        combined_pitcher_and_hitter_data = Object.assign(required_data_object_for_hitters, required_data_object_for_pitchers);

        return Object.keys(combined_pitcher_and_hitter_data).length === 0 ? false : combined_pitcher_and_hitter_data;
    }

    getPlayerNameFromElement(element){
        var player_name_link_element;
        var player_name_link_element_text_value;

        player_name_link_element = $(element).find("a");
        if (player_name_link_element.length === 0){
            return false;
        }

        player_name_link_element_text_value = $(player_name_link_element).text().trim();

        return this.validatePlayerName(player_name_link_element_text_value);
    }

    getPlayerIdFromElement(element){
        var player_id;
        var player_name_link_element;
        var player_name_link_element_href_value;

        player_name_link_element = $(element).find("a");
        if (player_name_link_element.length === 0){
            return false;
        }

        player_name_link_element_href_value = $(player_name_link_element).attr("href"); 
        if (!player_name_link_element_href_value){
            return false;
        }

        // The player's yahoo id in the last element of the href link that links to their yahoo page
        player_id = player_name_link_element_href_value.split("/").pop().trim();

        return player_id.length === 0 ? false : player_id;
    }

    getPlayerTeamNameAbbreviationFromElement(element){
        var team_name_abbreviation_text;
        var team_name_abbreviation_html_element = $(element).find("span");

        if (team_name_abbreviation_html_element.length === 0){
            return false;
        }

        team_name_abbreviation_text = $(team_name_abbreviation_html_element).text().split("-")[0].trim();

        // The team name is followed by the player's position separated by a dash
        // (e.g. Bos - SP). We only want to keep the team abbreviation.
        return team_name_abbreviation_text.length === 0 ? false : team_name_abbreviation_text;
    }

    getTeamMlbApiId(abbreviation){
        var uppercased_team_abbreviation;

        if (!abbreviation || typeof abbreviation !== "string"){
            return false;
        }
        
        uppercased_team_abbreviation = abbreviation.toUpperCase();

        return !(uppercased_team_abbreviation in TEAM_IDS) ? false : TEAM_IDS[uppercased_team_abbreviation];
    }

    getTeamIdOfPlayer(player){
        // Team id must exist and it must be a number
        if (!player || !("team_id" in player) || !player.team_id || isNaN(parseInt(player.team_id))){
            return false;
        }

        return parseInt(player.team_id);
    }

    getTeamIdsOfPlayersOnFantasyTeam(players){
        // Create a set so that if two players are on the same team, their team's id value will only show up once.
        // Showing up more than once would make us fetch the same game from the api more than once, which would be a waste
        var ids = new Set();

        if (!players || !(typeof players === "object") || !(players instanceof Object) || Array.isArray(players)){
            return false;
        }

        for (let player_id in players){
            if ("team_id" in players[player_id]){
                let team_id = this.getTeamIdOfPlayer(players[player_id]);

                if (team_id){
                    ids.add(team_id);
                } 
            }
        }

        return ids.size === 0 ? false : ids;
    }

    validatePlayerName(player_name){
        if (!player_name || typeof(player_name) != "string"){
            return false;
        }

        // This is a special case for players who are both pitchers and hitters in Yahoo!
        // These players have the values (pitcher) and (hitter) next to their names which we want to remove.
        player_name = player_name.split("(")[0];
        player_name = player_name.trim();

        return player_name.length === 0 ? false : player_name;
    }
}

export { TeamPageScraper };
