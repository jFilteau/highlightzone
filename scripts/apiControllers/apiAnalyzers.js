import { Validators } from "./apiDataValidators.js";
import { HIGHLIGHT_TYPES } from "../../constants/highlightTypes.js";
import { PLAYER_POSITIONS } from "../../constants/playerPositions.js";


class Analyzers {
    constructor(){
        this.validators = new Validators();
    }

    getHighlightTypeFromHighlightTexts(description, headline, position){
        var homerun_regex_pattern   = /(?:home(?:r| run)|grand slam|back-to-back|dinger|\bhr\b|\bblasts\b)/g;
        var hitting_regex_pattern   = /(?:\bsingles?\b|\bdoubles?\b|\btriples?\b|\bscores?\b|\brbi\b|\bsteals?\b)/g;
        var pitching_regex_pattern  = /(?:\bk's\b|strikes? ?outs?|\bshutout\b|striking out|\bsave\b)/g;
        var defense_regex_pattern   = /(?:defens(?:e|ive)|(?:bare|back)hand|div(?:e|ing)|throws? (?:out|to)|\bcatch(es)?\b|\bgrab\b|double play|\bfires\b|\bnabs\b)/g;
        var interview_regex_pattern = /(?:\bdiscuss(es)?\b|\btalks?\b|\bannounce(?:s|ment)?\b|\bupdates?\b|\breflects?\b)/g;
        var daily_recap_pattern     = / *daily +recap/g;

        // Make it lower case so that we don't have to worry about uppercase values not matching
        description = typeof description === "string" ? description.toLowerCase() : false;
        headline    = typeof headline    === "string" ? headline.toLowerCase()    : false;

        switch (true){
            // We'll check for the daily recap pattern first since it trumps other patterns.
            // e.g. If the description is "Daily recap: Sale discusses his shutout performance", the pitching, interview
            // and daily recap patterns match, but the daily recap case is the real highlight type.
            case daily_recap_pattern.test(description) || daily_recap_pattern.test(headline):
                return false;

            // We'll check for the interview pattern before hitting, pitching and defense since it trumps them.
            // e.g. If the description is "Sale discusses his shutout performance", the pitching
            // and interview patterns match, but the interview case is the real highlight type 
            case interview_regex_pattern.test(description) || interview_regex_pattern.test(headline):
                return HIGHLIGHT_TYPES.interview;

            // Check for a defensive match first since it trumps the other two types if both match.
            // e.g. The description "Trout makes the nice catch to save a run" matches defense (via the word catch) and
            // pitching (via the word save), but we can see that it's a defensive play
            case defense_regex_pattern.test(description) || defense_regex_pattern.test(headline):
                return HIGHLIGHT_TYPES.defense;

            case hitting_regex_pattern.test(description) || hitting_regex_pattern.test(headline):
                // A hitting pattern can only belong to a position player, i.e. we don't care about what pichers do on offense
                // for fantasy purposes
                return position !== PLAYER_POSITIONS.hitter ? false : HIGHLIGHT_TYPES.hitting;

            case homerun_regex_pattern.test(description) || homerun_regex_pattern.test(headline):
                // A homerun pattern can only belong to a position player
                return position !== PLAYER_POSITIONS.hitter ? false : HIGHLIGHT_TYPES.homerun;

            case pitching_regex_pattern.test(description) || pitching_regex_pattern.test(headline):
                // A pitching pattern can only belong to a pitcher player, i.e. we don't care about what position players do when
                // they have to pitch for fantasy purposes
                return position !== PLAYER_POSITIONS.pitcher ? false : HIGHLIGHT_TYPES.pitching;

            // The default is when no pattern matched, meaning we can't determine the type of highlight it is.
            default:
                return false;
        }
    }

    normalizeString(str){
        // We want to lowercase all letters in case the uppercase letters don't match (e.g. McCutchen and Mccutchen)
        var _str = str.trim().toLowerCase();
        return _str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    highlightDescriptionTextsContainPlayerName(headline, description, name){
        if (!headline || !description || !name){
            return false;
        }

        // Very important to normalize the strings before creating the regex since many players have accents in their names.
        var normalized_full_name   = this.normalizeString(name);
        var normalized_headline    = this.normalizeString(headline);
        var normalized_description = this.normalizeString(description);
        var last_name_regex        = RegExp(normalized_full_name.substring(normalized_full_name.lastIndexOf(" ") + 1), "g");
        var full_name_regex        = RegExp(normalized_full_name, "g");

        // Important to check for the last name in the headline and full name in the description in case other players
        // have the same name.
        // (e.g. The headline "Bauers makes a beautiful diving catch" would match both Trevor Bauer and Jake Bauers, so
        // we have to check the description where the full name will very likely be used, allowing us to know if the highlight
        // belongs to Jake or Trevor)
        return last_name_regex.test(normalized_headline) && full_name_regex.test(normalized_description);
    }

    highlightTeamIdsContainsPlayerTeamId(highlight_team_ids, team_id){
        if ( !team_id || !highlight_team_ids || !Array.isArray(highlight_team_ids)){
            return false;
        }

        return highlight_team_ids.includes(team_id);
    }

    highlightBelongsToPlayer(highlight, teams, player){
        if (!this.validators.objectHasRequiredKeys(highlight, ["headline", "description", "keywordsAll"]) ||
            !this.validators.objectHasRequiredKeys(player, ["team_id", "name"])){
            return false;
        }

        var keywords_list_associated_player = this.playerNameAssociatedToHighlightKeywordsList(highlight);

        // This case means the list is usable to determine the player associated to the highlight
        if (keywords_list_associated_player){
            return this.highlightTeamIdsContainsPlayerTeamId(teams, player["team_id"]) &&
                this.normalizeString(player["name"]) === keywords_list_associated_player;
        }

        // The player's team must be involved in the game and their name must be in the headline.
        // We're using the headline instead of description to validate the highlight since the
        // player's name can be found in the description without it being their own highlight.
        // (e.g. Headline -> Nelson Cruz hits a double
        //       Description -> Nelson Cruz hits a double, scoring Haniger from third
        // 
        //       This highlight should only belong to Cruz and not Haniger also)
        return this.highlightTeamIdsContainsPlayerTeamId(teams, player["team_id"]) &&
            this.highlightDescriptionTextsContainPlayerName(highlight["headline"], highlight["description"], player["name"]);
    }

    playerNameAssociatedToHighlightKeywordsList(highlight){
        var keywords_list = !(highlight instanceof Object) || !("keywordsAll" in highlight) ? false : highlight["keywordsAll"];

        if (!keywords_list || !Array.isArray(keywords_list) || keywords_list.length === 0){
            return false;
        }

        for (let i = 0; i < keywords_list.length; i++){
            if (keywords_list[i]["type"] === "player_id"){
                return !("displayName" in keywords_list[i]) ? false : this.normalizeString(keywords_list[i]["displayName"]);
            } 
        }

        return false;
    }
}

export { Analyzers };
