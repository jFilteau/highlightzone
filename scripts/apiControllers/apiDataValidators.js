class Validators {
    objectHasRequiredKeys(object, keys){
        if (
            !keys || !object || !Array.isArray(keys) || !(typeof object === "object") ||
            !(object instanceof Object) || Array.isArray(object) || keys.length === 0
        ){
            return false;
        }

        // All keys must exist in the object
        return keys.every(k => { return k in object; });
    }

    gameHasFantasyPlayerTeamInvolved(team_ids, game){
        if (
            !team_ids || !game || !(typeof game === "object") || !(game instanceof Object) ||
            !(team_ids instanceof Set) || !("teams" in game) || !Array.isArray(game.teams) || team_ids.length === 0
        ){
            return false;
        }

        // If any player on the fantasy team was part of the game
        return game.teams.some(t => { return team_ids.has(t); });
    }

    scheduleDataIsValid(data){
        return  !this.objectHasRequiredKeys(data, ["totalGames", "dates"]) ||
                data.totalGames === 0 ||
                !Array.isArray(data.dates) ||
                data.dates.length === 0 ||
                !this.objectHasRequiredKeys(data.dates[0], ["games"]) ? false : true;
    }

    gameHasHighlightVideos(game){
        return  !this.objectHasRequiredKeys(game, ["summary"]) || 
                !this.objectHasRequiredKeys(game.summary, ["hasHighlightsVideo"]) ||
                game.summary.hasHighlightsVideo === false ? false : true;
    }

    gameStateIsValid(game){
        var game_state;
        var game_state_info;

        if (
            !this.objectHasRequiredKeys(game, ["status"]) ||
            !this.objectHasRequiredKeys(game["status"], ["abstractGameCode", "codedGameState"])
        ){
            return false;
        }

        // Tells us if the game is finalized or live
        game_state = typeof game["status"]["abstractGameCode"] !== "string" ? false :
            game["status"]["abstractGameCode"].trim().toLowerCase();

        // Gives us additional info about the game's state, e.g. if finalized game was due to rescheduling, if the live
        // game is currently in a coaching challenge, etc
        game_state_info = typeof game["status"]["codedGameState"] !== "string" ? false :
            game["status"]["codedGameState"].trim().toLowerCase();

        //However, finalized games must have the game state info set to f, meaning that
        // the game was finalized
        return game_state === "l" ? true :  // Live games are always valid.
            game_state !== "f" ? false :    // Only finalized games are checked again under
                !["f", "o"].includes(game_state_info) ? false : true; // Game was completed in normal circumstances (e.g. not postponed)
    }
}

export { Validators };
