import * as $ from "jquery";
import moment from "moment";
import { ApiManipulator } from "./apiManipulation.js";
import { DataFilters } from "./apiDataFilters.js";
import { TeamPageScraper } from "../teamInfoScraper.js";


class ApiInitializer {
    constructor(players, callback){
        this.players = players;
        this.send_response = callback;
        this.api = new ApiManipulator();
        this.filters = new DataFilters();
        this.team_page_scraper = new TeamPageScraper();

        // Set the team ids upon creation of the object
        this.team_ids_of_team_players = this.team_page_scraper.getTeamIdsOfPlayersOnFantasyTeam(players);
    }

    createAllDatesForTheCurrentWeek(){
        // Important that the format be MM/DD/YYY since that's what's used in the API url param
        var date_format = "MM/DD/YYYY";

        var now = moment();    
        var weekday_number = now.isoWeekday();
        var dates = [{ weekday_number: weekday_number, date: now.format(date_format) }];
        
        // We're subtracting one from the weekday since today was already appended to the array above
        --weekday_number;

        // Monday is 1 and Sunday is 7, so we'll decrement until 0 to get all previous days
        while (weekday_number > 0){
            dates.push({ weekday_number: weekday_number, date:  now.subtract(1, "days").format(date_format) });
            --weekday_number;
        }

        return dates;
    }

    createGameDataAjaxRequestsForAllGamesFromScheduleResponse(response){
        var games_data;
        var games_with_fantasy_players_involved;

        if (!Array.isArray(response) || response.length < 2 || response[1] !== "success"){
            return false;
        }

        // Filter the schedule data received to only use the valid ones (e.g. if a game has no gamePk, then we discard it)
        games_data = this.filters.retrieveGamesDataFromScheduleResponse(response[0]);
        if (!games_data){
            return false;
        }

        games_with_fantasy_players_involved = this.filters.retrieveGamesWithFantasyPlayersInvolved(this.team_ids_of_team_players, games_data);

        return this.api.createGameDataRequests(games_with_fantasy_players_involved);
    }

    createAjaxRequestsForAllGamesFromScheduleResponses(responses){
        var game_data_requests_from_all_days = [];

        for (let i = 0; i < responses.length; i++){
            let game_data_requests = this.createGameDataAjaxRequestsForAllGamesFromScheduleResponse(responses[i]);

            if (game_data_requests){
                // Extending the request array to include all requests
                game_data_requests_from_all_days = game_data_requests_from_all_days.concat(game_data_requests);
            }
        }

        return game_data_requests_from_all_days.length === 0 ? false : game_data_requests_from_all_days;
    }

    retrieveHighlightVideoDataOfGameFromAjaxResponse(response){
        var highlight_videos;

        if (!response || !Array.isArray(response) || response.length < 2 || response[1] !== "success"){
            return false;
        }

        highlight_videos = this.filters.retrieveHighlightVideosFromResponseData(response[0]);

        return !highlight_videos ? false : highlight_videos; 
    }

    createLiveGameFeedAjaxRequestsFromRequiredData(data){
        var requests = [];

        if (!data || !(data instanceof Object) || Array.isArray(data)){
            return false;
        }

        for (let game_pk in data){
            let request = this.api.createLiveGameFeedDataAjaxRequest(game_pk, data[game_pk]);

            if (request){
                requests.push(request); 
            }
        }

        return requests.length === 0 ? false : requests;
    }

    retrieveHomerunMetricsFromResponseData(response){
        if (!response || response[1] !== "success" || !("homerun_metrics" in response[0])){
            return false;
        }

        return response[0]["homerun_metrics"];
    }

    fetchHomerunMetricsFromRequiredData(data, highlights_by_player){
        var self = this;
        var homerun_metrics = {};
        var live_feed_requests = this.createLiveGameFeedAjaxRequestsFromRequiredData(data);

        // If no requests are to be made, then send the response to the content script with the homerun
        // data empty
        if (!live_feed_requests){
            self.send_response({ highlights: highlights_by_player, homeruns: false });
        }

        $.when(...live_feed_requests).then(function (...responses){
            // Make sure to create an array of the responses if there was only one game to fetch since the response would be
            // an object instead of an array and we want to iterate over an array below.
            if (live_feed_requests.length === 1){
                responses = [responses];
            }

            for (let i = 0; i < responses.length; i++){
                let homerun_metrics_of_response = self.retrieveHomerunMetricsFromResponseData(responses[i]);

                if (homerun_metrics_of_response){
                    Object.assign(homerun_metrics, homerun_metrics_of_response);
                }
            }

            self.send_response({
                highlights: highlights_by_player,
                homeruns: Object.keys(homerun_metrics).length === 0 ? false : homerun_metrics
            });
        });
    }

    handleAjaxRequestsForAllGamesFromSchedule(requests){
        var self = this;
        var game_keys_data_to_fetch;
        var all_highlight_videos_data = [];
        var highlights_by_fantasy_team_players;

        if (!requests || !Array.isArray(requests) || requests.length === 0){
            // Make sure to tell the content script that nothing was retrieved
            self.send_response({ highlights: false, homeruns: false });
        }

        $.when(...requests).then(function (...game_data_responses){
            // Make sure to create an array of the responses if there was only one day to fetch since in the function
            // retrieveHighlightVideoDataOfGameFromAjaxResponse() below iterates over an array.
            if (requests.length === 1){
                game_data_responses = [game_data_responses];
            }

            for (let i = 0; i < game_data_responses.length; i++){
                let highlight_video_data = self.retrieveHighlightVideoDataOfGameFromAjaxResponse(game_data_responses[i]);

                if (highlight_video_data){
                    all_highlight_videos_data.push(highlight_video_data);
                }
            }

            // Filter all the weekly highlights to those of the team's players
            highlights_by_fantasy_team_players = self.filters.filterHighlightVideosDataByPlayer(all_highlight_videos_data, self.players);

            // Check to see if there are any homerun metrics to fetch
            game_keys_data_to_fetch = self.filters.gameKeysToFetchForHomerunMetrics(highlights_by_fantasy_team_players);
            if (game_keys_data_to_fetch){
                self.fetchHomerunMetricsFromRequiredData(game_keys_data_to_fetch, highlights_by_fantasy_team_players);
                return;
            }

            // Send the data back to the content script
            self.send_response({ highlights: highlights_by_fantasy_team_players, homeruns: false });
        });
    }

    init(){
        var self = this;
        var current_week_dates_to_fetch_data_from = this.createAllDatesForTheCurrentWeek();
        var ajax_requests_of_all_games = current_week_dates_to_fetch_data_from.map(date => this.api.createScheduleDataAjaxRequest(date));

        $.when(...ajax_requests_of_all_games).then(function (...responses){
            // Make sure to create an array of the responses if there was only one day to fetch since in the function
            // createAjaxRequestsForAllGamesFromScheduleResponses() below it iterates over an array
            if (ajax_requests_of_all_games.length === 1){
                responses = [responses];
            }

            return self.createAjaxRequestsForAllGamesFromScheduleResponses(responses);

        }).then(function (requests){
            self.handleAjaxRequestsForAllGamesFromSchedule(requests);
        });
    }
}

export { ApiInitializer };
