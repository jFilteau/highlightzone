import * as $ from "jquery";
import { Validators } from "./apiDataValidators";


class ApiManipulator {
    constructor() {
        this.validators = new Validators();
        this.api_base_url = "https://statsapi.mlb.com/api";
        this.schedule_url_base = this.api_base_url + "/v1/schedule?sportId=1";
    }

    validateParameterPassedToAjaxCreationFunction(param, required_keys){
        var validate_key_value = (key) => [false, true, null, undefined].includes(param[key]) ? false : true;

        return param && typeof param === "object" && param instanceof Object && !Array.isArray(param) &&
            this.validators.objectHasRequiredKeys(param, required_keys) && required_keys.every(validate_key_value);
    }

    validateLiveFeedResponseData(data){
        return  this.validators.objectHasRequiredKeys(data, ["liveData"]) &&
                this.validators.objectHasRequiredKeys(data["liveData"], ["plays"]) &&
                this.validators.objectHasRequiredKeys(data["liveData"]["plays"], ["allPlays"]);
    }

    getHomerunMetrics(data, guids){
        var all_plays_data;
        var homerun_metrics = {};
        
        if (!this.validateLiveFeedResponseData(data)){
            return false;
        }

        all_plays_data = data["liveData"]["plays"]["allPlays"];

        for (let i = 0; i < all_plays_data.length; i++){
            if (!this.validators.objectHasRequiredKeys(all_plays_data[i], ["playEvents"])){
                continue;
            }

            // Play events array describes what happened on every pitch of a given at bat
            all_plays_data[i]["playEvents"].map((event_data) => {
                guids.map((guid) => {
                    // The id associated to an event (playId) is the same value as the id (guid) associated to a highlight in the game
                    // highlight content API. Therefore, if they both match, then the details associated to the event is that of the
                    // highlight video
                    return event_data["playId"] !== guid ? false :
                        !("hitData" in event_data) ? false : homerun_metrics[guid] = event_data["hitData"];
                });
            });
        }

        return Object.keys(homerun_metrics).length === 0 ? false : homerun_metrics;
    }

    createLiveGameFeedDataAjaxRequest(game_pk, guids){
        var self = this;

        if (!game_pk || !guids){
            return false;
        }

        return $.ajax({
            url: this.api_base_url + "/v1.1/game/" + game_pk + "/feed/live",
            dataType: "json",
            success: function (data){
                // Append the homerun data to the response
                data["homerun_metrics"] = self.getHomerunMetrics(data, guids);

                return data;
            }
        });
    }

    createScheduleDataAjaxRequest(date){
        if (!this.validateParameterPassedToAjaxCreationFunction(date, ["date", "weekday_number"])){
            return false;
        }

        return $.ajax({
            url: this.schedule_url_base + "&date=" + date["date"],
            dataType: "json",
            success: function (data){
                // Append the day number to the response data
                data.weekday_number = date["weekday_number"];

                return data;
            }
        });       
    }

    createGameDataAjaxRequest(game){
        if (!this.validateParameterPassedToAjaxCreationFunction(game, ["pk", "teams", "weekday_number"])){
            return false;
        }

        return $.ajax({
            url: this.api_base_url + "/v1/game/" + game.pk + "/content?language=en",
            dataType: "json",
            success: function (data){
                // The gamePk, teams and weekday number must be manually added since we'll need them later
                data.game_pk = game.pk;
                data.teams = game.teams;
                data.weekday_number = game.weekday_number;

                return data;
            }
        });
    }

    createGameDataRequests(games){
        var requests = [];

        // This happens if the team's of the players on the fantasy team had no games, thus there are no gamePk to fetch
        if (!games){
            return;
        }

        // Create the request for each game
        games.map(game => {
            let game_request = this.createGameDataAjaxRequest(game);

            if (game_request){
                requests.push(game_request);
            }
        });

        return requests.length > 0 ? requests : null;
    }
}

export { ApiManipulator };
