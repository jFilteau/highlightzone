import moment from "moment";
import { Validators } from "./apiDataValidators";
import { Analyzers } from "./apiAnalyzers.js";
import { HIGHLIGHT_TYPES } from "../../constants/highlightTypes.js";


class DataFilters {
    constructor(){
        this.validators = new Validators();
        this.analyzers = new Analyzers();
    }

    getPlayerIdInvolvedInHighlight(highlight, teams, players){
        for (let id in players){
            if (this.analyzers.highlightBelongsToPlayer(highlight, teams, players[id])){
                return id;
            }
        }

        // Means no player on the fantasy team was involved in the highlight
        return false;
    }

    updateHighlightsByPlayerObject(obj, player_id, highlight){
        var updated_obj = obj;

        if (player_id in obj){
            updated_obj[player_id].push(highlight);
        } else {
            updated_obj[player_id] = [highlight];
        } 

        return updated_obj;
    }

    updateHighlightsByPlayerObjectFromMultiplePlayersData(obj, data){
        var update_highlights_by_player = obj;

        if (!data){
            return update_highlights_by_player;
        }

        for (let id in data){
            let array_of_highlights = data[id];

            for (let i = 0; i < array_of_highlights.length; i++){
                update_highlights_by_player = this.updateHighlightsByPlayerObject(update_highlights_by_player, id, array_of_highlights[i]);
            }
        }

        return update_highlights_by_player;
    }

    getHighlightTypeOfVideo(video, player){
        return  this.validators.objectHasRequiredKeys(player, ["position"]) &&
                this.validators.objectHasRequiredKeys(video, ["headline", "description"]) &&
                this.analyzers.getHighlightTypeFromHighlightTexts(video["description"], video["headline"], player["position"]);
    }

    validateGameHighlightData(game){
        return  game instanceof Object && this.validators.objectHasRequiredKeys(game, ["highlights", "weekday_number"]) &&
                Array.isArray(game["highlights"]) && game["highlights"].length !== 0;
    }
    
    filterGameHighlightVideosByPlayer(game, players){
        var game_highlights_array;
        var highlights_by_player = [];

        if (!this.validateGameHighlightData(game)){
            return false;
        }

        game_highlights_array = game["highlights"];

        for (let i = 0; i < game_highlights_array.length; i++){
            let player_id_involved_in_highlight = this.getPlayerIdInvolvedInHighlight(game_highlights_array[i], game.teams, players);

            if (player_id_involved_in_highlight){
                let highlight_type = this.getHighlightTypeOfVideo(game_highlights_array[i], players[player_id_involved_in_highlight]);

                if (highlight_type){
                    game_highlights_array[i].weekday_number = game.weekday_number;
                    game_highlights_array[i].highlight_type = highlight_type;
                    game_highlights_array[i].game_pk = game.game_pk;
                    game_highlights_array[i].player_id = player_id_involved_in_highlight;

                    highlights_by_player.push(game_highlights_array[i]);
                }
            }
        }

        return highlights_by_player.length === 0 ? false : highlights_by_player;
    }

    highlightVideosDataIsValid(videos){
        return videos && Array.isArray(videos) && videos.length > 0;
    } 

    playerDataIsValid(players){
        return players && typeof players === "object" && players instanceof Object && !Array.isArray(players);
    } 

    filterHighlightVideosDataByPlayer(videos, players){
        var highlights_by_players = [];

        if (!this.highlightVideosDataIsValid(videos) || !this.playerDataIsValid(players)){
            return false;
        } 

        for (let i = 0; i < videos.length; i++){
            if (this.validators.objectHasRequiredKeys(videos[i], ["highlights", "game_pk"])){
                let multiple_player_data = this.filterGameHighlightVideosByPlayer(videos[i], players);

                if (multiple_player_data){
                    highlights_by_players.push(...multiple_player_data);
                }
            }
        }

        // Sort the highlights by timestamp value so that the most recent ones get displayed first
        highlights_by_players = this.sortHighlightElementsListByTimestampValueDescending(highlights_by_players);

        return (!highlights_by_players || highlights_by_players.length === 0) ? false : highlights_by_players;
    }

    retrieveGamesWithFantasyPlayersInvolved(team_ids, games){
        var filtered_games = [];

        if (!team_ids || !games || !Array.isArray(games)){
            return false;
        }

        for (let i = 0; i < games.length; i++){
            if (this.validators.gameHasFantasyPlayerTeamInvolved(team_ids, games[i])){
                filtered_games.push(games[i]);
            }
        }

        return filtered_games.length === 0 ? false : filtered_games;
    }

    retrieveGamesDataFromScheduleResponse(response){
        var games;
        var games_data = [];

        if (!this.validators.scheduleDataIsValid(response)){
            return false;
        }

        games = response.dates[0].games;

        for (let i = 0; i < games.length; i++){
            let game_data = games[i];

            if (this.validators.objectHasRequiredKeys(game_data, ["gamePk", "teams"]) && this.validators.gameStateIsValid(game_data)){
                games_data.push({
                    pk: game_data["gamePk"],
                    teams: this.getTeamIdsFromGameData(game_data["teams"]),
                    weekday_number: "weekday_number" in  response ? response["weekday_number"]: null
                });
            }
        }

        return games_data.length === 0 ? false : games_data;
    }

    retrieveVideoDataFromHighlightItem(item){
        return !this.validators.objectHasRequiredKeys(item, ["name", "url"]) ? false :
            !(item["name"] === "mp4Avc" && item["url"].length !== 0) ? false : item["url"];
    }

    gameDataIsValid(data){
        return  this.validators.objectHasRequiredKeys(data, ["team"]) &&
                this.validators.objectHasRequiredKeys(data["team"], ["id"]) &&
                !isNaN(parseInt(data["team"]["id"]));
    }

    getTeamIdsFromGameData(data){
        var ids = [];

        if (!(typeof data === "object") || !(data instanceof Object) || Array.isArray(data)){
            return false;
        }

        for (let key in data){
            if (this.gameDataIsValid(data[key])){
                ids.push(data[key]["team"]["id"]);
            }
        }

        return ids.length === 0 ? false : ids;
    }

    getHighlightImageCutResolutionHeight(cut){
        return (!("height" in cut) || isNaN(parseInt(cut.height))) ? false : cut.height;
    }

    getHighlightImageCutAspectRatio(cut){
        return !("aspectRatio" in cut) ? false : cut.aspectRatio.trim();
    }

    highlightItemImageDataIsValid(item){
        return  this.validators.objectHasRequiredKeys(item, ["image"]) &&
                this.validators.objectHasRequiredKeys(item.image, ["cuts"]) &&
                Array.isArray(item.image.cuts) &&
                item.image.cuts.length !== 0;
    }

    getHighlightImageLinkSourceToUseForHighlghtItem(item){
        if (!this.highlightItemImageDataIsValid(item)){
            return false;
        }

        for (let i = 0; i < item.image.cuts.length; i++){
            let highlight_item_height = this.getHighlightImageCutResolutionHeight(item.image.cuts[i]);
            let highlight_item_aspect_ratio = this.getHighlightImageCutAspectRatio(item.image.cuts[i]);

            if (highlight_item_height === 144 && highlight_item_aspect_ratio === "4:3"){
                return !("src" in item.image.cuts[i]) ? false : item.image.cuts[i]["src"];
            }
        }
        
        return false;
    }

    highlightVideoItemIsValid(item){
        return  typeof item === "object" && item instanceof Object && "type" in item && item.type === "video";
    }

    retrieveRequiredDataFromHighlightVideoItem(item){
        var headline;
        var video_url_link;

        if (!this.highlightVideoItemIsValid(item)){
            return false;
        }

        headline = this.getHighlightItemHeadline(item);
        video_url_link = this.getHighlightVideoItemUrl(item);

        // The headline is important for the user to know what exactly the highlight is about (can't be making them guess).
        // The video link is important since we can't display a video without them. 
        return !(headline && video_url_link) ? false : {
            headline: headline,
            url: video_url_link,
            description: this.getHighlightItemVideoDescription(item),
            image: this.getHighlightImageLinkSourceToUseForHighlghtItem(item),
            guid: this.getHighlightItemGuid(item),
            keywordsAll: this.getHighlightItemKeywordsList(item),
            date: this.convertDateValueToUnixTimestamp(item["date"])
        };
    }

    highlightItemVideoDataIsValid(item){
        return  this.validators.objectHasRequiredKeys(item, ["playbacks"]) &&
                Array.isArray(item.playbacks) &&
                item.playbacks.length !== 0;
    }

    getHighlightVideoItemUrl(item){
        if (!this.highlightItemVideoDataIsValid(item)){
            return false;
        }

        for (let i = 0; i < item.playbacks.length; i++){
            let video_highlight_url = this.retrieveVideoDataFromHighlightItem(item.playbacks[i]);

            if (video_highlight_url){
                return video_highlight_url;
            }
        }

        return false;
    }

    getHighlightItemHeadline(item){
        return (!this.validators.objectHasRequiredKeys(item, ["headline"]) || item.headline.trim().length === 0) ? false : item.headline.trim();
    }

    getHighlightItemVideoDescription(highlight){
        return !this.validators.objectHasRequiredKeys(highlight, ["description"]) ||
                highlight.description.trim().length === 0 ? false : highlight.description.trim();
    }

    getHighlightItemGuid(highlight){
        return !this.validators.objectHasRequiredKeys(highlight, ["guid"]) ||
                highlight["guid"].trim().length === 0 ? false : highlight["guid"];
    }

    getHighlightItemKeywordsList(highlight){
        return !this.validators.objectHasRequiredKeys(highlight, ["keywordsAll"]) ||
                highlight["keywordsAll"].length === 0 ? false : highlight["keywordsAll"];
    }

    retrieveDataFromHighlightVideosItemsList(list){
        var filtered_highlight_videos_data = [];

        if (!Array.isArray(list) || list.length === 0){
            return false;
        }

        for (let i = 0; i < list.length; i++){
            let highlight_video_required_data = this.retrieveRequiredDataFromHighlightVideoItem(list[i]);

            if (highlight_video_required_data){
                filtered_highlight_videos_data.push(highlight_video_required_data);
            }
        }

        return filtered_highlight_videos_data.length === 0 ? false : filtered_highlight_videos_data;
    }

    gameHighlightsDataIsValid(data){
        return  this.validators.gameHasHighlightVideos(data) &&
                this.validators.objectHasRequiredKeys(data, ["highlights"]) &&
                this.validators.objectHasRequiredKeys(data.highlights, ["highlights"]) &&
                this.validators.objectHasRequiredKeys(data.highlights.highlights, ["items"]) &&
                Array.isArray(data.highlights.highlights.items);
    }

    retrieveHighlightVideosFromResponseData(data){
        var highlight_videos_data;

        if (!this.gameHighlightsDataIsValid(data)){
            return false;
        }


        highlight_videos_data = this.retrieveDataFromHighlightVideosItemsList(data.highlights.highlights.items);

        return !highlight_videos_data ? false : {
            game_pk: data.game_pk,
            highlights: highlight_videos_data,
            teams: data.teams,
            weekday_number: data.weekday_number
        };
    }

    gameKeysToFetchForHomerunMetrics(highlights){
        // Key will be the game pk and values will be an array of guid values, i.e. play ids
        var game_keys_to_guid_values = {};

        if (!highlights || !Array.isArray(highlights) || highlights.length === 0){
            return false;
        }

        for (let i = 0; i < highlights.length; i++){
            let highlight = highlights[i];

            if (this.validators.objectHasRequiredKeys(highlight, ["highlight_type", "guid", "game_pk"]) &&
                highlight["highlight_type"] === HIGHLIGHT_TYPES.homerun
            ){
                let game_pk = highlights[i]["game_pk"];
                let guid = highlights[i]["guid"];

                if (!(game_pk in game_keys_to_guid_values)){
                    game_keys_to_guid_values[game_pk] = [guid];
                } else {
                    game_keys_to_guid_values[game_pk].push(guid);
                }
            }
        }

        return Object.keys(game_keys_to_guid_values).length === 0 ? false : game_keys_to_guid_values;
    }

    convertDateValueToUnixTimestamp(date){
        var timestamp_value = false;

        if (moment(date, moment.ISO_8601, true).isValid()){
            timestamp_value = Date.parse(date);
        }

        return (!timestamp_value || isNaN(timestamp_value)) ? null : timestamp_value;
    }

    sortHighlightElementsListByTimestampValueDescending(list){
        var sort_function = function (a, b){
            let a_timestamp_value = Number(a["date"]);
            let b_timestamp_value = Number(b["date"]);

            switch (true){
                case isNaN(a_timestamp_value):
                    return 1;
                case isNaN(b_timestamp_value):
                    return -1;
                default:
                    return b_timestamp_value - a_timestamp_value;
            }
        };

        return (!Array.isArray(list) || list.length === 0) ? false : list.sort(sort_function);
    }
}

export { DataFilters };
