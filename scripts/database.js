class IndexedDb {
    constructor(){
        this.db_name = "HighlightZoneDb";
        this.version = 1;
    }

    getDb(){
        return this.db;
    }

    setDb(db){
        this.db = db;
        this.db.onerror = function (event){
            console.error("Database error: " + event.target.errorCode);
        };
    }

    createDb(){
        var self = this;

        return new Promise(function (resolve){
            let request = window.indexedDB.open(self.db_name, self.version);

            request.onupgradeneeded = function (event){
                let store;
                let db = event.target.result;

                if (!db.objectStoreNames.contains("PlayerHighlights")){
                    store = db.createObjectStore("PlayerHighlights", { keyPath: "player_id" });

                    // Table columns
                    store.createIndex("player_id", "player_id", { unique: true });
                    store.createIndex("team_id", "team_id");
                    store.createIndex("full_name", "full_name");
                    store.createIndex("highlights", "highlights");
                } 
            };

            request.onsuccess = function (event){
                let db = event.target.result;
                self.setDb(db);

                return resolve(true);
            };
        });
    }

    createPlayersInTheDB(players){
        var tx;
        var store;

        var db = this.getDb();

        if (!db){
            console.error("Highlights database does not exist.");
            return;
        }
        
        if (!players){
            return;
        } 

        tx = db.transaction("PlayerHighlights", "readwrite");
        store = tx.objectStore("PlayerHighlights");

        for (let id in players){
            // Create or update player entry in the database
            store.put({ 
                player_id: parseInt(id),
                team_id: parseInt(players[id].team_id),
                full_name: players[id].name
            });
        }
    }

    init(){
        if (!("indexedDB" in window)) {
            console.error("This browser doesn\"t support IndexedDB");
            return;
        }

        //window.indexedDB.deleteDatabase(this.db_name);

        return this.createDb();
    }
}

export { IndexedDb };
