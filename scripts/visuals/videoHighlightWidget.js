import * as $ from "jquery";
import "magnific-popup";


class HighlightzoneInitButton {
    createInitButton(){
        var button = $("<div>Highlight Zone</div>").attr({
            href: "#highlight-zone-popup",
            id: "highlightzone-launch-button"
        });
        return button;
    }

    createCustomCloseButton(){
        return "<button title=\"%title%\" type=\"button\" class=\"mfp-close\" style=\"font-size: 60px;\">&#215;</button>";
    }

    setButtonEvents(button){
        var self = this;

        $(button).click(function (){
            $.magnificPopup.open({
                type: "inline",
                midClick: true,
                alignTop: true,
                closeBtnInside: false,
                closeMarkup: self.createCustomCloseButton(),
                items: {
                    src: "#highlightzone-links-container",
                },
                callbacks: {
                    beforeOpen: function (){
                        // No need to update the popup data if the class created exists, i.e. the data already exists
                        if ($("#highlightzone-links-container").hasClass("created")){
                            return;
                        }
                    }
                }
            });
        });
    }

    displayHeaderNoHighlights(){
        $("#highlightzone-header-no-highlights").addClass("header-visible");
    }

    hideHeaderNoHighlights(){
        $("#highlightzone-header-no-highlights").removeClass("header-visible");
    }

    init(){
        var button;
        var target_location_to_append_on_page = $("#yspmaincontent #team-roster > header");

        if (target_location_to_append_on_page.length === 0){
            console.error("Highlightzone - Grid table element where widget is to be inserted was not found.")
            return;
        }
        
        button = this.createInitButton();
        this.setButtonEvents(button);

        // Appending to the page as the second child of the grid table
        $(target_location_to_append_on_page).after(button);
    }
}

export { HighlightzoneInitButton };
