import { HighlightzoneInitButton } from "./videoHighlightWidget";
import { MainPopup } from "./popups/mainPopup.js";
import { VideoPopup } from "./popups/videoPopup.js";
import { HighlightLinksPopup } from "./popups/highlightLinksPopup.js";


function visualInitialize(api_response_data, api_homerun_data, players){
    var main_popup = new MainPopup();
    var video_popup = new VideoPopup();
    var init_button = new HighlightzoneInitButton();
    var highlight_links_popup = new HighlightLinksPopup(players, api_homerun_data, api_response_data);

    // Creating the elements on the page
    main_popup.init(video_popup.init(), highlight_links_popup.init());

    // Creating the containers that will contain all the popup data (hidden until called upon)
    init_button.init();
}

export { visualInitialize };
