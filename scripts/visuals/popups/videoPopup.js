class VideoPopup {
    createMainContainer(){
        var container = document.createElement("div");
        container.setAttribute("id", "highlightzone-video-container");
        container.setAttribute("class", "mfp-hide");
        container.style.margin = "20px auto";
        container.style.position = "relative";
        return container;
    }

    init(){
        return this.createMainContainer();
    }
}

export { VideoPopup };
