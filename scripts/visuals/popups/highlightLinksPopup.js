import * as $ from "jquery";
import { MainPopup } from "./mainPopup.js";
import { HIGHLIGHT_TYPES } from "../../../constants/highlightTypes.js";
import { PLAYER_POSITIONS } from "../../../constants/playerPositions.js";


class HighlightLinksPopup extends MainPopup {
    constructor(players, api_homerun_data, api_response_data){
        super(players, api_homerun_data, api_response_data);
    }

    createMainContainer(){
        var container = $("<div></div>");
        $(container).attr({ class: "mfp-hide", id: "highlightzone-links-container" });
        return container;
    }

    createDropdownContainer(id){
        var container = $("<div></div>");  
        $(container).attr({ class: "highlightzone-dropdown-container", id: id });
        return container;
    }

    createDropdownContentContainer(id){
        var container = $("<div></div>");  
        $(container).attr({ class: "highlightzone-dropdown-content", id: id });
        $(container).css("width", "250px");
        return container;
    }

    createDropdownButton(id, text){
        var button = $("<button>" + text + "</button>");  
        $(button).attr({ class: "highlightzone-dropdown-button", id: id, value: "all" });
        return button;
    }

    createPlayerDropdownOption(id, name){
        var player_option = $("<a></a>");
        $(player_option).attr({ href: "#", "player-id": id });

        // No player specified means the default one of "All players"
        $(player_option).text(name ? name : "All players");

        this.setFilterOptionClickEvent(player_option, "player", "highlightzone-player-dropdown-button");

        return player_option;
    }

    createPlayersObjectWithNamesAsKeys(){
        var player_names_to_id = {};

        if (!this.players || Object.keys(this.players).length === 0){
            return false;
        }

        // Generate a new dict with the player names as the key instead of their IDs
        for (var player_id in this.players){
            if (this.players[player_id] instanceof Object && "name" in this.players[player_id]){
                player_names_to_id[this.players[player_id]["name"]] = player_id;
            }
        }

        return Object.keys(player_names_to_id).length === 0 ? false : player_names_to_id;
    }

    createPlayersDropdownMenu(){
        var container = this.createDropdownContainer("highlightzone-player-dropdown-container");
        var dropdown = this.createDropdownContentContainer("highlightzone-player-dropdown-content"); 
        var button = this.createDropdownButton("highlightzone-player-dropdown-button", "Select player");
        var player_object_with_names_as_keys = this.createPlayersObjectWithNamesAsKeys();

        if (!player_object_with_names_as_keys){
            return false;
        }

        // The first value is the default one of "All Players"
        $(dropdown).append(this.createPlayerDropdownOption("all"));

        // Sort the names and append them alphabetically so that it's easier to search for a player in the dropdown
        Object.keys(player_object_with_names_as_keys).sort().map((name) => {
            $(dropdown).append(this.createPlayerDropdownOption(player_object_with_names_as_keys[name], name));
        });

        $(container).append(button, dropdown);

        this.setDropdownButtonEvent(button, "highlightzone-player-dropdown-content");

        return container;
    }

    createNoHighlightsFoundHeaderContainer(){
        var container = $("<div>No highlights available.</div>");
        $(container).attr("id", "highlightzone-header-no-highlights");
        $(container).css("display", "none");
        
        // Make the header visible if there were no highlights
        if (!this.apiResponseDataIsValid()){
            $(container).addClass("highlightzone-header-visible");
        }

        return container;
    }

    validateHighlightLinkElementParamaters(highlight){
        var required_keys = ["url", "weekday_number", "headline", "image", "highlight_type", "player_id"];
        return this.validators.objectHasRequiredKeys(highlight, required_keys) &&
            typeof highlight["url"] === "string" && highlight["url"].length !== 0;
    }

    validateThatTheHighlightShouldBeDisplayedBasedOnTheType(highlight){
        var player_id = highlight["player_id"];
        var highlight_type = highlight["highlight_type"];
        var player_position = "position" in this.players[player_id] ? this.players[player_id]["position"] : false;

        if (!player_position || !highlight_type){
            return false;
        }

        switch (highlight_type){
            // Make sure that hitting highlights are associated to a hitter. For fantasy purposes, we don't care about hitting
            // highlights of pitchers
            case HIGHLIGHT_TYPES.hitting:
                return player_position === PLAYER_POSITIONS.hitter;

            // Make sure that homerun highlights are associated to a hitter.
            case HIGHLIGHT_TYPES.homerun:
                return player_position === PLAYER_POSITIONS.hitter;

            // Make sure that pitching highlights are associated to a pitcher since it's possible that a pitching highlight gets
            // associated to a hitter, in which case we want to discard it. 
            // (e.g. If the highlight is "Sale strikes out Trout" and associated to our player Trout, clearly we don't want to
            //  display our player getting K'd)
            case HIGHLIGHT_TYPES.pitching:
                return player_position === PLAYER_POSITIONS.pitcher;

            // Other highlight types can be associated to both hitters and pitchers
            case HIGHLIGHT_TYPES.defense:
            case HIGHLIGHT_TYPES.interview:
                return true;

            default:
                return false;
        }
    }
    
    appendHomerunBannerToContainer(container, highlight){
        var banner;
        var updated_container = container;
        var distance_rounded_up = Math.ceil(this.homeruns[highlight["guid"]]["totalDistance"]);

        if (isNaN(distance_rounded_up)){
            return updated_container;
        }

        banner = $("<div><span>" + distance_rounded_up.toString() + "  feet</span></div>").addClass("highlightzone-homerun-banner");
        return $(updated_container).append(banner);
    }

    createHighlightLinkElement(highlight){
        var main_container;
        var highlight_picture;
        var highlight_headline;
        var highlight_types_that_should_be_displayed_by_default = [HIGHLIGHT_TYPES.pitching, HIGHLIGHT_TYPES.hitting, HIGHLIGHT_TYPES.homerun];

        if (!this.validateHighlightLinkElementParamaters(highlight) ||
            !this.validateThatTheHighlightShouldBeDisplayedBasedOnTheType(highlight)){
            return false;
        }

        main_container = $("<div></div>").css("cursor", "pointer").attr({
            "class": "highlightzone-link-element-container",
            "player-id": highlight["player_id"],
            "href": highlight["url"],
            "weekday-id": highlight["weekday_number"],
            "highlight-type-id": highlight["highlight_type"],
            "date-timestamp": highlight["date"]
        });
        highlight_headline = $("<div>" + highlight["headline"] + "</div>").addClass("hightlightzone-highlight-headline");
        highlight_picture = $("<img></img>").attr({ height: 144, src: highlight["image"] , alt: highlight["headline"] });

        if (!highlight_types_that_should_be_displayed_by_default.includes(highlight["highlight_type"])){
            $(main_container).css("display", "none");        
        }

        this.setHighlightElementEvents(main_container);

        // Adding a corner banner displaying the distance in feet of homerun highlights
        if (highlight["highlight_type"] === HIGHLIGHT_TYPES.homerun && this.validators.objectHasRequiredKeys(highlight, ["guid"])){
            if (this.validators.objectHasRequiredKeys(this.homeruns[highlight["guid"]], ["totalDistance"])){
                main_container = this.appendHomerunBannerToContainer(main_container, highlight);
            }
        }

        $(main_container).append(highlight_picture, highlight_headline);

        return main_container;
    }

    createHighlightLinksContainer(){
        var container = $("<div></div>").attr("id", "highlightzone-links");
        return this.updateHighlightLinksContainerWithResponseData(container);
    }

    handleDropdownSelectedOptionOfTypePlayer(button_id, selected_option){
        var button = typeof button_id !== "string" ? false : button_id.trim().length === 0 ? false : $("#" + button_id.trim());

        var player_id = typeof selected_option !== "object" || selected_option === null ? false :
            selected_option.nodeType !== Node.ELEMENT_NODE ? false : $(selected_option).attr("player-id");

        if (button && player_id && player_id !== undefined && button.length !== 0){
            // The dropdown button text and value attribute gets updated with those of the selected player
            $(button).text($(selected_option).text());
            $(button).attr("value", player_id);
        }

        if ($(selected_option).parent().length !== 0){
            // Reset the sroll upon selection (important to set this before we hide the dropdown bellow)
            $(selected_option).parent()[0].scrollTop = 0;

            // Hide the dropdown opon selection
            $(selected_option).parent().removeClass("highlightzone-show-dropdown-menu");
        }

        this.displayHighlightsOfDropdownSelectedPlayerId(player_id);
    }

    handleToggleSwitchSelectedOption(selected_option){
        var selected_option_value = $(selected_option).attr("value");

        $(selected_option).addClass("selected");
        $(selected_option).siblings().removeClass("selected");

        this.displayHighlightsOfDropdownSelectedDateRangeId(selected_option_value);
    }

    handleHighlightTypeToggleSwitchSelectedOption(option){
        var player_button = $("#highlightzone-player-dropdown-button");

        // Retrieving the value attribute for each button
        var player_button_value = $(player_button).length === 0 ? false :
            !player_button[0].hasAttribute("value") ? false : $(player_button).attr("value");

        if (!option || option === null || option.nodeType !== Node.ELEMENT_NODE || !player_button_value){
            return false;
        }

        this.displaySpecificHighlightElements({
            player_button_value: player_button_value,
            additional_highlight_type_values: $(".highlightzone-toggle-input:checked")
        });
    }

    setFilterOptionClickEvent(option, type, button_id = false){
        var self = this;

        if (!option){
            return false;
        }

        $(option).click(function (){
            switch (type){
                case "player":
                    self.handleDropdownSelectedOptionOfTypePlayer(button_id, this);
                    return false; // Returning false to avoid the scrollbar from going to top upon exiting the popup
                case "highlight-type":
                    self.handleHighlightTypeToggleSwitchSelectedOption(this);
                    break;
                default:
                    break;
            }
        });
    }

    setDropdownButtonEvent(button, id){
        $(button).click(function (){
            document.getElementById(id).classList.toggle("highlightzone-show-dropdown-menu");
        });
    }

    setHighlightElementEvents(element){
        var self = this;

        $(element).click(function (e){
            e.stopPropagation();

            let element_href_link;

            // Make sure to fetch the scroll position of the popup before closing it bellow
            let scroll_position = $("#highlightzone-links-container").scrollTop();

            // Make sure to close the existing instance if we want to create the new one bellow
            $.magnificPopup.instance.close();

            element_href_link = $(this).attr("href");
            if (element_href_link){
                // **Function inherited from parent**
                self.openHighlightVideoPopup(element_href_link, scroll_position);
            }

            return false;
        });
    }

    highlightElementIsValidBasedOnDropdownFilters(element, filters){
        switch (true){
            // All highlights should be displayed, i.e. all players and all highlight types were selected
            case !filters.player_button_value && filters.additional_highlight_type_values.length === 5:
                return true;
            
            // The highlight types were specified
            case !filters.player_button_value:
                return filters.additional_highlight_type_values.includes($(element).attr("highlight-type-id"));

            // The default is when all filters are set, i.e. player and highlight types
            default:
                return $(element).attr("player-id") === filters.player_button_value &&
                    filters.additional_highlight_type_values.includes($(element).attr("highlight-type-id"));
        }
    }

    displayNoHighlightsHeader(display){
        if (display){
            $("#highlightzone-header-no-highlights").addClass("highlightzone-header-visible"); 
        } else {
            $("#highlightzone-header-no-highlights").removeClass("highlightzone-header-visible"); 
        }
    }

    toggleWeekdayHorizontalSplit(display_no_highlights_header, splits_to_display){
        // If display_no_highlights_header is true, that means there are no highlights to display and thus we should hide the splits.
        if (display_no_highlights_header){
            $(".highlightzone-weekday-split").hide();
            return;
        }

        // We check the splits one by on to see if they have to be displayed
        $(".highlightzone-weekday-split").each(function (){
            let weekday_represented_by_split = $(this).attr("weekday-id");

            // Only display the split if there are highlights to display for that day
            if (splits_to_display.has(weekday_represented_by_split)){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

    displayHighlightElementsBasedOnFilters(filters){
        var self = this;
        var display_no_highlights_header = true;
        var weekday_horizontal_splits_to_display = new Set();
        var highlight_element_containers = $(".highlightzone-link-element-container");

        if (!filters || Object.keys(filters).length === 0 || highlight_element_containers.length === 0){
            return false;
        }

        $(highlight_element_containers).each(function (){
            if (self.highlightElementIsValidBasedOnDropdownFilters(this, filters)){
                $(this).css("display", "flex");
                weekday_horizontal_splits_to_display.add($(this).attr("weekday-id"));        

                // No highlights header should not be displayed if there's any highlights to display after filtering
                display_no_highlights_header = false;
            
            } else {
                $(this).css("display", "none");
            }
        });

        this.displayNoHighlightsHeader(display_no_highlights_header);
        this.toggleWeekdayHorizontalSplit(display_no_highlights_header, weekday_horizontal_splits_to_display);
    }

    validateFilterButtonValues(values){
        var required_keys = ["player_button_value", "additional_highlight_type_values"];
        return values && typeof values === "object" && values instanceof Object && !Array.isArray(values) &&
            Object.keys(values).length !== 0 && this.validators.objectHasRequiredKeys(values, required_keys);
    }

    getAdditionalHighlightTypeValues(additional_types){
        var additional_highlight_type_values = [];

        if (!additional_types || [true, undefined].includes(additional_types) || additional_types.length === 0){
            return additional_highlight_type_values;
        }

        $(additional_types).each(function (){
            let value = $(this).attr("value");

            if (value !== undefined){
                additional_highlight_type_values.push(value);
            }
        });

        return additional_highlight_type_values;
    }

    displaySpecificHighlightElements(filter_button_values){
        // We set the default highlight types to hitting and pitching, i.e. these two types are always displayed 
        var default_highlight_type_values = ["hitting", "pitching", "homerun"].map(key => HIGHLIGHT_TYPES[key].toString());

        if (!this.validateFilterButtonValues(filter_button_values)){
            return false;
        }

        // A player filter was not set, therefore we will not filter based on players
        if(filter_button_values.player_button_value === "all"){
            filter_button_values["player_button_value"] = false;
        }

        // Update the default highlight types with additional ones if they exist
        filter_button_values["additional_highlight_type_values"] = 
            default_highlight_type_values.concat(this.getAdditionalHighlightTypeValues(filter_button_values.additional_highlight_type_values));

        this.displayHighlightElementsBasedOnFilters(filter_button_values); 
    }

    displayHighlightsOfDropdownSelectedPlayerId(id){
        return (!id || id === undefined) ? false : this.displaySpecificHighlightElements({
            player_button_value: id,
            additional_highlight_type_values: $(".highlightzone-toggle-input:checked")
        });
    }

    displayHighlightsOfDropdownSelectedDateRangeId(id){
        return (!id || id === undefined) ? false : this.displaySpecificHighlightElements({
            player_button_value: $("#highlightzone-player-dropdown-button").attr("value"),
            additional_highlight_type_values: $(".highlightzone-toggle-input:checked")
        });
    }

    apiResponseDataIsValid(){
        return this.api_data && Array.isArray(this.api_data) && this.api_data.length !== 0;
    }

    createWeekdayHorizontalSplitHtmlElement(weekday_number, today){
        const days = ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"];
        var line_split_element = $("<hr>").addClass("highlightzone-weekday-split");
        var weekday_name = false;

        // Make sure the type is not a boolean since calling Number() on true or false would return 1 or 0 and in our case we don't
        // want to use the integer value of the boolean.
        var weekday_number_as_int = typeof weekday_number === "boolean" ? false : Number(weekday_number);

        if (!isNaN(weekday_number_as_int) && weekday_number_as_int >= 1 && weekday_number_as_int <= days.length){
            // If the weekday is today, we'll display "TODAY" instead of the weekday name.
            // Else, make sure to subtract 1 from weekday_number since it's indexed starting at 1, i.e. Monday is 1 and Sunday is 7.
            weekday_name = weekday_number_as_int === today ? "TODAY" : days[weekday_number - 1];
        }

        return !weekday_name ? false : $(line_split_element).attr({ "data-content": weekday_name, "weekday-id": weekday_number_as_int });
    }

    updateHighlightLinksContainerWithResponseData(container){
        var self = this;
        var weekday_of_previous_highlight = false;
        var horizontal_split_days_to_display = new Set();
        var highlight_types_that_should_be_displayed = [HIGHLIGHT_TYPES.pitching, HIGHLIGHT_TYPES.hitting, HIGHLIGHT_TYPES.homerun];

        // Since the getDay() function has Sunday as the first day of the week and having index 0, we have to change this to Monday
        // as the first day of the week and having index 1 (i.e. Monday is 1 and Sunday is 7).
        // NOTE: We have to define a custom modulo function because when javascript computes the modulo of negative numbers, the
        // returned value is negative instead of positive.
        const mod_func = (n) => (((n - 1) % 7) + 7) % 7;
        const today = mod_func(new Date().getDay()) + 1;

        var append_horizontal_split = function (weekday){
            let horizontal_split_element = self.createWeekdayHorizontalSplitHtmlElement(weekday, today);

            if (horizontal_split_element){
                $(container).append(horizontal_split_element);

                // Important to update the variable after adding the horizontal split since it will allow us to know what day
                // the videos after the line split represent.
                weekday_of_previous_highlight = weekday;
            }
        };

        if (!this.apiResponseDataIsValid()){
            return container;
        }

        for (let i = 0; i < this.api_data.length; i++){
            let data = this.api_data[i];
            let highlight_link_element = this.createHighlightLinkElement(data);

            if (highlight_link_element){
                // Since the horizontal line is hidden by default, we have to know which lines to display based on the highlights.
                // Since defensive highlights and interviews are not displayed by default, then if a day only has these two types,
                // then the horizontal line will appear with nothing under it. In that case, we would keep the line hidden.
                if (highlight_types_that_should_be_displayed.includes(data["highlight_type"])){
                    horizontal_split_days_to_display.add(data["weekday_number"].toString());
                }

                // If the previous weekday is not set, that means we need to set a horizontal split first before appending the videos.
                // If the previous weekday value is not equal to the weekday value of the current highlight, that means we're about
                // to get highlights for a new day and thus should append a horizontal line to seperate the highlights.
                let highlight_week_of_day_accomplished = Number(data["weekday_number"]);
                if (!weekday_of_previous_highlight || weekday_of_previous_highlight !== highlight_week_of_day_accomplished){
                    append_horizontal_split(highlight_week_of_day_accomplished);
                }

                $(container).append(highlight_link_element);
            }
        }

        // Change the visibility of the line splits based on which days have at least 1 hitting, pitching or homerun highlight type.
        $(container).find(".highlightzone-weekday-split").each(function (){
            return horizontal_split_days_to_display.has($(this).attr("weekday-id")) ? $(this).show() : $(this).hide();
        });

        return container;
    }

    createHighlightTypeToggleOption(text, id, value){
        var container = $("<div></div>").addClass("highlightzone-highlight-type-option");
        var header = $("<h4></h4>").text(text);
        var label = $("<label class=\"highlightzone-toggle-label\"></label>").attr("for", id);
        var input = $("<input type=\"checkbox\" class=\"highlightzone-toggle-input\" />").attr({ id: id, value: value });

        this.setFilterOptionClickEvent(input, "highlight-type");

        $(container).append(header, input, label);
        return container;
    }

    createHighlightTypeToggleSwitch(){
        var container = $("<div></div>").addClass("highlightzone-highlight-type-container");

        var defense_toggle_option_container = $("<div></div>");
        var defense_toggle_option = this.createHighlightTypeToggleOption(
            "Defensive highlights", "highlightzone-highlight-type-defense", HIGHLIGHT_TYPES.defense
        );
        $(defense_toggle_option_container).append(defense_toggle_option);

        var interview_toggle_option_container = $("<div></div>");
        var interview_toggle_option = this.createHighlightTypeToggleOption(
            "Interviews", "highlightzone-highlight-type-interview", HIGHLIGHT_TYPES.interview
        );
        $(interview_toggle_option_container).append(interview_toggle_option);

        $(container).append(defense_toggle_option_container, interview_toggle_option_container);
        return container;
    }

    createFilterMenuHeader(){
        var container = $("<div></div>").addClass("highlightzone-button-menus-container");
        $(container).append(
            this.createPlayersDropdownMenu(),
            this.createHighlightTypeToggleSwitch()
        );
        return container;
    }

    updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes(container){
        var updated_container = container;

        // Use this to know if any highlights will be displayed when the popup is first activated.
        var no_highlights_are_displayed_by_default = true;

        $(container).find(".highlightzone-link-element-container").each(function (){
            if ($(this).css("display") !== "none"){
                no_highlights_are_displayed_by_default = false;
            } 
        });

        // If there are no highlights to be displayed upon first opening the popup, then the No highlights header should be displayed.
        if (no_highlights_are_displayed_by_default){
            $(updated_container).find("#highlightzone-header-no-highlights").addClass("highlightzone-header-visible");
        }

        return updated_container;
    }

    init(){
        var main_container = this.createMainContainer();
        $(main_container).append(
            this.createFilterMenuHeader(),
            this.createNoHighlightsFoundHeaderContainer(),
            this.createHighlightLinksContainer()
        );
        return this.updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes(main_container);
    }
}

export { HighlightLinksPopup };
