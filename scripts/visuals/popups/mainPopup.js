import * as $ from "jquery";
import moment from "moment";
import { Validators } from "../../apiControllers/apiDataValidators.js";


class MainPopup {
    constructor(player_data, api_homerun_data, api_response_data){
        this.players = player_data;
        this.homeruns = api_homerun_data;
        this.api_data = api_response_data;
        this.validators = new Validators();
        this.weekday_number = moment().isoWeekday();
    }

    createMagnificPopupContainer(){
        var container = $("<div></div>").attr("id", "highlightzone-main-container");
        $(container).css("display", "none !important");
        return container;
    }

    createCustomCloseButton(){
        return "<button title=\"%title%\" type=\"button\" class=\"mfp-close\" style=\"font-size: 60px;\">&#215;</button>";
    }

    appendIframeToVideoContainer(url){
        var container = $("#highlightzone-video-container");
        var iframe = "<iframe width=\"100%\" height=\"100%\" src=\"" + url + "\" frameborder=\"0\" allowfullscreen></iframe>";

        if (container.length === 0){
            console.error("Highlightzone error - The video iframe could not be loaded since the video container was not found.");
            return false;
        }

        // Make sure to empty the container first since it might still contain a previously opened video
        $(container).empty();
        $(container).append(iframe);
    }

    openHighlightLinksPopupContainer(scroll_position){
        var self = this;
        var video_container = $("#highlightzone-video-container");
        var highlight_links_container = $("#highlightzone-links-container");

        if (video_container.length !== 0){
            // Very important to empty the video container or else the video will continue playing while the highlight
            // links popup container is open
            $(video_container).empty();
        }

        // No point in calling the magnificPopup function if the container where the popup will open does not exist
        if (highlight_links_container.length === 0){
            console.error("Highlightzone error - The highlight links popup could not be opened since the container was not found.");
            return false;
        }

        $.magnificPopup.open({
            type: "inline",
            alignTop: true,
            closeBtnInside: false,
            closeMarkup: self.createCustomCloseButton(),
            items: {
                src: "#highlightzone-links-container"
            },
            callbacks: {
                open: function (){
                    // Resetting the scroll position to where it was before opening the video
                    $(highlight_links_container).scrollTop(scroll_position);
                }
            }
        });
    }

    openHighlightVideoPopup(url, scroll_position){
        var self = this;
        var video_container = $("#highlightzone-video-container");

        if (video_container.length === 0){
            console.error("Highlightzone error - The highlight video popup could not be opened since the container was not found.");
            return false;
        }

        $.magnificPopup.open({
            type: "inline",
            closeBtnInside: false,
            closeMarkup: self.createCustomCloseButton(),
            items: {
                src: "#highlightzone-video-container"
            },
            callbacks: {
                beforeOpen: function (){
                    $("#highlightzone-video-container").css({ width: "70vw", height: "calc(70vw * 9/16)" });
                    self.appendIframeToVideoContainer(url);
                },
                afterClose: function (){
                    // Reopen the highlight links popup when we leave the video
                    self.openHighlightLinksPopupContainer(scroll_position);
                }
            }
        });
    }

    init(links_container, video_container){
        var main_container = this.createMagnificPopupContainer();
        $(main_container).append(links_container, video_container);

        // Appending to the main page
        $(document.body).append(main_container);
    }
}

export { MainPopup };

