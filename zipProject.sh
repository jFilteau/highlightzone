#!/bin/bash

manifestFile="manifest.json"
archivedFolder="archivedVersions"

validate_archive_folder() {
    if [ ! -d "$archivedFolder" ];
    then
        mkdir $archivedFolder
        echo "Directory for archived versions was created."
    fi
}

archive_project() {
    # Additional required project files to zip
    dist="dist/"
    css="css/"
    magnificCss="node_modules/magnific-popup/dist/magnific-popup.css"
    icons="icons/"

    # Check that required folders exist
    for folder in $dist $css $icons
    do
        if [ ! -d $folder ];
        then
            exit "Could not zip the project - Folder $folder was not found."
        fi
    done

    # Check that required files exist
    for file in $magnificCss $manifestFile
    do
        if [ ! -f $file ];
        then
            exit "Could not zip the project - File $magnificCss was not found."
        fi
    done

    # Before creating he zipped file, make sure the folder in which it will be created exists.
    if [ ! -d $outputFolder ];
    then
        mkdir $outputFolder
    fi

    zip -q -r $outputLocation $manifestFile $dist $css $magnificCss $icons

    echo "Archived file $outputLocation for version $version was succesfully created."
}

main() {
    validate_archive_folder

    # Project version is scraped from the manifest file
    local version=`cat $manifestFile | awk '/"version": "([0-9]+\.){2}[0-9]+"/' | cut -d'"' -f4`

    # Creating a folder, named after the version, to keep the .crx and .pem pack files from getting mixed up with the root folder of the project
    local outputFolder="$archivedFolder/v$version"

    # Filename will contain the current version
    local filename=`echo $version | sed -r 's/\./-/g' | awk '{print "highlightzone_v"$1".zip"}'`

    local outputLocation="$outputFolder/$filename"

    if [ -f $outputLocation ];
    then
        while true;
        do
            # Confirm that we want to overrite the existing zipped file
            read -p "File $outputLocation already exists. Do you want to replace it (y/n)?" answer
            case ${answer:0:1} in
                [Yy]* ) echo "Replacing the file." && archive_project;break;;
                [Nn]* ) echo "File was not replaced.";break;;
                * ) echo "Please answer y or n.";;
            esac
        done
    else
        archive_project
    fi
}

main
