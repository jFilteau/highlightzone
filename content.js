import * as $ from "jquery";
import { visualInitialize } from "./scripts/visuals/initializer.js";
import { TeamPageScraper } from "./scripts/teamInfoScraper.js";


function validateCurrentPageUrlIsFantasyRoster(){
    var re = /^https:\/\/baseball\.fantasysports\.yahoo.com\/.+\/\d+\/\d+(?:$|\/team\?|#)/;
    return re.test(window.location.href);
}

function setPlayersDropdownHideOnWindowClick(){
    window.onclick = function(event) {
        if (!event.target.matches("#highlightzone-player-dropdown-button")){
            $("#highlightzone-player-dropdown-content").removeClass("highlightzone-show-dropdown-menu"); 
        }
    };
}

(function (){
    var players;
    var team_page_scraper;

    // Make sure the script only runs on pages representing a fantasy team
    if (!validateCurrentPageUrlIsFantasyRoster()){
        return;
    }
    
    setPlayersDropdownHideOnWindowClick();

    team_page_scraper = new TeamPageScraper();
    players = team_page_scraper.getPlayersOnFantasyTeam();

    // If scraping the page for the players info returned nothing, there's no point in trying to fetch data.
    if (!players){
        console.error("Highlightzone - No players retrieved from the team page.");
        return;
    }

    // Send the player data to the background script where all the DB and API work will be done
    chrome.runtime.sendMessage({ players: players }, function (response){
        // Important that the visualization be done after receiving the data since it's needed in the creation
        // of the popup elements
        visualInitialize(response["highlights"], response["homeruns"], players);
    });
}());
