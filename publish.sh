#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m"

set_env_variable() {
    local env_file=".env"

    if [ ! -f $env_file ];
    then
        echo -e "${RED}Error - Environment variables file not found. Exiting script.${NC}"
        exit 0
    else
        source $env_file
        echo -e "${GREEN}Environment variables were loaded.${NC}"
    fi
}

validate_archived_folder() {
    if [ ! -d $1 ];
    then
        echo -e "${RED}Error - Folder $1 does not exist. Exiting script.${NC}"
        exit 0
    fi
}

get_list_of_versions() {
    local i=0
    local versions=[]

    while read line
    do
        if [ ! -z "$line" ];
        then
            versions[ $i ]="$line" 
            (( i++ ))
        fi
    done < <(find $1/ -maxdepth 1 -type d -printf '%P\n')

    if [ ${#versions[@]} -eq 0 ];
    then
        echo -e "${RED}Error - No zipped versions to choose from in the $1/ directory. Exiting script.${NC}"
        exit 0
    else
        echo "${versions[@]}"
    fi

}

get_access_token () {
    local request_data="grant_type=refresh_token&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&refresh_token=$REFRESH_TOKEN"
    local access_token=$(curl -s -d $request_data  https://www.googleapis.com/oauth2/v4/token | jq ".access_token")
    echo $access_token
}

update_project() {
    local filepath=$1
    local access_token=$(get_access_token)
    local url="https://www.googleapis.com/upload/chromewebstore/v1.1/items/$APP_ID"

    curl -H "Authorization: Bearer $access_token" -H "x-goog-api-version: 2" -X "PUT" -T $filepath $url
}

get_zipped_file () {
    local filename=`echo $1 | sed -r 's/\./-/g' | awk '{print "highlightzone_"$1".zip"}'`
    local zipped_file="$archivedFolder/$1/$filename"

    if [ ! -f $zipped_file ];
    then
        echo -e "${RED} Error - Zipped file $zipped_file not found. Exiting script.${NC}"
        exit 0
    else
        echo $zipped_file
    fi
}

main() {
    local archivedFolder="archivedVersions"

    set_env_variable
    validate_archived_folder $archivedFolder

    local versions=$(get_list_of_versions $archivedFolder)

    # Selection list to choose from
    echo "Select the version to publish."
    select version in "${versions[@]}" "Exit script"; do
        case $version in
            "Exit script" )
                echo "Exiting the script.";
                break;;
            * ) if echo ${versions[@]} | grep -q -w "$version";
                then
                    echo "Version $version selected."
                    file=$(get_zipped_file $version)
                    update_project $file
                    break
                else
                    echo "Invalid number."
                fi;;
        esac
    done
}

main
