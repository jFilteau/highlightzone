import * as $ from "jquery";
import { TeamPageScraper } from "../scripts/teamInfoScraper.js";


describe("TeamPageScraper object", function (){
    var team_page_scraper;

    beforeEach(function (){
        team_page_scraper = new TeamPageScraper();    
    });

    describe("function getPlayerNameFromElement()", function (){
        beforeEach(function (){
            spyOn(team_page_scraper, "validatePlayerName").and.callThrough();
        });

        describe("should return false", function (){
            it("element parameter has no <a> tag", function (){
                let element = $("<div></div>");
                let output = team_page_scraper.getPlayerNameFromElement(element);
                expect(output).toEqual(false);
            });

            it("element parameter has invalid text in the <a> tag", function (){
                let element = $("<div><a></a></div>");
                let output = team_page_scraper.getPlayerNameFromElement(element);
                expect(output).toEqual(false);

                element = $("<div><a>       </a></div>");
                output = team_page_scraper.getPlayerNameFromElement(element);
                expect(output).toEqual(false);
            });
        });

        it("should return the player's name", function (){
            let element = $("<div><a>Test name</a></div>");
            let output = team_page_scraper.getPlayerNameFromElement(element);
            expect(output).toBe("Test name");
        });
    });

    describe("function getPlayerIdFromElement()", function (){
        describe("should return false", function (){
            it("element parameter has no <a> tag", function (){
                let element = $("<div></div>");
                let output = team_page_scraper.getPlayerIdFromElement(element);
                expect(output).toEqual(false);
            });

            it("element parameter has no href attribute in the <a> tag", function (){
                let element = $("<div><a></a></div>");
                let output = team_page_scraper.getPlayerIdFromElement(element);
                expect(output).toEqual(false);
            });

            it("element parameter has invalid text in the href attribute of the <a> tag", function (){
                let test_data = [
                    $("<div><a href=\"\"></a></div>"),
                    $("<div><a href=\"    \"></a></div>"),
                    $("<div><a href=\"invalidUrl/\"></a></div>")
                ];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getPlayerIdFromElement(test_data[i]);
                    expect(output).toEqual(false);
                }
            });
        });

        it("should return the player's id", function (){
            let element = $("<div><a href=\"page/link/with/id/1234\"></a></div>");
            let output = team_page_scraper.getPlayerIdFromElement(element);
            expect(output).toBe("1234");

            element = $("<div><a href=\"page/link/with/id/        1234    \"></a></div>");
            output = team_page_scraper.getPlayerIdFromElement(element);
            expect(output).toBe("1234");
        });
    });

    describe("function getPlayerTeamNameAbbreviationFromElement()", function (){
        describe("should return false", function (){
            it("element parameter has no <span> tag", function (){
                let element = $("<div></div>");
                let output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
                expect(output).toEqual(false);
            });

            it("element parameter has an invalid string in the <span> tag", function (){
                let element = $("<div><span></span></div>");
                let output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
                expect(output).toEqual(false);

                element = $("<div><span>         </span></div>");
                output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
                expect(output).toEqual(false);

                element = $("<div><span>  - </span></div>");
                output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
                expect(output).toEqual(false);

                element = $("<div><span>  -  -  -  </span></div>");
                output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
                expect(output).toEqual(false);
            });
        });

        it("should return the player's id", function (){
            let element = $("<div><span>Test Team-1B</span></div>");
            let output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
            expect(output).toBe("Test Team");

            element = $("<div><span>Test Team - 1B</span></div>");
            output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
            expect(output).toBe("Test Team");

            element = $("<div><span>           Test Team          - 1B</span></div>");
            output = team_page_scraper.getPlayerTeamNameAbbreviationFromElement(element);
            expect(output).toBe("Test Team");
        });
    })

    describe("function getPitchersTable()", function (){
        afterEach(function (){
            $(document.body).empty();
        });

        it("should return false", function (){
            let test_data = [
                $("<div></div>"),
                $("<div class=\"tablewrap\"></div>"),
                $("<div class=\"tablewrap\" data-pos-type=\"Invalid\"></div>")
            ];

            for (let i = 0; i < test_data.length; i++){
                $(document.body).empty();
                $(document.body).append(test_data[i]);

                let output = team_page_scraper.getPitchersTable();

                expect(output).toEqual(false);
            }
        });

        it("should return the pitchers table", function (){
            $(document.body).append($("<div class=\"tablewrap\" data-pos-type=\"P\"></div>"));

            let output = team_page_scraper.getPitchersTable();

            expect(output.length).toBe(1);
            expect($(output).hasClass("tablewrap")).toEqual(true);
        });
    });

    describe("function getHittersTable()", function (){
        afterEach(function (){
            $(document.body).empty();
        });

        it("should return false", function (){
            let test_data = [
                $("<div></div>"),
                $("<div class=\"tablewrap\"></div>"),
                $("<div class=\"tablewrap\" data-pos-type=\"Invalid\"></div>")
            ];

            for (let i = 0; i < test_data.length; i++){
                $(document.body).empty();
                $(document.body).append(test_data[i]);

                let output = team_page_scraper.getHittersTable();

                expect(output).toEqual(false);
            }
        });

        it("should return the pitchers table", function (){
            $(document.body).append($("<div class=\"tablewrap\" data-pos-type=\"B\"></div>"));

            let output = team_page_scraper.getHittersTable();

            expect(output.length).toBe(1);
            expect($(output).hasClass("tablewrap")).toEqual(true);
        });
    });

    describe("function getPlayerHtmlElementsWhoAreHitters()", function (){
        beforeEach(function (){
            spyOn(team_page_scraper, "getHittersTable");
        });

        afterEach(function (){
            $(document.body).empty();
        });
        
        describe("should return false", function (){
            it("hitters table doesn't exist", function (){
                team_page_scraper.getHittersTable.and.returnValue(false);

                let output = team_page_scraper.getPlayerHtmlElementsWhoAreHitters();

                expect(output).toEqual(false);
            });

            it("hitters table doesn't have the player html elements", function (){
                team_page_scraper.getHittersTable.and.returnValue($("<div></div>"));

                let output = team_page_scraper.getPlayerHtmlElementsWhoAreHitters();

                expect(output).toEqual(false);
            });
        });

        it("should return the players", function (){
            team_page_scraper.getHittersTable.and.returnValue($("<div><div class=\"ysf-player-name\"></div></div>"));

            let output = team_page_scraper.getPlayerHtmlElementsWhoAreHitters();

            expect(output.length).toBe(1);
            expect($(output).hasClass("ysf-player-name")).toEqual(true);
        });
    });

    describe("function getPlayerHtmlElementsWhoArePitchers()", function (){
        beforeEach(function (){
            spyOn(team_page_scraper, "getPitchersTable");
        });

        afterEach(function (){
            $(document.body).empty();
        });
        
        describe("should return false", function (){
            it("hitters table doesn't exist", function (){
                team_page_scraper.getPitchersTable.and.returnValue(false);

                let output = team_page_scraper.getPlayerHtmlElementsWhoArePitchers();

                expect(output).toEqual(false);
            });

            it("hitters table doesn't have the player html elements", function (){
                team_page_scraper.getPitchersTable.and.returnValue($("<div></div>"));

                let output = team_page_scraper.getPlayerHtmlElementsWhoArePitchers();

                expect(output).toEqual(false);
            });
        });

        it("should return the players", function (){
            team_page_scraper.getPitchersTable.and.returnValue($("<div><div class=\"ysf-player-name\"></div></div>"));

            let output = team_page_scraper.getPlayerHtmlElementsWhoArePitchers();

            expect(output.length).toBe(1);
            expect($(output).hasClass("ysf-player-name")).toEqual(true);
        });
    });

    describe("function generateRequiredDataObjectForAllPlayerElements()", function (){
        beforeEach(function (){
            spyOn($, "each").and.callThrough(); 
            spyOn(team_page_scraper, "getPlayerElementRequiredData");
        });

        describe("should return an empty object", function (){
            afterEach(function (){
                $.each.calls.reset(); 
            });

            it("elements parameters is invalid", function (){
                let test_data = [false, true, null, undefined, 0, 1234, "", "Test", [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.generateRequiredDataObjectForAllPlayerElements(test_data[i]);

                    expect(output).toEqual({});
                    expect($.each).not.toHaveBeenCalled();
                }
            });

            it("elements object doesn't have the required data", function (){
                team_page_scraper.getPlayerElementRequiredData.and.returnValue(false);

                let output = team_page_scraper.generateRequiredDataObjectForAllPlayerElements($("<div></div>"));

                expect(output).toEqual({});
                expect($.each).toHaveBeenCalled();
                expect(team_page_scraper.getPlayerElementRequiredData).toHaveBeenCalled();
            });
        }); 

        it("should return the players required object data", function (){
            team_page_scraper.getPlayerElementRequiredData.and.returnValue({ player_id: 1234, data: "Test data" });

            let output = team_page_scraper.generateRequiredDataObjectForAllPlayerElements($("<div></div>"));

            expect(output).toEqual({ 1234: "Test data" });
            expect($.each).toHaveBeenCalled();
            expect(team_page_scraper.getPlayerElementRequiredData).toHaveBeenCalled();
        });
    });

    describe("function getPlayersOnFantasyTeam()", function (){
        return;
        beforeEach(function (){
            spyOn(team_page_scraper, "getPlayerHtmlElementsWhoAreHitters"); 
            spyOn(team_page_scraper, "getPlayerHtmlElementsWhoArePitchers");
            spyOn(team_page_scraper, "generateRequiredDataObjectForAllPlayerElements");
        });

        afterEach(function (){
            team_page_scraper.getPlayerHtmlElementsWhoAreHitters.calls.reset();
            team_page_scraper.getPlayerHtmlElementsWhoArePitchers.calls.reset();
            team_page_scraper.generateRequiredDataObjectForAllPlayerElements.calls.reset();
        });

        describe("should return false", function (){
            it("there are no hitters retrieved", function (){
                team_page_scraper.getPlayerHtmlElementsWhoAreHitters.and.returnValue(false);
                team_page_scraper.getPlayerHtmlElementsWhoArePitchers.and.returnValue(true);

                let output = team_page_scraper.getPlayersOnFantasyTeam();

                expect(output).toEqual(false);
                expect(team_page_scraper.generateRequiredDataObjectForAllPlayerElements).not.toHaveBeenCalled();
            });

            it("there are no pitchers retrieved", function (){
                team_page_scraper.getPlayerHtmlElementsWhoAreHitters.and.returnValue(true);
                team_page_scraper.getPlayerHtmlElementsWhoArePitchers.and.returnValue(false);

                let output = team_page_scraper.getPlayersOnFantasyTeam();

                expect(output).toEqual(false);
                expect(team_page_scraper.generateRequiredDataObjectForAllPlayerElements).not.toHaveBeenCalled();
            });

            it("there are no pitchers and hitters retrieved", function (){
                team_page_scraper.getPlayerHtmlElementsWhoAreHitters.and.returnValue(false);
                team_page_scraper.getPlayerHtmlElementsWhoArePitchers.and.returnValue(false);

                let output = team_page_scraper.getPlayersOnFantasyTeam();

                expect(output).toEqual(false);
                expect(team_page_scraper.generateRequiredDataObjectForAllPlayerElements).not.toHaveBeenCalled();
            });

            it("the required data returned for both pitchers and hitters were empty objects", function (){
                team_page_scraper.getPlayerHtmlElementsWhoAreHitters.and.returnValue(true);
                team_page_scraper.getPlayerHtmlElementsWhoArePitchers.and.returnValue(true);
                team_page_scraper.generateRequiredDataObjectForAllPlayerElements.and.returnValues({}, {});

                let output = team_page_scraper.getPlayersOnFantasyTeam();

                expect(output).toEqual(false);
                expect(team_page_scraper.generateRequiredDataObjectForAllPlayerElements).toHaveBeenCalled();
            });
        });

        it("should return the object with the required players data", function (){
            team_page_scraper.getPlayerHtmlElementsWhoAreHitters.and.returnValue(true);
            team_page_scraper.getPlayerHtmlElementsWhoArePitchers.and.returnValue(true);

            let test_data = [
                { data: [{ 1234: "Test" }, {}], expected_output: { 1234: "Test" }},
                { data: [{}, { 1234: "Test "}], expected_output: { 1234: "Test" }},
                { data: [{ 1234: "Test" }, { 1234: "Test" }], expected_output: { 1234: "Test" }},
                { data: [{ 1111: "Test" }, { 2222: "Test" }], expected_output: { 1111: "Test", 2222: "Test" }},
                { data: [{ 1111: "Test", 2222: "Test" }, { 3333: "Test" }], expected_output: { 1111: "Test", 2222: "Test", 3333: "Test" }}
            ];

            for (let i = 0; i < test_data.length; i++){
                team_page_scraper.generateRequiredDataObjectForAllPlayerElements.and.returnValues(
                    test_data[i]["data"][0], test_data[i]["data"][1]
                );

                let output = team_page_scraper.getPlayersOnFantasyTeam();

                expect(output).toEqual(test_data[i]["expected_output"]);
                expect(team_page_scraper.generateRequiredDataObjectForAllPlayerElements).toHaveBeenCalled();
            }
        });
    });

    describe("function getTeamMlbApiId()", function (){
        describe("should return false", function (){
            it("abbreviation paramater is invalid", function (){
                let test_data = [false, null, undefined, 12345, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamMlbApiId(test_data[i]);

                    expect(output).toEqual(false);
                }
            });

            it("abbreviation was not found as a key in the TEAM_IDS constant", function (){
                let test_data = ["", "Invalid id", "            ", "123456789"];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamMlbApiId(test_data[i]);

                    expect(output).toEqual(false);
                }
            });
        });
    });

    describe("function getTeamIdOfPlayer()", function (){
        describe("should return false", function (){
            it("player parameter is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamIdOfPlayer(test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("player team_id is missing", function (){
                let test_data = [{}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamIdOfPlayer(test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("player team_id has an invalid associated value", function (){
                let test_data = [{ team_id: false }, { team_id: null }, { team_id: undefined }, { team_id: "Not a number" },
                    { team_id: "Not a number 12345" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamIdOfPlayer(test_data[i]);
                    expect(output).toEqual(false);
                }
            });
        });

        it("should return the team id", function (){
            let test_data = [{ team_id: 1234 }, { team_id: "1234" }, { team_id: "    1234    " }];

            for (let i = 0; i < test_data.length; i++){
                let output = team_page_scraper.getTeamIdOfPlayer(test_data[i]);
                expect(output).toBe(1234);
            }
        });
    });

    describe("function getTeamIdsOfPlayersOnFantasyTeam()", function (){
        beforeEach(function (){
            spyOn(team_page_scraper, "getTeamIdOfPlayer");
        });

        describe("should return false", function (){
            it("players parameter has an invalid value", function (){
                let test_data = [false, null, undefined, {}, [], ["Test"], "Test", 1234];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamIdsOfPlayersOnFantasyTeam(test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("players object values are missing the team_id key", function (){
                let test_data = [{ 1234: { key: "Test" } }, { 1234: { test: "Key" }, 5678: { someKey: "Test" } }];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.getTeamIdsOfPlayersOnFantasyTeam(test_data[i]);
                    expect(output).toEqual(false);
                }
            });
        }); 

        it("should return the player team ids", function (){
            // Players are all on the same team, i.e. team with id 1234
            team_page_scraper.getTeamIdOfPlayer.and.returnValue(1234);
            let test_data = [{ 1234: { team_id: "Key" }, 5678: { team_id: "Test" } }];
            for (let i = 0; i < test_data.length; i++){
                let output = team_page_scraper.getTeamIdsOfPlayersOnFantasyTeam(test_data[i]);
                expect(output).toEqual(new Set([1234]));
            }

            // Players are on different teams, i.e. teams with id 1234 and 5678
            team_page_scraper.getTeamIdOfPlayer.and.returnValues(1234, 5678);
            test_data = [{ 1234: { team_id: "Key" }, 5678: { team_id: "Test" } }];
            for (let i = 0; i < test_data.length; i++){
                let output = team_page_scraper.getTeamIdsOfPlayersOnFantasyTeam(test_data[i]);
                expect(output).toEqual(new Set([1234, 5678]));
            }
        });
    });

    describe("function validatePlayerName()", function (){
        describe("should return false", function (){
            it("player name is not a string", function (){
                let test_data = [false, true, null, undefined, {}, [], 1234];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.validatePlayerName(test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("player name is an empty string", function (){
                let test_data = ["", "         "];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.validatePlayerName(test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("player name is an empty string after removing (hitter)", function (){
                let test_data = ["(hitter)", "  (hitter)", "(pitcher)", "  (pitcher)"];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.validatePlayerName(test_data[i]);
                    expect(output).toEqual(false);
                }
            });
        });

        describe("should return the player's name", function (){
            it("name contains the '(' character", function (){
                let test_data = ["Test name(hitter)", "Test name (hitter)", "   Test name (hitter)",
                    "Test name(pitcher)", "Test name (pitcher)", "   Test name (pitcher)", "Test name (  hitter)",
                    "Test name (  pitcher)", "Test name (  pitcher    )"];

                for (let i = 0; i < test_data.length; i++){
                    let output = team_page_scraper.validatePlayerName(test_data[i]);
                    expect(output).toEqual("Test name");
                }
            }); 
        });
    });
});
