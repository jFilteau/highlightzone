import "magnific-popup";
import * as $ from "jquery";
import { HighlightzoneInitButton } from "../../scripts/visuals/videoHighlightWidget.js";

describe("HighlightzoneInitButton object initialization", function (){
    var highlightzone_main_button_test_obj;

    beforeEach(function (){
        highlightzone_main_button_test_obj = new HighlightzoneInitButton();

        spyOn(console, "error");
        spyOn($.magnificPopup, "open");
        spyOn(highlightzone_main_button_test_obj, "setButtonEvents").and.callThrough();
        spyOn(highlightzone_main_button_test_obj, "createInitButton").and.callThrough();
    });

    afterEach(function (){
        $(document.body).empty();
    });

    describe("didn't find the needed container", function (){
        it("and should not be appended to the window", function (){
            highlightzone_main_button_test_obj.init(); 

            // Test function calls
            expect(console.error).toHaveBeenCalled();
            expect(highlightzone_main_button_test_obj.createInitButton).not.toHaveBeenCalled();
            expect(highlightzone_main_button_test_obj.setButtonEvents).not.toHaveBeenCalled();
        });
    });

    describe("found the needed container", function (){
        it("and should be appended to the window", function (){
            // Needed container
            $(document.body).append("<div id=\"yspmaincontent\"><div id=\"team-roster\"><header></header></div></div>");

            highlightzone_main_button_test_obj.init(); 

            // Test function calls
            expect(console.error).not.toHaveBeenCalled();
            expect(highlightzone_main_button_test_obj.createInitButton).toHaveBeenCalled();
            expect(highlightzone_main_button_test_obj.setButtonEvents).toHaveBeenCalled();

            // Test that the created button element exists
            expect($("#highlightzone-launch-button").length).toEqual(1);

            // Test the button click event
            $("#highlightzone-launch-button").click();
            expect($.magnificPopup.open).toHaveBeenCalled();
        });
    });
});
