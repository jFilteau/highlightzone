import "magnific-popup";
import * as $ from "jquery";
import { HighlightLinksPopup } from "../../../scripts/visuals/popups/highlightLinksPopup.js";
import { HIGHLIGHT_TYPES } from "../../../constants/highlightTypes.js";


describe("HighlightLinksPopup object", function (){
    var highlight_links_popup_obj;

    beforeEach(function (){
        highlight_links_popup_obj = new HighlightLinksPopup(); 

        spyOn(highlight_links_popup_obj, "setDropdownButtonEvent");
        spyOn(highlight_links_popup_obj, "createDropdownButton").and.returnValue($("<div class=\"dropdown-button\"></div>"));
        spyOn(highlight_links_popup_obj, "createDropdownContainer").and.returnValue($("<div class=\"dropdown-container\"></div>"));
        spyOn(highlight_links_popup_obj, "createPlayerDropdownOption").and.callFake((id) => $("<a player-id=\"" + id + "\"></a>"));
        spyOn(highlight_links_popup_obj, "createDropdownContentContainer").and.returnValue($("<div class=\"dropdown-content-container\"></div>"));
    });

    describe("object has invalid player data", function (){
        describe("data is undefined", function (){
            it("should not create the dropdown menu HTML element", function (){
                let output = highlight_links_popup_obj.createPlayersDropdownMenu();
                expect(output).toEqual(false);
            });
        });

        describe("data is empty", function (){
            it("should not create the dropdown menu HTML element", function (){
                highlight_links_popup_obj.players = {};

                let output = highlight_links_popup_obj.createPlayersDropdownMenu();
                expect(output).toEqual(false);
            });
        });

        describe("data is missing the name key", function (){
            it("should not create the dropdown menu HTML element", function (){
                highlight_links_popup_obj.players = { id: "Test" };

                let output = highlight_links_popup_obj.createPlayersDropdownMenu();
                expect(output).toEqual(false);
            });
        });
    });

    describe("object has valid player data", function (){
        describe("data has one player", function (){
            it("should create the dropdown menu HTML element with two options", function (){
                highlight_links_popup_obj.players = { 1234: { name: "Test Name" }};

                let output =  highlight_links_popup_obj.createPlayersDropdownMenu();

                // Test function calls
                expect(highlight_links_popup_obj.setDropdownButtonEvent).toHaveBeenCalled();
                expect(highlight_links_popup_obj.createPlayerDropdownOption.calls.allArgs()).toEqual([["all"], ["1234", "Test Name"]]);

                // Test values appended to the dropdown
                expect($(output).find(".dropdown-content-container [player-id='all']").length).toBe(1);
                expect($(output).find(".dropdown-content-container [player-id='1234']").length).toBe(1);
            });
        });

        describe("data has multiple players", function (){
            it("should create the dropdown menu HTML element three", function (){
                highlight_links_popup_obj.players = { 
                    1111: { name: "Test Name 1" },
                    2222: { name: "Test Name 2" },
                    3333: { name: "Test Name 3" }
                };

                let output = highlight_links_popup_obj.createPlayersDropdownMenu();

                // Test function calls
                expect(highlight_links_popup_obj.setDropdownButtonEvent).toHaveBeenCalled();
                expect(highlight_links_popup_obj.createPlayerDropdownOption).toHaveBeenCalled();
                expect(highlight_links_popup_obj.createPlayerDropdownOption.calls.allArgs()).toEqual(
                    [["all"], ["1111", "Test Name 1"], ["2222", "Test Name 2"], ["3333", "Test Name 3"]]
                );

                // Test values appended to the dropdown
                expect($(output).find(".dropdown-content-container [player-id='all']").length).toBe(1);
                expect($(output).find(".dropdown-content-container [player-id='1111']").length).toBe(1);
                expect($(output).find(".dropdown-content-container [player-id='2222']").length).toBe(1);
                expect($(output).find(".dropdown-content-container [player-id='3333']").length).toBe(1);
            });
        });
    });

    describe("object has invalid api data", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "apiResponseDataIsValid");
        });
        
        afterEach(function (){
            highlight_links_popup_obj.apiResponseDataIsValid.calls.reset(); 
        });

        it("should make the no highlights header visible", function (){
            highlight_links_popup_obj.apiResponseDataIsValid.and.returnValue(false);

            let no_highlights_container = highlight_links_popup_obj.createNoHighlightsFoundHeaderContainer();

            expect($(no_highlights_container).hasClass("highlightzone-header-visible")).toEqual(true);
        });
    });

    describe("object has valid api data", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "apiResponseDataIsValid");
        });
        
        afterEach(function (){
            highlight_links_popup_obj.apiResponseDataIsValid.calls.reset(); 
        });

        it("should not make the no highlights header visible", function (){
            highlight_links_popup_obj.apiResponseDataIsValid.and.returnValue(true);

            let no_highlights_container = highlight_links_popup_obj.createNoHighlightsFoundHeaderContainer();

            expect($(no_highlights_container).hasClass("header-visible")).toEqual(false);
        });
    });

    describe("function validateHighlightLinkElementParamaters()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj.validators, "objectHasRequiredKeys").and.callThrough();
        });
        
        afterEach(function (){
            highlight_links_popup_obj.validators.objectHasRequiredKeys.calls.reset(); 
        });

        it("should return false", function (){
            let test_data;

            // Testing for missing key
            test_data = [
                { weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: "Test", headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: "Test", weekday_number: 1, image: "Test", highlight_type: 2, player_id: 1234 },
                { url: "Test", weekday_number: 1, headline: "Test", highlight_type: 2, player_id: 1234 },
                { url: "Test", weekday_number: 1, headline: "Test", image: "Test", player_id: 1234 },
                { url: "Test", weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2 }
            ];
            for(let i = 0; i < test_data.length; i++){
                let output = highlight_links_popup_obj.validateHighlightLinkElementParamaters(test_data[i]["id"], test_data[i]["highlight"]);
                expect(output).toEqual(false);
            }

            // Testing for invalid type associated to the url key
            test_data = [
                { url: false, weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: ["Test"], weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: { id: "Test" }, weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: "", weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 },
                { url: "test", weekday_number: 1, headline: "Test", image: "Test", highlight_type: 2, player_id: 1234 }
            ];
            for(let i = 0; i < test_data.length; i++){
                let output = highlight_links_popup_obj.validateHighlightLinkElementParamaters(test_data[i]["id"], test_data[i]["highlight"]);
                expect(output).toEqual(false);
            }
        }); 

        it("should return true", function (){
            let test_data = { url: "test", weekday_number: 1, headline: "test", image: "test", highlight_type: 2, player_id: 1234 };

            let output = highlight_links_popup_obj.validateHighlightLinkElementParamaters(test_data);

            expect(output).toEqual(true);
        });
    });

    describe("function createHighlightLinkElement()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "validateHighlightLinkElementParamaters");
            spyOn(highlight_links_popup_obj, "validateThatTheHighlightShouldBeDisplayedBasedOnTheType");
            spyOn(highlight_links_popup_obj, "setHighlightElementEvents");
            spyOn(highlight_links_popup_obj.validators, "objectHasRequiredKeys");
            spyOn(highlight_links_popup_obj, "appendHomerunBannerToContainer");
        });

        afterEach(function (){
            highlight_links_popup_obj.validateHighlightLinkElementParamaters.calls.reset();
            highlight_links_popup_obj.validateThatTheHighlightShouldBeDisplayedBasedOnTheType.calls.reset();
            highlight_links_popup_obj.setHighlightElementEvents.calls.reset();
            highlight_links_popup_obj.validators.objectHasRequiredKeys.calls.reset();
            highlight_links_popup_obj.appendHomerunBannerToContainer.calls.reset();
        });

        describe("should not create the highlight element", function (){
            afterEach(function (){
                highlight_links_popup_obj.validateHighlightLinkElementParamaters.calls.reset();
                highlight_links_popup_obj.validateThatTheHighlightShouldBeDisplayedBasedOnTheType.calls.reset();
            });

            it("parameter validation fails", function (){
                highlight_links_popup_obj.validateHighlightLinkElementParamaters.and.returnValue(false);

                let output = highlight_links_popup_obj.createHighlightLinkElement("Highlight");

                expect(output).toEqual(false);
                expect(highlight_links_popup_obj.setHighlightElementEvents).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.appendHomerunBannerToContainer).not.toHaveBeenCalled();
            });

            it("highlight should be displayed validation fails", function (){
                highlight_links_popup_obj.validateHighlightLinkElementParamaters.and.returnValue(true);
                highlight_links_popup_obj.validateThatTheHighlightShouldBeDisplayedBasedOnTheType.and.returnValue(false);

                let output = highlight_links_popup_obj.createHighlightLinkElement("Highlight");

                expect(output).toEqual(false);
                expect(highlight_links_popup_obj.setHighlightElementEvents).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.appendHomerunBannerToContainer).not.toHaveBeenCalled();
            });
        });

        describe("should create the highlight element", function (){
            let test_highlight;

            beforeEach(function (){
                test_highlight = {
                    url: "Test URL",
                    weekday_number: 1,
                    headline: "Test Headline",
                    image: "Test Image",
                    player_id: 1234
                };

                // Set both return values to true to be able to create the element
                highlight_links_popup_obj.validateHighlightLinkElementParamaters.and.returnValue(true);
                highlight_links_popup_obj.validateThatTheHighlightShouldBeDisplayedBasedOnTheType.and.returnValue(true);
            });

            describe("highlight element is displayed", function (){
                it("highlighty type is pitching", function (){
                    test_highlight["highlight_type"] = HIGHLIGHT_TYPES.pitching;

                    let output = highlight_links_popup_obj.createHighlightLinkElement(test_highlight);

                    expect($(output).length).toBe(1);
                    expect($(output).attr("player-id")).toBe("1234");
                    expect($(output).attr("weekday-id")).toBe("1");
                    expect($(output).attr("href")).toBe("Test URL");
                    expect($(output).find("img").length).toBe(1);
                    expect($(output).find("img").attr("src")).toBe("Test Image");
                    expect($(output).find(".hightlightzone-highlight-headline").length).toBe(1);
                    expect($(output).find(".hightlightzone-highlight-headline").text()).toBe("Test Headline");
                    expect(highlight_links_popup_obj.validators.objectHasRequiredKeys).not.toHaveBeenCalled();
                });

                it("highlighty type is hitting", function (){
                    test_highlight["highlight_type"] = HIGHLIGHT_TYPES.hitting;

                    let output = highlight_links_popup_obj.createHighlightLinkElement(test_highlight);

                    expect($(output).length).toBe(1);
                    expect($(output).attr("player-id")).toBe("1234");
                    expect($(output).attr("weekday-id")).toBe("1");
                    expect($(output).attr("href")).toBe("Test URL");
                    expect($(output).find("img").length).toBe(1);
                    expect($(output).find("img").attr("src")).toBe("Test Image");
                    expect($(output).find(".hightlightzone-highlight-headline").length).toBe(1);
                    expect($(output).find(".hightlightzone-highlight-headline").text()).toBe("Test Headline");
                    expect(highlight_links_popup_obj.validators.objectHasRequiredKeys).not.toHaveBeenCalled();
                });

                it("highlighty type is homerun", function (){
                    test_highlight["guid"] = "test-guid";
                    test_highlight["highlight_type"] = HIGHLIGHT_TYPES.homerun;
                    highlight_links_popup_obj.homeruns = { "test-guid": { totalDistance: 400 } }
                    highlight_links_popup_obj.validators.objectHasRequiredKeys.and.returnValue(true);
                    highlight_links_popup_obj.appendHomerunBannerToContainer.and.callThrough();

                    let output = highlight_links_popup_obj.createHighlightLinkElement(test_highlight);

                    expect($(output).length).toBe(1);
                    expect($(output).attr("player-id")).toBe("1234");
                    expect($(output).attr("weekday-id")).toBe("1");
                    expect($(output).attr("href")).toBe("Test URL");
                    expect($(output).find("img").length).toBe(1);
                    expect($(output).find("img").attr("src")).toBe("Test Image");
                    expect($(output).find(".hightlightzone-highlight-headline").length).toBe(1);
                    expect($(output).find(".hightlightzone-highlight-headline").text()).toBe("Test Headline");
                    expect($(output).find(".highlightzone-homerun-banner").length).toBe(1);
                    expect($(output).find(".highlightzone-homerun-banner > span").length).toBe(1);
                    expect($(output).find(".highlightzone-homerun-banner > span").text()).toBe("400  feet");
                    expect(highlight_links_popup_obj.validators.objectHasRequiredKeys).toHaveBeenCalled();
                });
            });

            describe("highlight element has css display set to none", function (){
                it("highlighty type is defense", function (){
                    test_highlight["highlight_type"] = HIGHLIGHT_TYPES.defense;

                    let output = highlight_links_popup_obj.createHighlightLinkElement(test_highlight);

                    expect($(output).length).toBe(1);
                    expect($(output).attr("player-id")).toBe("1234");
                    expect($(output).attr("weekday-id")).toBe("1");
                    expect($(output).attr("href")).toBe("Test URL");
                    expect($(output).find("img").length).toBe(1);
                    expect($(output).find("img").attr("src")).toBe("Test Image");
                    expect($(output).find(".hightlightzone-highlight-headline").length).toBe(1);
                    expect($(output).find(".hightlightzone-highlight-headline").text()).toBe("Test Headline");
                    expect($(output).css("display")).toBe("none");
                    expect(highlight_links_popup_obj.validators.objectHasRequiredKeys).not.toHaveBeenCalled();
                });

                it("highlighty type is interview", function (){
                    test_highlight["highlight_type"] = HIGHLIGHT_TYPES.interview;

                    let output = highlight_links_popup_obj.createHighlightLinkElement(test_highlight);

                    expect($(output).length).toBe(1);
                    expect($(output).attr("player-id")).toBe("1234");
                    expect($(output).attr("weekday-id")).toBe("1");
                    expect($(output).attr("href")).toBe("Test URL");
                    expect($(output).find("img").length).toBe(1);
                    expect($(output).find("img").attr("src")).toBe("Test Image");
                    expect($(output).find(".hightlightzone-highlight-headline").length).toBe(1);
                    expect($(output).find(".hightlightzone-highlight-headline").text()).toBe("Test Headline");
                    expect($(output).css("display")).toBe("none");
                    expect(highlight_links_popup_obj.validators.objectHasRequiredKeys).not.toHaveBeenCalled();
                });
            });
        }); 
    });

    describe("function setFilterOptionClickEvent()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "handleDropdownSelectedOptionOfTypePlayer");
            spyOn(highlight_links_popup_obj, "handleToggleSwitchSelectedOption");
        });

        afterEach(function (){
            highlight_links_popup_obj.handleToggleSwitchSelectedOption.calls.reset();
            highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer.calls.reset();
        });

        it("should not call any handlers", function (){
            let test_option = $("<div></div>");

            let test_data = [false, true, null, undefined, "", 1234, "Invalid case"]

            for (let i = 0; i < test_data.length; i++){
                highlight_links_popup_obj.setFilterOptionClickEvent(test_option, test_data[i]);

                $(test_option).click();

                expect(highlight_links_popup_obj.handleToggleSwitchSelectedOption).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer).not.toHaveBeenCalled();
            }
        });

        describe("runs the switch cases", function (){
            afterEach(function (){
                highlight_links_popup_obj.handleToggleSwitchSelectedOption.calls.reset();
                highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer.calls.reset();
            });

            it("should not run any case, i.e. run the default", function (){
                let test_option = $("<div></div>");

                highlight_links_popup_obj.setFilterOptionClickEvent(test_option, "invalid type");
                $(test_option).click();

                expect(highlight_links_popup_obj.handleToggleSwitchSelectedOption).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer).not.toHaveBeenCalled();
            }); 

            it("should run the 'player' use case", function (){
                let test_option = $("<div></div>");

                highlight_links_popup_obj.setFilterOptionClickEvent(test_option, "player", "test-button");
                $(test_option).click();

                expect(highlight_links_popup_obj.handleToggleSwitchSelectedOption).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer).toHaveBeenCalledWith("test-button", test_option[0]);
            }); 

            it("should run the 'highlight-type' use case", function (){
                let test_option = $("<div></div>");

                highlight_links_popup_obj.setFilterOptionClickEvent(test_option, "highlight-type");
                $(test_option).click();

                expect(highlight_links_popup_obj.handleToggleSwitchSelectedOption).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer).not.toHaveBeenCalled();
            }); 
        });

        it("should reset the scrollbar", function (){
            let parent_container = $("<div></div>").css("overflow", "scroll");
            let option = $("<div id=\"test-option\"></div>");
            $(document.body).append($(parent_container).append(option));

            let output = highlight_links_popup_obj.setFilterOptionClickEvent($("#test-option"), "test-button", null);
            $(option).click();

            expect($(parent_container).scrollTop()).toBe(0);
        });
    });

    describe("function displayHighlightElementsBasedOnFilters()", function (){
        afterEach(function (){
            $(document.body).empty();
        });

        describe("should not try to display anything", function (){
            it("has no filters", function (){
                let test_data = [null, false, {}, []];

                for(let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.displayHighlightElementsBasedOnFilters(test_data[i]);
                    expect(output).toEqual(false); 
                }
            });

            it("fetched no highlight elements", function (){
                let output = highlight_links_popup_obj.displayHighlightElementsBasedOnFilters({ filter: "some filter" });
                expect(output).toEqual(false); 
            });
        }); 

        describe("should try to display elements", function (){
            beforeEach(function (){
                $(document.body).append("<div class=\"highlightzone-link-element-container\" weekday-id=\"1\"></div>");

                spyOn(highlight_links_popup_obj, "highlightElementIsValidBasedOnDropdownFilters");
                spyOn(highlight_links_popup_obj, "displayNoHighlightsHeader");
                spyOn(highlight_links_popup_obj, "toggleWeekdayHorizontalSplit");
            });

            afterEach(function (){
                $(document.body).empty();

                highlight_links_popup_obj.highlightElementIsValidBasedOnDropdownFilters.calls.reset();
                highlight_links_popup_obj.displayNoHighlightsHeader.calls.reset();
                highlight_links_popup_obj.toggleWeekdayHorizontalSplit.calls.reset();
            });

            describe("function toggleWeekdayHorizontalSplit() calls", function (){
                it("called with a value not undefined", function (){
                    let filter_object_missing_date_range = { filter: "some filter", date_range_button_value: 1 };

                    highlight_links_popup_obj.displayHighlightElementsBasedOnFilters(filter_object_missing_date_range);

                    expect(highlight_links_popup_obj.toggleWeekdayHorizontalSplit).toHaveBeenCalledWith(true, new Set());
                });
            });

            it("should set the element display to flex", function (){
                highlight_links_popup_obj.highlightElementIsValidBasedOnDropdownFilters.and.returnValue(true);
                highlight_links_popup_obj.displayHighlightElementsBasedOnFilters({ filter: "some filter" });

                expect(highlight_links_popup_obj.highlightElementIsValidBasedOnDropdownFilters).toHaveBeenCalled();
                expect(highlight_links_popup_obj.displayNoHighlightsHeader).toHaveBeenCalledWith(false);
                expect(highlight_links_popup_obj.toggleWeekdayHorizontalSplit).toHaveBeenCalledWith(false, new Set(["1"]));
                expect($(".highlightzone-link-element-container").css("display")).toBe("flex");
            });

            it("should set the element display to none", function (){
                highlight_links_popup_obj.highlightElementIsValidBasedOnDropdownFilters.and.returnValue(false);
                highlight_links_popup_obj.displayHighlightElementsBasedOnFilters({ filter: "some filter" });

                expect(highlight_links_popup_obj.highlightElementIsValidBasedOnDropdownFilters).toHaveBeenCalled();
                expect(highlight_links_popup_obj.displayNoHighlightsHeader).toHaveBeenCalledWith(true);
                expect(highlight_links_popup_obj.toggleWeekdayHorizontalSplit).toHaveBeenCalledWith(true, new Set());
                expect($(".highlightzone-link-element-container").css("display")).toBe("none");
            });
        });
    });

    describe("function displaySpecificHighlightElements()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "displayHighlightElementsBasedOnFilters");
            spyOn(highlight_links_popup_obj, "getAdditionalHighlightTypeValues");
        });

        it("should not try to display highlight elements based on filter", function (){
            let test_data = [null, false, [], ["Test"], {}, "Test", 1234]; 

            for (let i = 0; i < test_data.length; i++){
                let output = highlight_links_popup_obj.displaySpecificHighlightElements(test_data[i]);
                expect(highlight_links_popup_obj.displayHighlightElementsBasedOnFilters).not.toHaveBeenCalled();
                expect(output).toEqual(false);
            }
        });

        it("should try to display highlight elements based on filter", function (){
            highlight_links_popup_obj.getAdditionalHighlightTypeValues.and.returnValue(["3"]);

            let test_data = [
                { 
                    data: { player_button_value: "1" , additional_highlight_type_values: "3" },
                    expected: { 
                        player_button_value: "1",
                        additional_highlight_type_values: [HIGHLIGHT_TYPES.hitting, HIGHLIGHT_TYPES.pitching, HIGHLIGHT_TYPES.homerun, "3"]
                    }
                },
                { 
                    data: { player_button_value: "all" , additional_highlight_type_values: "3" },
                    expected: {
                        player_button_value: false,
                        additional_highlight_type_values: [HIGHLIGHT_TYPES.hitting, HIGHLIGHT_TYPES.pitching, HIGHLIGHT_TYPES.homerun, "3"]
                    }
                },
                { 
                    data: { player_button_value: "all" , additional_highlight_type_values: "3" },
                    expected: { 
                        player_button_value: false,
                        additional_highlight_type_values: [HIGHLIGHT_TYPES.hitting, HIGHLIGHT_TYPES.pitching, HIGHLIGHT_TYPES.homerun, "3"]
                    }
                }
            ];

            for (let i = 0; i < test_data.length; i++){
                let output = highlight_links_popup_obj.displaySpecificHighlightElements(test_data[i]["data"]);

                expect(highlight_links_popup_obj.displayHighlightElementsBasedOnFilters).toHaveBeenCalledWith(test_data[i]["expected"]);
            }
        });
    });

    describe("function createPlayersObjectWithNamesAsKeys()", function (){
        afterEach(function (){
            highlight_links_popup_obj.players = null;
        });

        describe("should return false", function (){
            afterEach(function (){
                highlight_links_popup_obj.players = null;
            });

            it("players object value is invalid", function (){
                let test_data = [false, null, undefined, "", "Test", 1234, [], ["Test"], {}];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.players = test_data[i];
                    
                    let output = highlight_links_popup_obj.createPlayersObjectWithNamesAsKeys();

                    expect(output).toEqual(false);
                }
            });

            it("players object values associated to the keys are not objects", function (){
                let test_data = [{ 1234: [] }, { 1234: ["Test"] }, { 1234: "" }, { 1234: "Test" }, { 1234: false }, { 1234: null }];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.players = test_data[i];
                    
                    let output = highlight_links_popup_obj.createPlayersObjectWithNamesAsKeys();

                    expect(output).toEqual(false);
                }
            });

            it("players object values associated to the keys don't have the required name key", function (){
                let test_data = [{ 1234: {} }, { 1234: { key: "Test" } }, { 1234: { key1: "Test", key2: "Test" } }];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.players = test_data[i];
                    
                    let output = highlight_links_popup_obj.createPlayersObjectWithNamesAsKeys();

                    expect(output).toEqual(false);
                }
            });
        });

        it("should return a new object with the names as keys", function (){
            highlight_links_popup_obj.players = { 1: { name: "John Smith" }, 2: { name: "Alex Johnson"  }, 3: { key: "Invalid" },
                4: { name: "Chris Taylor" } };

            let output = highlight_links_popup_obj.createPlayersObjectWithNamesAsKeys();

            expect(output).toEqual({ "Alex Johnson": "2", "Chris Taylor": "4", "John Smith": "1" });
        });
    });

    describe("function handleDropdownSelectedOptionOfTypePlayer()", function (){
        beforeEach(function (){
            $(document.body).append($("<button id=\"test-button\"></button>"));

            spyOn(highlight_links_popup_obj, "displayHighlightsOfDropdownSelectedPlayerId");
        });  

        afterEach(function (){
            $(document.body).empty();
        });

        describe("should feed an invalid argument to displayHighlightsOfDropdownSelectedPlayerId()", function (){
            it("button id is invalid", function (){
                let selected_option = $("<div player-id=\"1234\"></div>")[0];
                let test_data = [false, true, null, undefined, 1234, "", "    ", "invalid-button-id", "invalid button id", [], ["Test"],
                    {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer(test_data[i], selected_option);

                    expect($("#test-button").text()).toBe("");
                    expect($("#test-button").attr("value")).toEqual(undefined);
                    expect(highlight_links_popup_obj.displayHighlightsOfDropdownSelectedPlayerId).toHaveBeenCalledWith("1234");
                }
            });

            it("selected option is not a jQuery object", function (){
                let button_id = "test-button";
                let test_data = [false, true, null, undefined, 1234, "", "    ", "invalid-button-id", "invalid button id", [], ["Test"],
                    {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer(button_id, test_data[i]);

                    expect($("#test-button").text()).toBe("");
                    expect($("#test-button").attr("value")).toEqual(undefined);
                    expect(highlight_links_popup_obj.displayHighlightsOfDropdownSelectedPlayerId).toHaveBeenCalledWith(false);
                }
            });

            it("selected option is a jQuery object but doesn't have the player-id attribute", function (){
                let button_id = "test-button";
                let test_data = [$("<div></div>")[0], $("<div test-attribute=\"test\"></div>")[0]];

                for (let i = 0; i < test_data.length; i++){
                    highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer(button_id, test_data[i]);

                    expect($("#test-button").text()).toBe("");
                    expect($("#test-button").attr("value")).toEqual(undefined);
                    expect(highlight_links_popup_obj.displayHighlightsOfDropdownSelectedPlayerId).toHaveBeenCalledWith(undefined);
                }
            });
        });

        it("should feed a valid argument to displayHighlightsOfDropdownSelectedPlayerId()", function (){
            let button_id = "test-button";
            let test_option = $("<div player-id=\"1234\">Test player</div>")[0];

            highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer(button_id, test_option);

            expect($("#test-button").text()).toBe("Test player");
            expect($("#test-button").attr("value")).toEqual("1234");
            expect(highlight_links_popup_obj.displayHighlightsOfDropdownSelectedPlayerId).toHaveBeenCalledWith("1234");
        });

        it("should remove the class from the option parent", function (){
            $(document.body).append($("<div class=\"highlightzone-show-dropdown-menu\"><div id=\"test\">Test player</div></div>"));

            let button_id = "test-button";
            let test_option = $("#test");

            highlight_links_popup_obj.handleDropdownSelectedOptionOfTypePlayer(button_id, test_option);

            expect($("#test").parent().hasClass("highlightzone-show-dropdown-menu")).toEqual(false);
        });
    });

    describe("function handleHighlightTypeToggleSwitchSelectedOption()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "displaySpecificHighlightElements");
        });

        afterEach(function (){
            $(document.body).empty();

            highlight_links_popup_obj.displaySpecificHighlightElements.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                $(document.body).empty();

                highlight_links_popup_obj.displaySpecificHighlightElements.calls.reset();
            });

            it("option parameter is not an html node", function (){
                let test_data = [false, true, null, undefined, 1233, "", "  ", "Test", [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.handleHighlightTypeToggleSwitchSelectedOption(test_data[i]);

                    expect(output).toEqual(false);
                    expect(highlight_links_popup_obj.displaySpecificHighlightElements).not.toHaveBeenCalled();
                }
            });

            it("player button node does not exist on the page", function (){
                let test_date_range_button = $("<div class=\"highlightzone-toggle-options-container\"><div class=\"selected\"></div></div>");
                $(document.body).append(test_date_range_button); 

                let test_option_parameter = $("<div></div>")[0];
                let output = highlight_links_popup_obj.handleHighlightTypeToggleSwitchSelectedOption(test_option_parameter);

                expect(output).toEqual(false);
                expect(highlight_links_popup_obj.displaySpecificHighlightElements).not.toHaveBeenCalled();
            });

            it("player button node doesn't have the value attribute", function (){
                let test_date_range_button = $("<div class=\"highlightzone-toggle-options-container\"><div value=\"test\" class=\"selected\"></div></div>");
                let test_player_button = $("<div id=\"highlightzone-player-dropdown-button\"></div>");
                $(document.body).append(test_date_range_button, test_player_button); 

                let test_option_parameter = $("<div></div>")[0];
                let output = highlight_links_popup_obj.handleHighlightTypeToggleSwitchSelectedOption(test_option_parameter);

                expect(output).toEqual(false);
                expect(highlight_links_popup_obj.displaySpecificHighlightElements).not.toHaveBeenCalled();
            });
        });

        it("should call the displaySpecificHighlightElements() function", function (){
            let test_date_range_button = $("<div class=\"highlightzone-toggle-options-container\"><div value=\"test\" class=\"selected\"></div></div>");
            let test_player_button = $("<div value=\"test\" id=\"highlightzone-player-dropdown-button\"></div>");
            $(document.body).append(test_date_range_button, test_player_button); 

            let test_option_parameter = $("<div></div>")[0];
            let output = highlight_links_popup_obj.handleHighlightTypeToggleSwitchSelectedOption(test_option_parameter);

            expect(highlight_links_popup_obj.displaySpecificHighlightElements).toHaveBeenCalled();
        });
    });

    describe("function getAdditionalHighlightTypeValues()", function (){
        afterEach(function (){
            $(document.body).empty();        
        });

        describe("should return an empty array", function (){
            it("additional_types parameter is invalid", function (){
                let test_data = [false, null, true, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.getAdditionalHighlightTypeValues(test_data[i]);

                    expect(output).toEqual([]);
                }
            }); 

            it("additional_types parameter has length 0", function (){
                let output = highlight_links_popup_obj.getAdditionalHighlightTypeValues($("#test"));

                expect(output).toEqual([]);
            }); 

            it("additional_types parameter node does not have the value attribute", function (){
                let test_node = $("<div id=\"test\"></div>");
                $(document.body).append(test_node);

                let output = highlight_links_popup_obj.getAdditionalHighlightTypeValues($("#test"));

                expect(output).toEqual([]);
            });
        });  

        it("should return an array of additional highlight types", function (){
            let test_node = $("<div class=\"test\" value=\"1\"></div>");
            $(document.body).append(test_node);

            let output = highlight_links_popup_obj.getAdditionalHighlightTypeValues($(".test"));
            expect(output).toEqual(["1"]);

            // Add an extra node
            test_node = $("<div class=\"test\" value=\"2\"></div>");
            $(document.body).append(test_node);

            output = highlight_links_popup_obj.getAdditionalHighlightTypeValues($(".test"));
            expect(output).toEqual(["1", "2"]);
        });
    });

    describe("function setHighlightElementEvents()", function (){
        let test_element;

        beforeEach(function (){
            test_element = $("<div id=\"test-element\"></div>");

            // Creating a test magnificPopup container to see if it gets closed
            $(document.body).append($("<div id=\"test-popup\"></div>"));
            $.magnificPopup.open({ items: { src: "#test-popup" }, type: "inline" });

            spyOn($.magnificPopup.instance, "close");
            spyOn(highlight_links_popup_obj, "openHighlightVideoPopup");
        });   

        afterEach(function (){
            $(document.body).empty();
        });

        it("should not call the openHighlightVideoPopup() function", function (){
            highlight_links_popup_obj.setHighlightElementEvents(test_element); 

            // Function not called since the click event wasn't dispatched
            expect(highlight_links_popup_obj.openHighlightVideoPopup).not.toHaveBeenCalled();

            // Click on the test element to trigger the event
            $(test_element).click();
            expect($.magnificPopup.instance.close).toHaveBeenCalled();
            expect(highlight_links_popup_obj.openHighlightVideoPopup).not.toHaveBeenCalled();
        }); 

        it("should call the openHighlightVideoPopup() function with an undefined scroll value", function (){
            // Element must have an href to be able to call the openHighlightVideoPopup() function
            $(test_element).attr("href", "Test href");

            highlight_links_popup_obj.setHighlightElementEvents(test_element); 
            
            // Function not called since the click event wasn't dispatched
            expect(highlight_links_popup_obj.openHighlightVideoPopup).not.toHaveBeenCalled();

            // Click on the test element to trigger the event
            $(test_element).click();
            expect($.magnificPopup.instance.close).toHaveBeenCalled();
            expect(highlight_links_popup_obj.openHighlightVideoPopup).toHaveBeenCalledWith("Test href", undefined);
        }); 

        it("should call the openHighlightVideoPopup() function with a scroll value", function (){
            // Element must have an href to be able to call the openHighlightVideoPopup() function
            $(test_element).attr("href", "Test href");

            // Creating the links container to test for scroll position
            $(document.body).append($("<div id=\"highlightzone-links-container\" style=\"overflow: scroll;\"></div>"))

            highlight_links_popup_obj.setHighlightElementEvents(test_element); 

            // Function not called since the click event wasn't dispatched
            expect(highlight_links_popup_obj.openHighlightVideoPopup).not.toHaveBeenCalled();

            let test_scroll_data = [0, 10, 20, 100];
            for (let i = 0; i < test_scroll_data.lenght; i++){
                // Manually set the scroll position for testing purposes
                $("#highlightzone-links-container").scrollTop(test_scroll_data[i]);

                // Click on the test element to trigger the event
                $(test_element).click();
                expect($.magnificPopup.instance.close).toHaveBeenCalled();
                expect(highlight_links_popup_obj.openHighlightVideoPopup).toHaveBeenCalledWith("Test href", test_scroll_data[i]);
            }
        }); 
    });

    describe("function updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes()", function (){
        describe("should display the header", function (){
            it("no containers with the class highlightzone-link-element-container", function (){
                let test_data = $("<div><div id=\"highlightzone-header-no-highlights\"></div></div>");

                let output = highlight_links_popup_obj.updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes(test_data);

                expect($(output).find("#highlightzone-header-no-highlights.highlightzone-header-visible").length).toBe(1);
            });

            it("all the containers have a display value of none", function (){
                let test_data = [
                    $(`<div>
                        <div id="highlightzone-header-no-highlights"></div>
                        <div style="display: none;" class="highlightzone-link-element-container"></div>
                    </div>`),
                    $(`<div>
                        <div id="highlightzone-header-no-highlights"></div>
                        <div style="display: none;" class="highlightzone-link-element-container"></div>
                        <div style="display: none;" class="highlightzone-link-element-container"></div>
                    </div>`)
                ];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes(test_data[i]);

                    expect($(output).find("#highlightzone-header-no-highlights.highlightzone-header-visible").length).toBe(1);
                }
            });
        });

        describe("should not display the header", function (){
            it("at least one container does not have a display value of none", function (){
                let test_data = [
                    $(`<div>
                        <div id="highlightzone-header-no-highlights"></div>
                        <div style="display: flex;" class="highlightzone-link-element-container"></div>
                    </div>`),
                    $(`<div>
                        <div id="highlightzone-header-no-highlights"></div>
                        <div style="display: flex;" class="highlightzone-link-element-container"></div>
                        <div style="display: block;" class="highlightzone-link-element-container"></div>
                    </div>`),
                    $(`<div>
                        <div id="highlightzone-header-no-highlights"></div>
                        <div class="highlightzone-link-element-container"></div>
                        <div style="display: none;" class="highlightzone-link-element-container"></div>
                        <div style="display: inline-block;" class="highlightzone-link-element-container"></div>
                    </div>`),
                ];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.updateNoHighlightsHeaderDisplayBasedOnHighlightElementTypes(test_data[i]);

                    expect($(output).find("#highlightzone-header-no-highlights.highlightzone-header-visible").length).toBe(0);
                }
            });
        }); 
    });

    describe("function createWeekdayHorizontalSplitHtmlElement()", function (){
        describe("should return false", function (){
            it("weekday value is not an integer", function (){
                let test_data = [true, false, null, undefined, "", "Test", [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement(test_data[i]);

                    expect(output).toEqual(false);
                }
            });

            it("weekday value is not between 1 and 7", function (){
                let test_data = [-1, 0, 0.1, 0.5, 0.9, 0.999, 7.01, 7.1, 7.5, 7.9, "0", "0.9", "7.1", "7.9", "8","7.9   ", "   8"];

                for (let i = 0; i < test_data.length; i++){
                    let output = highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement(test_data[i]);

                    expect(output).toEqual(false);
                }
            });
        });

        it("should return an html jquery element", function (){
            let test_data = [
                { 
                    data: [1, 1.0, "1", "1.0", "   1"],
                    expected: $("<hr weekday-id=\"1\" data-content=\"MONDAY\" class=\"highlightzone-weekday-split\">")
                },
                { 
                    data: [2, 2.0, "2", "2.0", "   2"],
                    expected: $("<hr weekday-id=\"2\" data-content=\"TUESDAY\" class=\"highlightzone-weekday-split\">")
                },
                { 
                    data: [3, 3.0, "3", "3.0", "   3"],
                    expected: $("<hr weekday-id=\"3\" data-content=\"WEDNESDAY\" class=\"highlightzone-weekday-split\">")
                },
                { 
                    data: [4, 4.0, "4", "4.0", "   4"],
                    expected: $("<hr weekday-id=\"4\" data-content=\"THURSDAY\" class=\"highlightzone-weekday-split\">")
                },
                {
                    data: [5, 5.0, "5", "5.0", "   5"],
                    expected: $("<hr weekday-id=\"5\" data-content=\"FRIDAY\" class=\"highlightzone-weekday-split\">")
                },
                {
                    data: [6, 6.0, "6", "6.0", "   6"],
                    expected: $("<hr weekday-id=\"6\" data-content=\"SATURDAY\" class=\"highlightzone-weekday-split\">")
                },
                { 
                    data: [7, 7.0, "7", "7.0", "   7"],
                    expected: $("<hr weekday-id=\"7\" data-content=\"SUNDAY\" class=\"highlightzone-weekday-split\">")
                }
            ];

            for (let i = 0; i < test_data.length; i++){
                for (let j = 0; j < test_data[i]["data"].length; j++){
                    let output = highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement(test_data[i]["data"][j]);

                    expect(output).toEqual(test_data[i]["expected"]);
                }
            }
        });
    });

    describe("function updateHighlightLinksContainerWithResponseData()", function (){
        beforeEach(function (){
            spyOn(highlight_links_popup_obj, "apiResponseDataIsValid"); 
            spyOn(highlight_links_popup_obj, "createHighlightLinkElement"); 
            spyOn(highlight_links_popup_obj, "createWeekdayHorizontalSplitHtmlElement");
        });

        afterEach(function (){
            highlight_links_popup_obj.apiResponseDataIsValid.calls.reset();
            highlight_links_popup_obj.createHighlightLinkElement.calls.reset();
            highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.calls.reset();
        });

        describe("should return the container element unchanged", function (){
            it("api response data is invalid", function (){
                highlight_links_popup_obj.apiResponseDataIsValid.and.returnValue(false);

                let test_data = $("<div>");

                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                expect(output).toEqual(test_data);
                expect(highlight_links_popup_obj.createHighlightLinkElement).not.toHaveBeenCalled();
                expect(highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement).not.toHaveBeenCalled();
            });

            it("function createHighlightLinkElement() always returns false", function (){
                let output;
                let test_data = $("<div>");

                highlight_links_popup_obj.apiResponseDataIsValid.and.returnValue(true);
                highlight_links_popup_obj.createHighlightLinkElement.and.returnValue(false);

                // Only one value in the array
                highlight_links_popup_obj.api_data = [{ weekday_number: "1" }];
                output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);
                expect(output).toEqual(test_data);
                expect(highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement).not.toHaveBeenCalled();

                // Multiple values in the array
                highlight_links_popup_obj.api_data = [{ weekday_number: "1" }, { weekday_number: "2" }, { weekday_number: "3" }];
                output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);
                expect(output).toEqual(test_data);
                expect(highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement).not.toHaveBeenCalled();
            });
        });

        describe("should return the container updated with the highlights", function (){
            beforeAll(function (){
                // Setting the default date to May 13, 2019 which is a Monday (Note: Date() object month value is 0-indexed)
                jasmine.clock().mockDate(new Date(2019, 4, 13));
            });

            describe("api highlight videos were on separate days so horizontal split exists", function (){
                beforeEach(function (){
                    highlight_links_popup_obj.apiResponseDataIsValid.and.returnValue(true);
                });

                afterEach(function (){
                    highlight_links_popup_obj.createHighlightLinkElement.calls.reset();
                    highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.calls.reset();
                });

                it("one horizontal split", function (){
                    let output;
                    let test_data = $("<div>");

                    highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.and.returnValue($("<hr class=\"test-split\">"));
                    highlight_links_popup_obj.createHighlightLinkElement.and.returnValues($("<div class=\"test\">"));

                    highlight_links_popup_obj.api_data = [{ weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.hitting }];
                    output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                    expect($(output).find(".test").length).toBe(1);
                    expect($(output).find(".test-split").length).toBe(1);
                    expect(highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement).toHaveBeenCalled();
                });

                describe("multiple horizontal splits", function (){
                    it("two splits", function (){
                        let output;
                        let test_data = $("<div>");

                        highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.and.returnValues(
                            $("<hr class=\"test-split\">"), $("<hr class=\"test-split\">"));
                        highlight_links_popup_obj.createHighlightLinkElement.and.returnValues(
                            $("<div class=\"test\">"), $("<div class=\"test\">"));

                        highlight_links_popup_obj.api_data = [
                            { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.hitting },
                            { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.hitting }
                        ];
                        output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);
                        expect($(output).find(".test").length).toBe(2);
                        expect($(output).find(".test-split").length).toBe(2);
                    });

                    it("three splits", function (){
                        let output;
                        let test_data = $("<div>");

                        highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.and.returnValues(
                            $("<hr class=\"test-split\">"), $("<hr class=\"test-split\">"), $("<hr class=\"test-split\">"));
                        highlight_links_popup_obj.createHighlightLinkElement.and.returnValues(
                            $("<div class=\"test\">"), $("<div class=\"test\">"), $("<div class=\"test\">"));

                        highlight_links_popup_obj.api_data = [
                            { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.hitting },
                            { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.hitting },
                            { weekday_number: "3", highlight_type: HIGHLIGHT_TYPES.hitting }
                        ];
                        output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);
                        expect($(output).find(".test").length).toBe(3);
                        expect($(output).find(".test-split").length).toBe(3);
                    });

                    describe("horizontal splits that stay hidden", function (){
                        describe("two splits", function (){
                            beforeEach(function (){
                                highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.and.returnValues(
                                    $("<hr class=\"highlightzone-weekday-split\" weekday-id=\"1\">"),
                                    $("<hr class=\"highlightzone-weekday-split\" weekday-id=\"2\">"));
                                highlight_links_popup_obj.createHighlightLinkElement.and.returnValues(
                                    $("<div class=\"test\">"), $("<div class=\"test\">"));
                            });

                            it("none staying hidden", function (){
                                // Both highlight types must be hitting, pitching or homerun.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.hitting },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.pitching }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(0);
                            });

                            it("one staying hidden", function (){
                                // One highlight type must be hitting, pitching or homerun and the other defense or interview.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.hitting },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.defense }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(1);
                            });

                            it("both staying hidden", function (){
                                // Both highlight types must not be hitting, pitching or homerun.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.interview },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.defense }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(2);
                            });
                        });

                        describe("three splits", function (){
                            beforeEach(function (){
                                highlight_links_popup_obj.createWeekdayHorizontalSplitHtmlElement.and.returnValues(
                                    $("<hr class=\"highlightzone-weekday-split\" weekday-id=\"1\">"),
                                    $("<hr class=\"highlightzone-weekday-split\" weekday-id=\"2\">"),
                                    $("<hr class=\"highlightzone-weekday-split\" weekday-id=\"3\">")
                                );
                                highlight_links_popup_obj.createHighlightLinkElement.and.returnValues(
                                    $("<div class=\"test\">"), $("<div class=\"test\">"), $("<div class=\"test\">"));
                            });

                            it("none staying hidden", function (){
                                // All highlight types must be hitting, pitching or homerun.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.pitching },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.hitting },
                                    { weekday_number: "3", highlight_type: HIGHLIGHT_TYPES.homerun }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(0);
                            });

                            it("two staying hidden", function (){
                                // One highlight type must be hitting, pitching or homerun and the other two defense or interview.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.defense },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.hitting },
                                    { weekday_number: "3", highlight_type: HIGHLIGHT_TYPES.interview }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(2);
                            });

                            it("three staying hidden", function (){
                                // All highlight types must not be hitting, pitching or homerun.
                                highlight_links_popup_obj.api_data = [
                                    { weekday_number: "1", highlight_type: HIGHLIGHT_TYPES.defense },
                                    { weekday_number: "2", highlight_type: HIGHLIGHT_TYPES.defense },
                                    { weekday_number: "3", highlight_type: HIGHLIGHT_TYPES.interview }
                                ];

                                let test_data = $("<div>");
                                let output = highlight_links_popup_obj.updateHighlightLinksContainerWithResponseData(test_data);

                                expect($(output)
                                    .find(".highlightzone-weekday-split")
                                    .filter(function(){ return $(this).css("display") == "none"; }).length).toBe(3);
                            });
                        });
                    });
                });
            });
        }); 
    });
});
