import "magnific-popup";
import * as $ from "jquery";
import { MainPopup } from "../../../scripts/visuals/popups/mainPopup.js";


describe("MainPopup object", function (){
    var main_popup_obj;

    beforeEach(function (){
        main_popup_obj = new MainPopup();

        spyOn(console, "error");
        spyOn($.magnificPopup, "open").and.callThrough();
        spyOn(main_popup_obj, "createCustomCloseButton").and.callThrough();
        spyOn(main_popup_obj, "createMagnificPopupContainer").and.callThrough();
        spyOn(main_popup_obj, "openHighlightLinksPopupContainer").and.callThrough();
    });

    afterEach(function (){
        $(document.body).empty();
    });

    it("should create the main popup HTML element on the page", function (){
        let links_container = $("<div></div>").attr("id", "test-links-container");
        let video_container = $("<div></div>").attr("id", "test-video-container");

        main_popup_obj.init(links_container, video_container); 

        // Test function calls
        expect(main_popup_obj.createMagnificPopupContainer).toHaveBeenCalled();
        
        // Test that the DOM elements appended exist
        expect($("#highlightzone-main-container").length).toEqual(1);
        expect($("#highlightzone-main-container > #test-links-container").length).toEqual(1);
        expect($("#highlightzone-main-container > #test-video-container").length).toEqual(1);
    });

    describe("didn't find the needed container", function (){
        it("should not append the video iframe", function (){
            let output = main_popup_obj.appendIframeToVideoContainer("https://www.testUrl.com");

            expect(output).toEqual(false);
            expect(console.error).toHaveBeenCalled();
        });

        it("should open the main highlight links popup", function (){
            let output = main_popup_obj.openHighlightLinksPopupContainer();

            expect(output).toEqual(false);
            expect(console.error).toHaveBeenCalled();
            expect($.magnificPopup.open).not.toHaveBeenCalled();
            expect(main_popup_obj.createCustomCloseButton).not.toHaveBeenCalled();
        });
    });

    describe("found the needed container", function (){
        afterEach(function (){
            $(document.body).empty();
        });

        it("should append the video iframe", function (){
            // Needed container
            $(document.body).append("<div id=\"highlightzone-video-container\"></div>");

            main_popup_obj.appendIframeToVideoContainer("https://www.testUrl.com");

            expect(console.error).not.toHaveBeenCalled();

            // Test that the iframe was appended
            expect($("#highlightzone-video-container > iframe").length).toBe(1);
        });

        it("should open the main highlight links popup", function (){
            // Needed container
            $(document.body).append("<div id=\"highlightzone-video-container\"></div>", "<div id=\"highlightzone-links-container\"></div>");

            main_popup_obj.openHighlightLinksPopupContainer();

            expect(console.error).not.toHaveBeenCalled();
            expect($.magnificPopup.open).toHaveBeenCalled();
            expect(main_popup_obj.createCustomCloseButton).toHaveBeenCalled();
        });

        it("should open the video popup", function (){
            // Needed container
            $(document.body).append("<div id=\"highlightzone-video-container\"></div>");

            main_popup_obj.openHighlightVideoPopup("https://www.testUrl.com");

            expect(console.error).not.toHaveBeenCalled();
            expect($.magnificPopup.open).toHaveBeenCalled();
            expect(main_popup_obj.createCustomCloseButton).toHaveBeenCalled();
        });
    });
});
