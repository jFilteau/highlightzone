import { VideoPopup } from "../../../scripts/visuals/popups/videoPopup.js";


describe("VideoPopup object", function (){
    var video_popup_object;

    beforeEach(function (){
        video_popup_object = new VideoPopup(); 

        spyOn(video_popup_object, "createMainContainer").and.callThrough();
    });
    
    it("should create the video container for the popup", function (){
        let container = video_popup_object.init();

        expect(video_popup_object.createMainContainer).toHaveBeenCalled();
        expect(container.getAttribute("id")).toBe("highlightzone-video-container");
        expect(container.getAttribute("class")).toBe("mfp-hide");
    });
});
