import * as $ from "jquery";
import { ApiManipulator } from "../../scripts/apiControllers/apiManipulation.js";


describe("ApiManipulator object", function (){
    var api_manipulator_obj;

    beforeEach(function (){
        api_manipulator_obj = new ApiManipulator();
        spyOn($, "ajax");
    });

    describe("function createScheduleDataAjaxRequest()", function (){
        it("should return false", function (){
            let test_data = [
                false, null, undefined, [], ["Test"], 1234, "", "Test", {}, { key: "Test" }, { date: "Test" },
                { "weekday_number" : 1 }, { date: null, weekday_number: 1 }, { date: "Test date", weekday_number: null },
                { date: false, weekday_number: null }, { date: "Test date", weekday_number: undefined },
                { date: null, weekday_number: undefined }, { date: true, weekday_number: 1 }
            ];

            for (let i = 0; i < test_data.length; i++){
                let output = api_manipulator_obj.createScheduleDataAjaxRequest(test_data[i]);

                expect($.ajax).not.toHaveBeenCalled();
                expect(output).toEqual(false);
            }
        });

        it("should call the ajax function", function (){
            let output = api_manipulator_obj.createScheduleDataAjaxRequest({ date: "Test date", weekday_number: 1 });

            expect($.ajax).toHaveBeenCalled();
        });
    });

    describe("function createGameDataAjaxRequest()", function (){
        it("should return false", function (){
            let test_data = [
                false, null, undefined, [], ["Test"], 1234, "", "Test", {}, { key: "Test" }, { pk: "Test" },
                { weekday_number : 1 }, { teams: "Test teams" }, { pk: null, weekday_number: 1, teams: "Test teams" },
                { pk: 1, weekday_number: null, teams: "Test teams" }, { pk: 1, weekday_number: 1, teams: null },
                { pk: undefined, weekday_number: 1, teams: true }, { pk: true, weekday_number: false, teams: null }
            ];

            for (let i = 0; i < test_data.length; i++){
                let output = api_manipulator_obj.createGameDataAjaxRequest(test_data[i]);

                expect($.ajax).not.toHaveBeenCalled();
                expect(output).toEqual(false);
            }
        });

        it("should call the ajax function", function (){
            let output = api_manipulator_obj.createGameDataAjaxRequest({ pk: 1, weekday_number: 1, teams: "Test teams" });

            expect($.ajax).toHaveBeenCalled();
        });
    });

    describe("function getHomerunMetrics()", function (){
        beforeEach(function (){
            spyOn(api_manipulator_obj, "validateLiveFeedResponseData");
            spyOn(api_manipulator_obj.validators, "objectHasRequiredKeys");
        });

        describe("should return false", function (){
            afterEach(function (){
                api_manipulator_obj.validateLiveFeedResponseData.calls.reset();
                api_manipulator_obj.validators.objectHasRequiredKeys.calls.reset();
            });

            it("function validateLiveFeedResponseData() returns false", function (){
                api_manipulator_obj.validateLiveFeedResponseData.and.returnValue(false);

                let output = api_manipulator_obj.getHomerunMetrics("Test", "Test");

                expect(output).toEqual(false);
            });

            it("function objectHasRequiredKeys() returns false", function (){
                api_manipulator_obj.validateLiveFeedResponseData.and.returnValue(true);
                api_manipulator_obj.validators.objectHasRequiredKeys.and.returnValue(false);

                let test_data = { liveData: { plays: { allPlays: ["Test play 1", "Test play 2"] } } };

                let output = api_manipulator_obj.getHomerunMetrics(test_data, "Test");

                expect(output).toEqual(false);
            });

            it("none of the guids is found in any play event", function (){
                api_manipulator_obj.validateLiveFeedResponseData.and.returnValue(true);
                api_manipulator_obj.validators.objectHasRequiredKeys.and.returnValue(true);

                let test_data = { liveData:
                    {
                        plays:
                            {
                                allPlays: [
                                    { playEvents: [{ playId: 9999 }] },
                                    { playEvents: [{ playId: 8888 }] }
                                ] 
                            } 
                    } 
                };
                let test_guids = [1111, 2222];

                let output = api_manipulator_obj.getHomerunMetrics(test_data, test_guids);

                expect(output).toEqual(false);
            });

            it("none of the play events that match a guid has any hit data", function (){
                api_manipulator_obj.validateLiveFeedResponseData.and.returnValue(true);
                api_manipulator_obj.validators.objectHasRequiredKeys.and.returnValue(true);

                let test_data = { liveData:
                    {
                        plays:
                        {
                            allPlays: [
                                { playEvents: [{ playId: 1111 }] },
                                { playEvents: [{ playId: 9999 }] },
                                { playEvents: [{ playId: 3333 }] }
                            ] 
                        } 
                    } 
                };
                let test_guids = [1111, 2222, 3333, 4444, 5555];

                let output = api_manipulator_obj.getHomerunMetrics(test_data, test_guids);

                expect(output).toEqual(false);
            });
        });

        it("should return an object with the guids as keys and the homerun metrics object as associated value", function (){
            api_manipulator_obj.validateLiveFeedResponseData.and.returnValue(true);
            api_manipulator_obj.validators.objectHasRequiredKeys.and.returnValue(true);

            let test_data = { liveData:
                {
                    plays:
                    {
                        allPlays: [
                            { playEvents: [{ playId: 1111, hitData: { distance: "400 feet" } }] },
                            { playEvents: [{ playId: 9999, hitData: { distance: "450 feet" } }] },
                            { playEvents: [{ playId: 3333, hitData: { distance: "420 feet" } }] }
                        ] 
                    } 
                } 
            };
            let test_guids = [1111, 2222, 3333, 4444, 5555];

            let output = api_manipulator_obj.getHomerunMetrics(test_data, test_guids);

            expect(output).toEqual({ 1111: { distance: "400 feet" }, 3333: { distance: "420 feet" } });
        });
    });
});
