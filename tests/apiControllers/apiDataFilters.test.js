import { HIGHLIGHT_TYPES } from "../../constants/highlightTypes.js";
import { DataFilters } from "../../scripts/apiControllers/apiDataFilters.js";


describe("DataFilters object", function (){
    var data_filters_obj;

    beforeEach(function (){
        data_filters_obj = new DataFilters();
    });

    describe("function getPlayerIdInvolvedInHighlight()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj.analyzers, "highlightBelongsToPlayer");
        });

        afterEach(function (){
            data_filters_obj.analyzers.highlightBelongsToPlayer.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.analyzers.highlightBelongsToPlayer.calls.reset();
            });

            it("highlight doesn't belong to players", function (){
                data_filters_obj.analyzers.highlightBelongsToPlayer.and.returnValue(false);

                let test_data = [false, true, null, undefined, [], ["Test"], {}, { key: "Test" }, 1234, "", "Test"];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.getPlayerIdInvolvedInHighlight(test_data[i], "Data", { id: "Data" });

                    expect(data_filters_obj.analyzers.highlightBelongsToPlayer).toHaveBeenCalled();
                    expect(output).toEqual(false);
                }
            });
        });

        it("should return the player id", function (){
            data_filters_obj.analyzers.highlightBelongsToPlayer.and.returnValue(true);

            let output = data_filters_obj.getPlayerIdInvolvedInHighlight("Test", "Test", { 1234: "Data" });

            expect(data_filters_obj.analyzers.highlightBelongsToPlayer).toHaveBeenCalled();
            expect(output).toEqual("1234");
        });
    });
    
    describe("function updateHighlightsByPlayerObject()", function (){
        it("should create the player's data in the object", function (){
            let output = data_filters_obj.updateHighlightsByPlayerObject({}, 1234, "Highlight");

            expect(output).toEqual({ 1234: ["Highlight"] });
        }); 

        it("should update the player's data in the object", function (){
            let output = data_filters_obj.updateHighlightsByPlayerObject({ 1234: ["Highlight 1"] }, 1234, "Highlight 2");

            expect(output).toEqual({ 1234: ["Highlight 1", "Highlight 2"] });
        }); 
    });

    describe("function validateGameHighlightData()", function (){
        it("should return false", function (){
            let test_data = [
                false, true, null, undefined, {}, { highlights: ["Test"]}, { weekday_number: 2 }, 1234, [], ["Test"],
                { highlights: false, weekday_number: 1 }, { highlights: "", weekday_number: 1 }, { highlights: 1234, weekday_number: 1 },
                { highlights: {}, weekday_number: 1 }, { highlights: true, weekday_number: 1 }, { highlights: [], weekday_number: 1 }
            ];

            for (let i = 0 ; i < test_data.length; i++){
                let output = data_filters_obj.validateGameHighlightData(test_data[i]);

                expect(output).toEqual(false);
            }
        });

        it("should return true", function (){
            let output = data_filters_obj.validateGameHighlightData({ highlights: ["Test"], weekday_number: 1 });

            expect(output).toEqual(true);
        });
    });
    
    describe("function filterGameHighlightVideosByPlayer()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "validateGameHighlightData");
            spyOn(data_filters_obj, "getPlayerIdInvolvedInHighlight"); 
            spyOn(data_filters_obj, "getHighlightTypeOfVideo");
        });

        afterEach(function (){
            data_filters_obj.validateGameHighlightData.calls.reset();
            data_filters_obj.getPlayerIdInvolvedInHighlight.calls.reset();
            data_filters_obj.getHighlightTypeOfVideo.calls.reset();
        });

        describe("should return false", function (){
            it("game parameter is invalid", function (){
                data_filters_obj.validateGameHighlightData.and.returnValue(false);

                let output = data_filters_obj.filterGameHighlightVideosByPlayer("Test", "Test");

                expect(output).toEqual(false);
                expect(data_filters_obj.getPlayerIdInvolvedInHighlight).not.toHaveBeenCalled();
                expect(data_filters_obj.getHighlightTypeOfVideo).not.toHaveBeenCalled();
            }); 

            it("no players were involved in any of the highlights", function (){
                data_filters_obj.validateGameHighlightData.and.returnValue(true);
                data_filters_obj.getPlayerIdInvolvedInHighlight.and.returnValue(false);

                let output = data_filters_obj.filterGameHighlightVideosByPlayer({ highlights: ["Highlight 1"], weekday_number: 1 }, "Player");

                expect(output).toEqual(false);
                expect(data_filters_obj.getPlayerIdInvolvedInHighlight).toHaveBeenCalled();
                expect(data_filters_obj.getHighlightTypeOfVideo).not.toHaveBeenCalled();
            });

            it("no highlight types were valid", function (){
                data_filters_obj.validateGameHighlightData.and.returnValue(true);
                data_filters_obj.getPlayerIdInvolvedInHighlight.and.returnValue(1234);
                data_filters_obj.getHighlightTypeOfVideo.and.returnValue(false);

                // In this test, the player parameter is invalid so the getHighlightTypeFromHighlightTexts() is not called
                let output = data_filters_obj.filterGameHighlightVideosByPlayer({ highlights: ["Highlight 1"], weekday_number: 1 }, "Player");

                expect(output).toEqual(false);
                expect(data_filters_obj.getPlayerIdInvolvedInHighlight).toHaveBeenCalled();
                expect(data_filters_obj.getHighlightTypeOfVideo).toHaveBeenCalled();
            });
        });

        it("should create the array of highlight data", function (){
            data_filters_obj.validateGameHighlightData.and.returnValue(true);
            data_filters_obj.getPlayerIdInvolvedInHighlight.and.returnValue(9999);
            data_filters_obj.getHighlightTypeOfVideo.and.returnValue(HIGHLIGHT_TYPES.hitting);

            let output = data_filters_obj.filterGameHighlightVideosByPlayer(
                { highlights: [{ headline: "Test highlight" }], weekday_number: 1, game_pk: 1234 }, { 1234: { position: "SP" } }
            );

            expect(output).toEqual([{
                headline: "Test highlight", weekday_number: 1, highlight_type: HIGHLIGHT_TYPES.hitting, game_pk: 1234, player_id: 9999
            }]);
            expect(data_filters_obj.getHighlightTypeOfVideo).toHaveBeenCalled();
            expect(data_filters_obj.getPlayerIdInvolvedInHighlight).toHaveBeenCalled();
        });
    });

    describe("function filterHighlightVideosDataByPlayer()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "playerDataIsValid");
            spyOn(data_filters_obj, "highlightVideosDataIsValid");
            spyOn(data_filters_obj.validators, "objectHasRequiredKeys");
            spyOn(data_filters_obj, "filterGameHighlightVideosByPlayer");
            spyOn(data_filters_obj, "sortHighlightElementsListByTimestampValueDescending")
        }); 

        afterEach(function (){
            data_filters_obj.playerDataIsValid.calls.reset();
            data_filters_obj.highlightVideosDataIsValid.calls.reset();
            data_filters_obj.filterGameHighlightVideosByPlayer.calls.reset();
            data_filters_obj.validators.objectHasRequiredKeys.calls.reset();
            data_filters_obj.sortHighlightElementsListByTimestampValueDescending.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.playerDataIsValid.calls.reset();
                data_filters_obj.highlightVideosDataIsValid.calls.reset();
                data_filters_obj.validators.objectHasRequiredKeys.calls.reset();
            });

            it("videos parameter is invalid", function (){
                data_filters_obj.highlightVideosDataIsValid.and.returnValue(false);

                let output = data_filters_obj.filterHighlightVideosDataByPlayer("Test", "Test");

                expect(output).toEqual(false);
            });

            it("players parameter is invalid", function (){
                data_filters_obj.highlightVideosDataIsValid.and.returnValue(true);
                data_filters_obj.playerDataIsValid.and.returnValue(false);

                let output = data_filters_obj.filterHighlightVideosDataByPlayer("Test", "Test");

                expect(output).toEqual(false);
            });

            it("videos parameter doesn't have the required keys", function (){
                data_filters_obj.highlightVideosDataIsValid.and.returnValue(true);
                data_filters_obj.playerDataIsValid.and.returnValue(true);
                data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(false);
                data_filters_obj.sortHighlightElementsListByTimestampValueDescending.and.callThrough();

                let output = data_filters_obj.filterHighlightVideosDataByPlayer(["Test video"], "Test player");

                expect(data_filters_obj.validators.objectHasRequiredKeys).toHaveBeenCalledWith("Test video", ["highlights", "game_pk"]);
                expect(output).toEqual(false);
            });
        });

        describe("should return an array of objects representing highlight videos", function (){
            beforeEach(function (){
                data_filters_obj.highlightVideosDataIsValid.and.returnValue(true);
                data_filters_obj.playerDataIsValid.and.returnValue(true);
                data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(true);
            });

            afterEach(function (){
                data_filters_obj.playerDataIsValid.calls.reset();
                data_filters_obj.highlightVideosDataIsValid.calls.reset();
                data_filters_obj.filterGameHighlightVideosByPlayer.calls.reset();
                data_filters_obj.validators.objectHasRequiredKeys.calls.reset();
                data_filters_obj.sortHighlightElementsListByTimestampValueDescending.calls.reset();
            });

            it("array has 1 value", function (){
                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(["Highlight 1"], false, false);
                data_filters_obj.sortHighlightElementsListByTimestampValueDescending.and.returnValue(["Highlight 1"]);

                let output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");

                expect(data_filters_obj.validators.objectHasRequiredKeys.calls.allArgs()).toEqual([
                    ["Highlight 1", ["highlights", "game_pk"]],
                    ["Highlight 2", ["highlights", "game_pk"]],
                    ["Highlight 3", ["highlights", "game_pk"]]
                ]);
                expect(output).toEqual(["Highlight 1"]);
            });

            it("array has 2 values", function (){
                let output;
                data_filters_obj.sortHighlightElementsListByTimestampValueDescending.and.returnValue(["Highlight 1", "Highlight 3"]);

                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(["Highlight 1"], false, ["Highlight 3"]);
                output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");
                expect(data_filters_obj.validators.objectHasRequiredKeys.calls.allArgs()).toEqual([
                    ["Highlight 1", ["highlights", "game_pk"]],
                    ["Highlight 2", ["highlights", "game_pk"]],
                    ["Highlight 3", ["highlights", "game_pk"]]
                ]);
                expect(output).toEqual(["Highlight 1", "Highlight 3"]);

                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(["Highlight 1", "Highlight 3"], false, false);
                output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");
                expect(output).toEqual(["Highlight 1", "Highlight 3"]);
            });

            it("array has 3 values", function (){
                let output;
                data_filters_obj.sortHighlightElementsListByTimestampValueDescending.and.returnValue(
                    ["Highlight 1", "Highlight 2", "Highlight 3"]);

                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(["Highlight 1"], ["Highlight 2"], ["Highlight 3"]);
                output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");
                expect(data_filters_obj.validators.objectHasRequiredKeys.calls.allArgs()).toEqual([
                    ["Highlight 1", ["highlights", "game_pk"]],
                    ["Highlight 2", ["highlights", "game_pk"]],
                    ["Highlight 3", ["highlights", "game_pk"]]
                ]);
                expect(output).toEqual(["Highlight 1", "Highlight 2", "Highlight 3"]);

                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(false, ["Highlight 1", "Highlight 2"], ["Highlight 3"]);
                output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");
                expect(output).toEqual(["Highlight 1", "Highlight 2", "Highlight 3"]);

                data_filters_obj.filterGameHighlightVideosByPlayer.and.returnValues(["Highlight 1"], false, ["Highlight 2", "Highlight 3"]);
                output = data_filters_obj.filterHighlightVideosDataByPlayer(["Highlight 1", "Highlight 2", "Highlight 3"], "Test player");
                expect(output).toEqual(["Highlight 1", "Highlight 2", "Highlight 3"]);
            });
        });
    });
    
    describe("function retrieveGamesWithFantasyPlayersInvolved()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj.validators, "gameHasFantasyPlayerTeamInvolved"); 
        });

        afterEach(function (){
            data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved.calls.reset();
            });

            it("team_ids parameter is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.retrieveGamesWithFantasyPlayersInvolved(test_data[i], "Test");

                    expect(data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved).not.toHaveBeenCalled();
                    expect(output).toEqual(false);
                }
            });

            it("games parameter is invalid", function (){
                let test_data = [false, null, undefined, {}, { key: "Test" }, 1234, "", "Test"];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.retrieveGamesWithFantasyPlayersInvolved(true, test_data[i]);

                    expect(data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved).not.toHaveBeenCalled();
                    expect(output).toEqual(false);
                }
            });

            it("none of the games had players from the fantasy team involved", function (){
                data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved.and.returnValue(false);

                let output = data_filters_obj.retrieveGamesWithFantasyPlayersInvolved([1, 2], ["Game 1", "Game 2"]);

                expect(data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved).toHaveBeenCalled();
                expect(output).toEqual(false);
            });
        });

        it("should return the array of filtered games", function (){
            // Both games have players involved
            data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved.and.returnValue(true);
            let output = data_filters_obj.retrieveGamesWithFantasyPlayersInvolved([1, 2], ["Game 1", "Game 2"]);
            expect(data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved).toHaveBeenCalled();
            expect(output).toEqual(["Game 1", "Game 2"]);

            // Only the first game has players involved
            data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved.and.returnValues(true, false);
            output = data_filters_obj.retrieveGamesWithFantasyPlayersInvolved([1, 2], ["Game 1", "Game 2"]);
            expect(data_filters_obj.validators.gameHasFantasyPlayerTeamInvolved).toHaveBeenCalled();
            expect(output).toEqual(["Game 1"]);
        });
    });
    
    describe("function retrieveGamesDataFromScheduleResponse()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj.validators, "scheduleDataIsValid");
            spyOn(data_filters_obj.validators, "gameStateIsValid");
        });

        afterEach(function (){
            data_filters_obj.validators.scheduleDataIsValid.calls.reset();
            data_filters_obj.validators.gameStateIsValid.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.validators.scheduleDataIsValid.calls.reset();
                data_filters_obj.validators.gameStateIsValid.calls.reset();
            });

            it("response data is invalid", function (){
                data_filters_obj.validators.scheduleDataIsValid.and.returnValue(false);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse("Test");
                
                expect(output).toEqual(false);
            });

            it("response games array is 0", function (){
                data_filters_obj.validators.scheduleDataIsValid.and.returnValue(true);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({ dates: [{ games: [] }] });
                
                expect(output).toEqual(false);
            });

            it("response games array data objects have invalid game states", function (){
                data_filters_obj.validators.scheduleDataIsValid.and.returnValue(true);
                data_filters_obj.validators.gameStateIsValid.and.returnValue(false);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({ dates: [{ games: [{ key: "Test", key2: "Test" }] }] });
                
                expect(output).toEqual(false);
            });

            it("response games array data objects are missing the gamePk key", function (){
                data_filters_obj.validators.scheduleDataIsValid.and.returnValue(true);
                data_filters_obj.validators.gameStateIsValid.and.returnValue(true);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({ dates: [{ games: [{ key: "Test", key2: "Test" }] }] });
                
                expect(output).toEqual(false);
            });
        }); 

        describe("should return an array of the games data", function (){
            beforeEach(function (){
                spyOn(data_filters_obj, "getTeamIdsFromGameData");
                spyOn(data_filters_obj.validators, "objectHasRequiredKeys").and.returnValue(true);

                data_filters_obj.validators.scheduleDataIsValid.and.returnValue(true);
                data_filters_obj.validators.gameStateIsValid.and.returnValue(true);
            });

            afterEach(function (){
                data_filters_obj.getTeamIdsFromGameData.calls.reset();
            });

            it("team ids missing", function (){
                data_filters_obj.getTeamIdsFromGameData.and.returnValue(null);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({
                    dates: [{ games: [{ gamePk: 1, teams: null }] }],
                    weekday_number: 1
                });

                expect(output).toEqual([{ pk: 1, teams: null, weekday_number: 1 }]);
            });

            it("weekday_number missing", function (){
                data_filters_obj.getTeamIdsFromGameData.and.returnValue([1, 2]);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({
                    dates: [{ games: [{ gamePk: 1, teams: [1, 2] }] }]
                });

                expect(output).toEqual([{ pk: 1, teams: [1, 2], weekday_number: null }]);
            });

            it("no missing values", function (){
                data_filters_obj.getTeamIdsFromGameData.and.returnValue([1, 2]);

                let output = data_filters_obj.retrieveGamesDataFromScheduleResponse({
                    dates: [{ games: [{ gamePk: 1, teams: [1, 2] }] }],
                    weekday_number: 1
                });

                expect(output).toEqual([{ pk: 1, teams: [1, 2], weekday_number: 1 }]);
            });
        });
    });
    
    describe("function retrieveVideoDataFromHighlightItem()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj.validators, "objectHasRequiredKeys");
        });

        afterEach(function (){
            data_filters_obj.validators.objectHasRequiredKeys.calls.reset(); 
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.validators.objectHasRequiredKeys.calls.reset(); 
            });

            it("item doesn't have the required keys", function (){
                data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(false);

                let output = data_filters_obj.retrieveVideoDataFromHighlightItem("Test");

                expect(output).toEqual(false);
            });

            it("name is not mp4Avc", function (){
                data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(true);

                let output = data_filters_obj.retrieveVideoDataFromHighlightItem({ url: "Test URL", name: "Invalid name" });

                expect(output).toEqual(false);
            });

            it("url length is 0", function (){
                data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(true);

                let output = data_filters_obj.retrieveVideoDataFromHighlightItem({ name: "mp4Avc", url: "" });

                expect(output).toEqual(false);
            });
        });

        it("should return the video data object", function (){
            data_filters_obj.validators.objectHasRequiredKeys.and.returnValue(true);

            let output = data_filters_obj.retrieveVideoDataFromHighlightItem({ url: "Test URL", name: "mp4Avc" });

            expect(output).toEqual("Test URL");
        });
    });
    
    describe("function getTeamIdsFromGameData()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "gameDataIsValid");
        });

        afterEach(function (){
            data_filters_obj.gameDataIsValid.calls.reset();
        });

        describe("should return false", function (){
            it("data parameter is invalid", function (){
                let test_data = [false, true, null, undefined, "", "Test", 1234, [], ["Test"], [1234]];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.getTeamIdsFromGameData(test_data[i]);

                    expect(output).toEqual(false);
                }
            });

            it("gameDataIsValid() returns false", function (){
                data_filters_obj.gameDataIsValid.and.returnValue(false);

                let output = data_filters_obj.getTeamIdsFromGameData({ key: "Test", key2: "Test" });

                expect(output).toEqual(false);
            });
        }); 

        it("should return an array of the teams ids", function (){
            data_filters_obj.gameDataIsValid.and.returnValue(true);

            let output = data_filters_obj.getTeamIdsFromGameData({ team1: { team: { id: 1 } }, team2: { team: { id: 2 }  } });

            expect(output).toEqual([1, 2]);
        });
    });

    describe("function getHighlightImageLinkSourceToUseForHighlghtItem()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "highlightItemImageDataIsValid"); 
            spyOn(data_filters_obj, "getHighlightImageCutResolutionHeight"); 
            spyOn(data_filters_obj, "getHighlightImageCutAspectRatio"); 
        });

        afterEach(function (){
            data_filters_obj.highlightItemImageDataIsValid.calls.reset(); 
            data_filters_obj.getHighlightImageCutResolutionHeight.calls.reset(); 
            data_filters_obj.getHighlightImageCutAspectRatio.calls.reset(); 
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.highlightItemImageDataIsValid.calls.reset(); 
                data_filters_obj.getHighlightImageCutResolutionHeight.calls.reset(); 
                data_filters_obj.getHighlightImageCutAspectRatio.calls.reset(); 
            });

            it("highlightItemImageDataIsValid() returns false", function (){
                data_filters_obj.highlightItemImageDataIsValid.and.returnValue(false);

                let output = data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem("Test");

                expect(output).toEqual(false);
            });

            it("getHighlightImageCutResolutionHeight() returns a value other than 144", function (){
                data_filters_obj.highlightItemImageDataIsValid.and.returnValue(true);
                data_filters_obj.getHighlightImageCutAspectRatio.and.returnValue("4:3");

                let test_data = [false, null, undefined, "", "Test", 1234, 0, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    data_filters_obj.getHighlightImageCutResolutionHeight.and.returnValue(test_data[i]);

                    let output = data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem({ image: { cuts: ["Test"] } });
                    
                    expect(output).toEqual(false);
                }
            });

            it("getHighlightImageCutAspectRatio() returns a value other than 4:3", function (){
                data_filters_obj.highlightItemImageDataIsValid.and.returnValue(true);
                data_filters_obj.getHighlightImageCutResolutionHeight.and.returnValue(144);

                let test_data = [false, null, undefined, "", "Test", 1234, 0, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    data_filters_obj.getHighlightImageCutAspectRatio.and.returnValue(test_data[i]);

                    let output = data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem({ image: { cuts: ["Test"] } });
                    
                    expect(output).toEqual(false);
                }
            });

            it("the image cut does not have the src key", function (){
                data_filters_obj.highlightItemImageDataIsValid.and.returnValue(true);
                data_filters_obj.getHighlightImageCutResolutionHeight.and.returnValue(144);
                data_filters_obj.getHighlightImageCutAspectRatio.and.returnValue("4:3");

                let output = data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem({ image: { cuts: [{ key: "Test" }] } });

                expect(output).toEqual(false);
            });
        });

        it("should return the url source string of the image", function (){
            data_filters_obj.highlightItemImageDataIsValid.and.returnValue(true);
            data_filters_obj.getHighlightImageCutResolutionHeight.and.returnValue(144);
            data_filters_obj.getHighlightImageCutAspectRatio.and.returnValue("4:3");

            let output = data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem({ image: { cuts: [{ src: "Test link" }] } });

            expect(output).toEqual("Test link");
        });
    });
    
    describe("function retrieveRequiredDataFromHighlightVideoItem()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "getHighlightItemGuid"); 
            spyOn(data_filters_obj, "getHighlightVideoItemUrl"); 
            spyOn(data_filters_obj, "getHighlightItemHeadline"); 
            spyOn(data_filters_obj, "highlightVideoItemIsValid"); 
            spyOn(data_filters_obj, "getHighlightItemKeywordsList"); 
            spyOn(data_filters_obj, "getHighlightItemVideoDescription"); 
            spyOn(data_filters_obj, "getHighlightImageLinkSourceToUseForHighlghtItem"); 
            spyOn(data_filters_obj, "convertDateValueToUnixTimestamp"); 
        });

        afterEach(function (){
            data_filters_obj.getHighlightItemGuid.calls.reset(); 
            data_filters_obj.getHighlightVideoItemUrl.calls.reset(); 
            data_filters_obj.getHighlightItemHeadline.calls.reset(); 
            data_filters_obj.highlightVideoItemIsValid.calls.reset(); 
            data_filters_obj.getHighlightItemKeywordsList.calls.reset(); 
            data_filters_obj.getHighlightItemVideoDescription.calls.reset();
            data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem.calls.reset(); 
            data_filters_obj.convertDateValueToUnixTimestamp.calls.reset(); 
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.getHighlightVideoItemUrl.calls.reset(); 
                data_filters_obj.getHighlightItemHeadline.calls.reset(); 
                data_filters_obj.highlightVideoItemIsValid.calls.reset(); 
            });

            it("item parameter is invalid", function (){
                data_filters_obj.highlightVideoItemIsValid.and.returnValue(false);

                let output = data_filters_obj.retrieveRequiredDataFromHighlightVideoItem("Test");

                expect(output).toEqual(false);
            });

            it("headline is invalid", function (){
                data_filters_obj.getHighlightVideoItemUrl.and.returnValue(true);
                data_filters_obj.getHighlightItemHeadline.and.returnValue(false);
                data_filters_obj.highlightVideoItemIsValid.and.returnValue(true);

                let output = data_filters_obj.retrieveRequiredDataFromHighlightVideoItem("Test");

                expect(output).toEqual(false);
                expect(data_filters_obj.getHighlightItemGuid).not.toHaveBeenCalled();
                expect(data_filters_obj.getHighlightItemVideoDescription).not.toHaveBeenCalled();
                expect(data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem).not.toHaveBeenCalled();
            });

            it("videos by resolution is invalid", function (){
                data_filters_obj.getHighlightVideoItemUrl.and.returnValue(false);
                data_filters_obj.getHighlightItemHeadline.and.returnValue(true);
                data_filters_obj.highlightVideoItemIsValid.and.returnValue(true);

                let output = data_filters_obj.retrieveRequiredDataFromHighlightVideoItem("Test");

                expect(output).toEqual(false);
                expect(data_filters_obj.getHighlightItemGuid).not.toHaveBeenCalled();
                expect(data_filters_obj.getHighlightItemVideoDescription).not.toHaveBeenCalled();
                expect(data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem).not.toHaveBeenCalled();
            });
        });

        it("should return the data object of the video highlight item", function (){
            data_filters_obj.highlightVideoItemIsValid.and.returnValue(true);
            data_filters_obj.getHighlightItemGuid.and.returnValue("Test guid");
            data_filters_obj.getHighlightVideoItemUrl.and.returnValue(["Test video"]);
            data_filters_obj.getHighlightItemHeadline.and.returnValue("Test headline");
            data_filters_obj.getHighlightItemKeywordsList.and.returnValue(["Test list"]);
            data_filters_obj.getHighlightItemVideoDescription.and.returnValue("Test description");
            data_filters_obj.getHighlightImageLinkSourceToUseForHighlghtItem.and.returnValue("Test image");
            data_filters_obj.convertDateValueToUnixTimestamp.and.returnValue("1234567");

            let output = data_filters_obj.retrieveRequiredDataFromHighlightVideoItem({ date: "1234567" });

            expect(output).toEqual({
                headline: "Test headline",
                url: ["Test video"],
                image: "Test image",
                description: "Test description",
                guid: "Test guid",
                keywordsAll: ["Test list"],
                date: "1234567"
            });
        });
    });
    
    describe("function getHighlightVideoItemUrl()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "highlightItemVideoDataIsValid");
            spyOn(data_filters_obj, "retrieveVideoDataFromHighlightItem");
        });

        afterEach(function (){
            data_filters_obj.highlightItemVideoDataIsValid.calls.reset();
            data_filters_obj.retrieveVideoDataFromHighlightItem.calls.reset();
        });

        describe("should return false", function (){
            it("highlightItemVideoDataIsValid() returns false", function (){
                data_filters_obj.highlightItemVideoDataIsValid.and.returnValue(false);

                let output = data_filters_obj.getHighlightVideoItemUrl("Test");

                expect(output).toEqual(false);
            });

            it("retrieveVideoDataFromHighlightItem() returns false", function (){
                data_filters_obj.highlightItemVideoDataIsValid.and.returnValue(true);
                data_filters_obj.retrieveVideoDataFromHighlightItem.and.returnValue(false);

                let output = data_filters_obj.getHighlightVideoItemUrl({ playbacks: ["Test"] });

                expect(output).toEqual(false);
                expect(data_filters_obj.retrieveVideoDataFromHighlightItem).toHaveBeenCalled();
            });
        });

        it("should return the highlight item url link", function (){
            data_filters_obj.highlightItemVideoDataIsValid.and.returnValue(true);
            data_filters_obj.retrieveVideoDataFromHighlightItem.and.returnValue("Test URL");

            let output = data_filters_obj.getHighlightVideoItemUrl({ playbacks: ["Test 1", "Test 2"] });

            expect(output).toEqual("Test URL");
            expect(data_filters_obj.retrieveVideoDataFromHighlightItem).toHaveBeenCalled();
        });
    });

    describe("function retrieveDataFromHighlightVideosItemsList()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "retrieveRequiredDataFromHighlightVideoItem");
        });

        afterEach(function (){
            data_filters_obj.retrieveRequiredDataFromHighlightVideoItem.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.retrieveRequiredDataFromHighlightVideoItem.calls.reset();
            });

            it("list parameter is invalid", function (){
                let test_data = [true, false, null, undefined, {}, { key: "Test" }, [], "Test", "", 1234];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.retrieveDataFromHighlightVideosItemsList(test_data[i]);

                    expect(output).toEqual(false);
                    expect(data_filters_obj.retrieveRequiredDataFromHighlightVideoItem).not.toHaveBeenCalled();
                }
            });

            it("retrieveRequiredDataFromHighlightVideoItem() returns false", function (){
                data_filters_obj.retrieveRequiredDataFromHighlightVideoItem.and.returnValue(false);

                let output = data_filters_obj.retrieveDataFromHighlightVideosItemsList(["Test"]);

                expect(output).toEqual(false);
                expect(data_filters_obj.retrieveRequiredDataFromHighlightVideoItem).toHaveBeenCalled();
            });
        });

        it("should return an array of highlight videos", function (){
            data_filters_obj.retrieveRequiredDataFromHighlightVideoItem.and.returnValues("Test video 1", "Test video 2");

            let output = data_filters_obj.retrieveDataFromHighlightVideosItemsList(["Test 1", "Test 2"]);

            expect(output).toEqual(["Test video 1", "Test video 2"]);
            expect(data_filters_obj.retrieveRequiredDataFromHighlightVideoItem).toHaveBeenCalled();
        });
 
    });
   
    describe("function retrieveHighlightVideosFromResponseData()", function (){
        beforeEach(function (){
            spyOn(data_filters_obj, "gameHighlightsDataIsValid");
            spyOn(data_filters_obj, "retrieveDataFromHighlightVideosItemsList");
        });

        afterEach(function (){
            data_filters_obj.gameHighlightsDataIsValid.calls.reset(); 
            data_filters_obj.retrieveDataFromHighlightVideosItemsList.calls.reset(); 
        });

        describe("should return false", function (){
            afterEach(function (){
                data_filters_obj.gameHighlightsDataIsValid.calls.reset(); 
                data_filters_obj.retrieveDataFromHighlightVideosItemsList.calls.reset(); 
            });

            it("gameHighlightsDataIsValid() returns false", function (){
                data_filters_obj.gameHighlightsDataIsValid.and.returnValue(false);

                let output = data_filters_obj.retrieveHighlightVideosFromResponseData("Test");

                expect(output).toEqual(false);
                expect(data_filters_obj.retrieveDataFromHighlightVideosItemsList).not.toHaveBeenCalled(); 
            });

            it("retrieveDataFromHighlightVideosItemsList() returns false", function (){
                data_filters_obj.gameHighlightsDataIsValid.and.returnValue(true);
                data_filters_obj.retrieveDataFromHighlightVideosItemsList.and.returnValue(false); 

                let output = data_filters_obj.retrieveHighlightVideosFromResponseData({
                    game_pk: 1,
                    teams: [1, 2],
                    weekday_number: 1,
                    highlights: {
                        highlights: { 
                            items: ["Test"] 
                        }
                    } 
                });

                expect(output).toEqual(false);
                expect(data_filters_obj.retrieveDataFromHighlightVideosItemsList).toHaveBeenCalled(); 
            });
        });

        it("should return the object data of highlights for a game", function (){
            data_filters_obj.gameHighlightsDataIsValid.and.returnValue(true);
            data_filters_obj.retrieveDataFromHighlightVideosItemsList.and.returnValue(["Test videos"]);

            let output = data_filters_obj.retrieveHighlightVideosFromResponseData({
                game_pk: 1,
                teams: [1, 2],
                weekday_number: 1,
                highlights: {
                    highlights: { 
                        items: ["Test"] 
                    }
                } 
            });

            expect(output).toEqual({ game_pk: 1, highlights: ["Test videos"], teams: [1, 2], weekday_number: 1 });
            expect(data_filters_obj.retrieveDataFromHighlightVideosItemsList).toHaveBeenCalled(); 
        });
    });

    describe("function gameKeysToFetchForHomerunMetrics()", function (){
        describe("should return false", function (){
            it("highlights parameter is invalid", function (){
                let test_data = [false, true, null, undefined, 1234, "", "Test", [], {}, { key: "test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data[i]);

                    expect(output).toEqual(false);
                }
            });

            it("none of the player highlights contain the highlight type key", function (){
                let test_data = { 1234: [{ key: "Test", guid: "Test guid", game_pk: 1234 }] };

                let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data);

                expect(output).toEqual(false);
            });

            it("none of the player highlights contain the guid key", function (){
                let test_data = { 1234: [{ key: "Test", game_pk: 1234, highlight_type: HIGHLIGHT_TYPES.homerun }] };

                let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data);

                expect(output).toEqual(false);
            })

            it("none of the player highlights contain the game_pk key", function (){
                let test_data = { 1234: [{ key: "Test", guid: "Test guid", highlight_type: HIGHLIGHT_TYPES.homerun }] };

                let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data);

                expect(output).toEqual(false);
            })
            it("none of the player highlights has a highlight type of homerun", function (){
                let test_data = { 1234: [
                    { key: "Test", guid: "Test guid", game_pk: 1234, highlight_type: HIGHLIGHT_TYPES.hitting },
                    { key: "Test", guid: "Test guid", game_pk: 1234, highlight_type: HIGHLIGHT_TYPES.pitching },
                    { key: "Test", guid: "Test guid", game_pk: 1234, highlight_type: HIGHLIGHT_TYPES.defense },
                    { key: "Test", guid: "Test guid", game_pk: 1234, highlight_type: HIGHLIGHT_TYPES.interview }
                ] };

                let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data);

                expect(output).toEqual(false);
            });
        }); 

        it("should return an object with the game keys as key and and array of guids as associated values", function (){
            let test_data = [
                { game_pk: 1111, guid: "guid-1", highlight_type: HIGHLIGHT_TYPES.homerun },
                { game_pk: 2222, guid: "guid-2", highlight_type: HIGHLIGHT_TYPES.homerun },
                { game_pk: 2222, guid: "guid-3", highlight_type: HIGHLIGHT_TYPES.homerun }
            ];

            let output = data_filters_obj.gameKeysToFetchForHomerunMetrics(test_data);

            expect(output).toEqual({ 1111: ["guid-1"], 2222: ["guid-2", "guid-3"] });
        });
    });

    describe("function convertDateValueToUnixTimestamp()", function (){
        it("should return null", function (){
            let test_data = [false, null, undefined, true, 1234, "", "Test", [], ["Test"], {}, { key: "Test" }, -2, "80-80-2020",
                "27-10-2019", "2019-27-01", "01-2019-27", "27-2019-01", "10-27-2019", "19-01-27"]

            for (let i = 0; i < test_data.length; i++){
                let output = data_filters_obj.convertDateValueToUnixTimestamp(test_data[i]);

                expect(output).toEqual(null);
            }
        }); 

        it("should return the unix timestamp value of the date", function (){
            let test_data = [
                { date: "2019-01-27T09:30:45Z", timestamp: 1548581445000 },
                { date: "2019-01-27T09:30:45.000Z", timestamp: 1548581445000 },
                { date: "2019-01-27T09:30:45+00:00", timestamp: 1548581445000 },
                { date: "2019-01-27T09:30:45.000+00:00", timestamp: 1548581445000 }
            ];

            for (let i = 0; i < test_data.length; i++){
                let output = data_filters_obj.convertDateValueToUnixTimestamp(test_data[i]["date"]);

                expect(output).toBe(test_data[i]["timestamp"]);
            }
        }); 
    });

    describe("function sortHighlightElementsListByTimestampValueDescending()", function (){
        it("should return false", function (){
            let test_data = [true, false, null, undefined, 1234, "", "Test", {}, { key: "Test" }, []];

            for (let i = 0; i < test_data.length; i++){
                let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]);

                expect(output).toEqual(false);
            }
        });

        describe("should return the sorted array", function (){
            describe("with invalid date values", function (){
                describe("missing date key", function (){
                    it("1 missing", function (){
                        let test_data = [
                            {
                                list: [{ invalidKey: "Invalid" }, { date: "1234567" }],
                                sorted: [{ date: "1234567" }, { invalidKey: "Invalid" }]
                            },
                            {
                                list: [{ date: "1234567" }, { invalidKey: "Invalid" }],
                                sorted: [{ date: "1234567" }, { invalidKey: "Invalid" }]
                            },
                            {
                                list: [{ date: "5678" }, { invalidKey: "Invalid" }, { date: "1234" }],
                                sorted: [{ date: "5678" }, { date: "1234" }, { invalidKey: "Invalid" }]
                            }
                        ];

                        for (let i = 0; i < test_data.length; i++){
                            let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]["list"]);

                            expect(output).toEqual(test_data[i]["sorted"]);
                        }
                    });

                    it("2 missing", function (){
                        let test_data = [
                            {
                                list: [{ invalidKey1: "Invalid" }, { invalidKey2: "Invalid" }, { date: "1234567" }],
                                sorted: [{ date: "1234567" }, { invalidKey1: "Invalid" }, { invalidKey2: "Invalid" }]
                            },
                            {
                                list: [{ date: "1234567" }, { invalidKey2: "Invalid" }, { invalidKey1: "Invalid" }],
                                sorted: [{ date: "1234567" }, { invalidKey2: "Invalid" }, { invalidKey1: "Invalid" }]
                            },
                            {
                                list: [ { invalidKey2: "Invalid" }, { date: "5678" }, { invalidKey1: "Invalid" }, { date: "1234" }],
                                sorted: [{ date: "5678" }, { date: "1234" }, { invalidKey2: "Invalid" }, { invalidKey1: "Invalid" }]
                            }
                        ];

                        for (let i = 0; i < test_data.length; i++){
                            let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]["list"]);

                            expect(output).toEqual(test_data[i]["sorted"]);
                        }
                    });
                });

                describe("invalid date key", function (){
                    it("1 invalid", function (){
                        let test_data = [
                            {
                                list: [{ date: "Invalid" }, { date: "1234567" }],
                                sorted: [{ date: "1234567" },{ date: "Invalid" }]
                            },
                            {
                                list: [{ date: "1234567" }, { date: "Invalid" }],
                                sorted: [{ date: "1234567" },{ date: "Invalid" }]
                            },
                            {
                                list: [{ date: "5678" }, { date: "Invalid" }, { date: "1234" }],
                                sorted: [{ date: "5678" }, { date: "1234" }, { date: "Invalid" }]
                            },
                            {
                                list: [{ date: false }, { date: "1234567" }],
                                sorted: [{ date: "1234567" },{ date: false }]
                            },
                            {
                                list: [{ date: { key: "Test" } }, { date: "1234567" }],
                                sorted: [{ date: "1234567" },{ date: { key: "Test"  } }]
                            }
                        ];

                        for (let i = 0; i < test_data.length; i++){
                            let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]["list"]);

                            expect(output).toEqual(test_data[i]["sorted"]);
                        }
                    });

                    it("2 invalid", function (){
                        let test_data = [
                            {
                                list: [{ date: "Invalid1" }, { date: "1234567" }, { date: "Invalid2" }],
                                sorted: [{ date: "1234567" }, { date: "Invalid1" }, { date: "Invalid2" }]
                            },
                            {
                                list: [{ date: "1234567" }, { date: "Invalid2" }, { date: "Invalid1" }],
                                sorted: [{ date: "1234567" }, { date: "Invalid2" }, { date: "Invalid1" }]
                            },
                            {
                                list: [{ date: "5678" }, { date: "Invalid1" }, { date: "1234" }, { date: "Invalid2" }],
                                sorted: [{ date: "5678" }, { date: "1234" }, { date: "Invalid1" }, { date: "Invalid2" }]
                            }
                        ];

                        for (let i = 0; i < test_data.length; i++){
                            let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]["list"]);

                            expect(output).toEqual(test_data[i]["sorted"]);
                        }
                    });
                });
            });

            it("with all valid date values", function (){
                let test_data = [
                    { 
                        list: [{ date: "1234" }],
                        sorted: [{ date: "1234" }]
                    },
                    { 
                        list: [{ date: "1111" }, { date: "2222" }],
                        sorted: [{ date: "2222" }, { date: "1111" }]
                    },
                    {
                        list: [{ date: "3333" }, { date: "2222" },{ date: "1111" }],
                        sorted: [{ date: "3333" }, { date: "2222" }, { date: "1111" }]
                    },
                    {
                        list: [{ date: "3333" }, { date: "1111" },{ date: "2222" }],
                        sorted: [{ date: "3333" }, { date: "2222" }, { date: "1111" }]
                    }
                ];

                for (let i = 0; i < test_data.length; i++){
                    let output = data_filters_obj.sortHighlightElementsListByTimestampValueDescending(test_data[i]["list"]);

                    expect(output).toEqual(test_data[i]["sorted"]);
                }
            });
        });
    });
});
