import { Validators } from "../../scripts/apiControllers/apiDataValidators.js";


describe("Validators object", function (){
    var validators_obj;

    beforeEach(function (){
        validators_obj = new Validators();    
    });

    describe("function objectHasRequiredKeys()", function (){
        describe("should return false", function (){
            it("keys data in invalid", function (){
                let test_data = [false, null, undefined, "", "Test", 1234, []];

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.objectHasRequiredKeys({ key: "Test" }, test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("object data in invalid", function (){
                let test_data = [false, null, undefined, "", "Test", 1234, [], {}];

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.objectHasRequiredKeys(test_data[i], ["key"]);
                    expect(output).toEqual(false);
                }
            });

            it("object data and keys are valid but keys are not in the object", function (){
                let output = validators_obj.objectHasRequiredKeys({ some_key: "Test" }, ["key"]);
                expect(output).toEqual(false);
            });
        }); 

        it("should return true", function (){
            let output = validators_obj.objectHasRequiredKeys({ key: "Test", key2: "Test" }, ["key", "key2"]);
            expect(output).toEqual(true);
        });
    });

    describe("function gameHasFantasyPlayerTeamInvolved()", function (){
        describe("should return false", function (){
            it("team_ids parameter is invalid", function(){
                let test_data = [false, null, undefined, "", "Test", 1234, {}, { key: "Test" }, [], ["Test"], new Set()]; 

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.gameHasFantasyPlayerTeamInvolved(test_data[i], { teams: ["Test team"] });
                    expect(output).toEqual(false);
                }
            });

            it("game parameter is invalid", function(){
                let test_data = [false, null, undefined, "", "Test", 1234, {}, { key: "Test" }, { teams: "Test" } , [], ["Test"]]; 

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.gameHasFantasyPlayerTeamInvolved(new Set([1]), test_data[i]);
                    expect(output).toEqual(false);
                }
            });

            it("game and team_ids parameters are valid but none of the team_ids is in the game.teams array", function(){
                let output = validators_obj.gameHasFantasyPlayerTeamInvolved(new Set([1]), { teams: [2, 3] });
                expect(output).toEqual(false);

                output = validators_obj.gameHasFantasyPlayerTeamInvolved(new Set([1, 2, 3]), { teams: [10, 11] });
                expect(output).toEqual(false);
            });
        });      

        it("should return true", function (){
            let output = validators_obj.gameHasFantasyPlayerTeamInvolved(new Set([1, 2, 3]), { teams: [2, 4, 5] })
            expect(output).toEqual(true);
        });
    });

    describe("function scheduleDataIsValid()", function (){
        beforeEach(function (){
            spyOn(validators_obj, "objectHasRequiredKeys"); 
        });

        describe("should return false", function (){
            afterEach(function (){
                validators_obj.objectHasRequiredKeys.calls.reset();
            });

            it("objectHasRequiredKeys() returns false", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(false);
                let output = validators_obj.scheduleDataIsValid("Some data");
                expect(output).toEqual(false);
            });

            it("totalGames key is 0", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);
                let output = validators_obj.scheduleDataIsValid({ totalGames: 0 });
                expect(output).toEqual(false);
            });

            it("dates key is not an array", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);
                
                let test_data = [false, null, undefined, "", "Test", {}, 1234];

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.scheduleDataIsValid({ totalGames: 1, dates: test_data[i] });
                    expect(output).toEqual(false);
                }
            });

            it("dates key is an array but of length 0", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);
                let output = validators_obj.scheduleDataIsValid({ totalGames: 1, dates: [] });
                expect(output).toEqual(false);
            });

            it("objectHasRequiredKeys() returns true on the first call but false on the second", function (){
                validators_obj.objectHasRequiredKeys.and.returnValues(true, false);
                let output = validators_obj.scheduleDataIsValid({ totalGames: 1, dates: [1] });
                expect(output).toEqual(false);
            });
        });

        it("should return true", function (){
            validators_obj.objectHasRequiredKeys.and.returnValue(true);
            let output = validators_obj.scheduleDataIsValid({ totalGames: 1, dates: [1] });
            expect(output).toEqual(true);
        });
    });

    describe("function gameHasHighlightVideos()", function (){
        beforeEach(function (){
            spyOn(validators_obj, "objectHasRequiredKeys"); 
        });

        describe("should return false", function (){
            afterEach(function (){
                validators_obj.objectHasRequiredKeys.calls.reset();
            });

            it("objectHasRequiredKeys() returns false", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(false);
                let output = validators_obj.gameHasHighlightVideos("Some data");
                expect(output).toEqual(false);
            });

            it("objectHasRequiredKeys() returns true on the first call but false on the second", function (){
                validators_obj.objectHasRequiredKeys.and.returnValues(true, false);
                let output = validators_obj.gameHasHighlightVideos("Some data");
                expect(output).toEqual(false);
            });

            it("hasHighlightsVideo key is false", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);
                let output = validators_obj.gameHasHighlightVideos({ summary: { hasHighlightsVideo: false } });
                expect(output).toEqual(false);
            });
        });

        it("should return true", function (){
            validators_obj.objectHasRequiredKeys.and.returnValue(true);
            let output = validators_obj.gameHasHighlightVideos({ summary: { hasHighlightsVideo: true } });
            expect(output).toEqual(true);
        });
    });

    describe("function gameStateIsValid()", function (){
        beforeEach(function (){
            spyOn(validators_obj, "objectHasRequiredKeys");
        });

        describe("should return false", function (){
            afterEach(function (){
                validators_obj.objectHasRequiredKeys.calls.reset();
            });

            it("game parameter does not have the required keys", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(false);

                let output = validators_obj.gameStateIsValid("Game");

                expect(output).toEqual(false);
            });

            it("abstractGameCode key associated value is not a string", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let test_data = [false, true, undefined, null, 1234, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: test_data[i], codedGameState: "Test" } });

                    expect(output).toEqual(false);
                }
            });

            it("codedGameState key associated value is not a string", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let test_data = [false, true, undefined, null, 1234, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "Test", codedGameState: test_data[i] } });

                    expect(output).toEqual(false);
                }
            });

            it("abstractGameCode key associated value does not have the value l or f", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "Test", codedGameState: "Test" } });

                expect(output).toEqual(false);
            });

            it("codedGameState key associated value does not have the value f or o", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "f", codedGameState: "Test" } });

                expect(output).toEqual(false);
            });
        }); 

        describe("should return true", function (){
            afterEach(function (){
                validators_obj.objectHasRequiredKeys.calls.reset();
            });

            it("abstractGameCode key associated value is l", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "l", codedGameState: "Test" } });

                expect(output).toEqual(true);
            });

            it("abstractGameCode key associated value is f and that of codedGameState is f", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "l", codedGameState: "f" } });

                expect(output).toEqual(true);
            });

            it("abstractGameCode key associated value is f and that of codedGameState is o", function (){
                validators_obj.objectHasRequiredKeys.and.returnValue(true);

                let output = validators_obj.gameStateIsValid({ status: {  abstractGameCode: "l", codedGameState: "o" } });

                expect(output).toEqual(true);
            });
        });
    });
});
