import { ApiInitializer } from "../../scripts/apiControllers/initializer.js";


describe("ApiInitializer object", function (){
    let initializer_obj;

    beforeEach(function (){
        initializer_obj = new ApiInitializer();
    });

    describe("function createLiveGameFeedAjaxRequestsFromRequiredData()", function (){
        beforeEach(function (){
            spyOn(initializer_obj.api, "createLiveGameFeedDataAjaxRequest"); 
        }); 

        afterEach(function (){
            initializer_obj.api.createLiveGameFeedDataAjaxRequest.calls.reset();
        });

        describe("should return false", function (){
            it("data parameter is invalid", function (){
                let test_data = [false, true, null, undefined, 1234, "", "Test", [], ["Test"]];

                for (let i = 0; i < test_data.length; i++){
                    let output = initializer_obj.createLiveGameFeedAjaxRequestsFromRequiredData(test_data[i]);

                    expect(output).toEqual(false);
                    expect(initializer_obj.api.createLiveGameFeedDataAjaxRequest).not.toHaveBeenCalled();
                }
            });

            it("function createLiveGameFeedDataAjaxRequest() returns false", function (){
                initializer_obj.api.createLiveGameFeedDataAjaxRequest.and.returnValue(false);

                let test_data = { 1234: "Test", 4567: "Test" };

                let output = initializer_obj.createLiveGameFeedAjaxRequestsFromRequiredData(test_data);

                expect(output).toEqual(false);
                expect(initializer_obj.api.createLiveGameFeedDataAjaxRequest).toHaveBeenCalled();
            });
        });

        it("should create an array of requests", function (){
            initializer_obj.api.createLiveGameFeedDataAjaxRequest.and.returnValues("Test request 1", "Test request 2");

            let test_data = { 1234: "Test", 4567: "Test" };

            let output = initializer_obj.createLiveGameFeedAjaxRequestsFromRequiredData(test_data);

            expect(output).toEqual(["Test request 1", "Test request 2"]);
            expect(initializer_obj.api.createLiveGameFeedDataAjaxRequest).toHaveBeenCalled();
        });
    });
});
