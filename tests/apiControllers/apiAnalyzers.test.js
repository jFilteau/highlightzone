import { Analyzers } from "../../scripts/apiControllers/apiAnalyzers.js";
import { HIGHLIGHT_TYPES } from "../../constants/highlightTypes.js";
import { PLAYER_POSITIONS } from "../../constants/playerPositions.js";


describe("Analyzers object", function (){
    var analyzers_obj;

    beforeEach(function (){
        analyzers_obj = new Analyzers();
    });

    describe("function getHighlightTypeFromHighlightTexts()", function (){
        describe("should return false", function (){
            it("description paramater is invalid", function (){
                let test_data = [false, null, undefined, [], 1234, {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], "Test", PLAYER_POSITIONS.hitter);

                    expect(output).toEqual(false);
                }
            });

            it("header paramater is invalid", function (){
                let test_data = [false, null, undefined, [], 1234, {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts("Test", test_data[i], PLAYER_POSITIONS.hitter);

                    expect(output).toEqual(false);
                }
            });

            it("no pattern matched for either text values", function (){
                let test_data = ["Test text", "home", "grand", "back-to-", "ding", "sco", "defen", "discu", "stri ou",
                    "run", "r", "e", "ive", "hand", "div", "ing", "bare", "back"];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);

                    expect(output).toEqual(false);
                }
            });
        });

        describe("should return the highlight type", function (){
            it("highlight type is hitting", function (){
                let test_data = ["Arenado scores on walk", "Trout singles to center", "Betts doubles down the line",
                    "Moncada hits a triple in the gap"];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);

                    expect(output).toEqual(HIGHLIGHT_TYPES.hitting);
                }
            });

            it("highlight type is homerun", function (){
                let test_data = ["HoMer to left", "Betts home RUN to center", "Trout with the dinger to right","Trout's grand slam",
                    "Betts and Martinez go back-to-BACK", "Betts hits a HR.", "hr by Betts in the first.", "Trout blasts it over centerfield."];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);

                    expect(output).toEqual(HIGHLIGHT_TYPES.homerun);
                }
            });

            it("highlight type is pitching", function (){
                let test_data = ["Sale strikes out the side", "Scherzer with 8 K's", "Kershaw's 8 inning shutout",
                    "Diaz's 8th save of the season", "Hader striking out the side in the 8th", "Corbin strikes out nine."];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.pitcher);

                    expect(output).toEqual(HIGHLIGHT_TYPES.pitching);
                }
            });

            it("highlight type is defense", function (){
                let test_data = ["Nice defensive stop by Baez", "Barehanded play by Baez", "Judge dives to catch the liner",
                    "Great throw to second by Trout", "Betts with the grab at the wall", "Trout catches the liner on the run",
                    "Baez and Russell team up for a nice double play", "Baez fires to first to get the out", "Trout nabs Baez at the plate"];

                for (let i = 0; i < test_data.length; i++){
                    // Testing with player's position being a hitter
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);
                    expect(output).toEqual(HIGHLIGHT_TYPES.defense);

                    // Testing with player's position being a pitcher
                    output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.pitcher);
                    expect(output).toEqual(HIGHLIGHT_TYPES.defense);
                }
            });

            it("highlight type is interview", function (){
                let test_data = ["Francona discusses injuries", "Trout talks about team's win", "Betts gives an UpDAtE on injury",
                    "Announcement made by Francona", "Sale discusses his shutout", "Trout talks about his grand slam.",
                    "Bumgarner discusses his 8 inning shutout and two homeruns"];

                for (let i = 0; i < test_data.length; i++){
                    // Testing with player's position being a hitter
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);
                    expect(output).toEqual(HIGHLIGHT_TYPES.interview);

                    // Testing with player's position being a pitcher
                    output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.pitcher);
                    expect(output).toEqual(HIGHLIGHT_TYPES.interview);
                }
            });

            it("highlight type is daily recap and returns false", function (){
                let test_data = [
                    "Daily recap: Francona discusses injuries",
                    "   Daily recap Trout talks about team's win",
                    " daily RECAP: Betts hits a homer in the 9th to win it.",
                    "       daily recap-Chapman's stellar defensive plays help lead the A's to victory",
                    " daily           recap : Sale's shutout gives the sox their first win",
                    "Daily recap,", " daily recap:"
                ];

                for (let i = 0; i < test_data.length; i++){
                    // Testing with player's position being a hitter
                    let output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.hitter);
                    expect(output).toEqual(false);

                    // Testing with player's position being a pitcher
                    output = analyzers_obj.getHighlightTypeFromHighlightTexts(test_data[i], test_data[i], PLAYER_POSITIONS.pitcher);
                    expect(output).toEqual(false);
                }
            });
        });
    });

    describe("function highlightTeamIdsContainsPlayerTeamId()", function (){
        describe("should return false", function (){
            it("team_id parameter is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.highlightTeamIdsContainsPlayerTeamId([1, 2], test_data[i]);

                    expect(output).toEqual(false);
                }
            });

            it("highlight_team_ids parameter is invalid", function (){
                let test_data = [false, null, undefined, 1234, "Test", {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.highlightTeamIdsContainsPlayerTeamId(test_data[i], 1234);

                    expect(output).toEqual(false);
                }
            });

            it("highlight_team_ids array does not contain the team_id parameter", function (){
                let output = analyzers_obj.highlightTeamIdsContainsPlayerTeamId([1, 2], 8);

                expect(output).toEqual(false);
            });
        });

        it("should return true", function (){
            let output = analyzers_obj.highlightTeamIdsContainsPlayerTeamId([1, 2], 2);

            expect(output).toEqual(true);
        });
    });

    describe("function normalizeString()", function (){
        it("should return a string without accents", function (){
            let test_data = "Test: éèêë àäââá îïîí çñó ÉÈÜÎ";

            let output = analyzers_obj.normalizeString(test_data);

            expect(output).toBe("test: eeee aaaaa iiii cno eeui");
        });
    });

    describe("function highlightDescriptionTextsContainPlayerName()", function (){
        beforeEach(function (){
            spyOn(analyzers_obj, "normalizeString");
        });

        describe("should return false", function (){
            afterEach(function (){
                analyzers_obj.normalizeString.calls.reset();
            });

            it("headline paramater is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.highlightDescriptionTextsContainPlayerName(test_data[i], "Test description", "Test");

                    expect(output).toEqual(false);
                }
            });

            it("description paramater is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.highlightDescriptionTextsContainPlayerName("Test headline", test_data[i], "Test");

                    expect(output).toEqual(false);
                }
            });

            it("name paramater is invalid", function (){
                let test_data = [false, null, undefined];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.highlightDescriptionTextsContainPlayerName("Test headline", "Test description", test_data[i]); 

                    expect(output).toEqual(false);
                }
            });

            it("headline does not contain the player's last name but the description does contain the full name", function (){
                analyzers_obj.normalizeString.and.returnValues("mike trout", "harper highlight", "mike trout highlight");

                let output = analyzers_obj.highlightDescriptionTextsContainPlayerName("Test headline", "Test description", "Test name"); 

                expect(output).toEqual(false);
                expect(analyzers_obj.normalizeString.calls.allArgs()).toEqual([["Test name"], ["Test headline"], ["Test description"]]);
            });

            it("headline contains the player's last name but the description doesn't contain the full name", function (){
                analyzers_obj.normalizeString.and.returnValues("mike trout", "trout highlight", "bryce harper highlight");

                let output = analyzers_obj.highlightDescriptionTextsContainPlayerName("Test headline", "Test description", "Test name"); 

                expect(output).toEqual(false);
                expect(analyzers_obj.normalizeString.calls.allArgs()).toEqual([["Test name"], ["Test headline"], ["Test description"]]);
            });
        });

        describe("returns true", function (){
            it("headline contains the player's last name and description contains the full name", function (){
                analyzers_obj.normalizeString.and.returnValues("mike trout", "trout highlight", "mike trout highlight");

                let output = analyzers_obj.highlightDescriptionTextsContainPlayerName("Test headline", "Test description", "Test name"); 

                expect(output).toEqual(true);
                expect(analyzers_obj.normalizeString.calls.allArgs()).toEqual([["Test name"], ["Test headline"], ["Test description"]]);
            });
        });
    });

    describe("function highlightBelongsToPlayer()", function (){
        beforeEach(function (){
            spyOn(analyzers_obj, "normalizeString");
            spyOn(analyzers_obj.validators, "objectHasRequiredKeys");
            spyOn(analyzers_obj, "playerNameAssociatedToHighlightKeywordsList");
        });

        afterEach(function (){
            analyzers_obj.normalizeString.calls.reset();
            analyzers_obj.validators.objectHasRequiredKeys.calls.reset();
            analyzers_obj.playerNameAssociatedToHighlightKeywordsList.calls.reset();
        });

        describe("should return false", function (){
            afterEach(function (){
                analyzers_obj.validators.objectHasRequiredKeys.calls.reset();
                analyzers_obj.playerNameAssociatedToHighlightKeywordsList.calls.reset();
            });

            it("function objectHasRequiredKeys() returns false", function (){
                let output;

                analyzers_obj.validators.objectHasRequiredKeys.and.returnValues(true, false);
                output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "Player");
                expect(output).toEqual(false);

                analyzers_obj.validators.objectHasRequiredKeys.and.returnValues(false, true);
                output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "Player");
                expect(output).toEqual(false);

                analyzers_obj.validators.objectHasRequiredKeys.and.returnValues(false, false);
                output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "Player");
                expect(output).toEqual(false);
            });        

            describe("function objectHasRequiredKeys() returns true", function (){
                beforeEach(function (){
                    analyzers_obj.validators.objectHasRequiredKeys.and.returnValue(true);

                    spyOn(analyzers_obj, "highlightTeamIdsContainsPlayerTeamId");
                    spyOn(analyzers_obj, "highlightDescriptionTextsContainPlayerName");
                }); 

                describe("function playerNameAssociatedToHighlightKeywordsList() returns false", function (){
                    beforeEach(function (){
                        analyzers_obj.playerNameAssociatedToHighlightKeywordsList.and.returnValue(false);
                    });

                    afterEach(function (){
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.calls.reset(); 
                        analyzers_obj.highlightDescriptionTextsContainPlayerName.calls.reset(); 
                    });

                    it("function highlightTeamIdsContainsPlayerTeamId() returns false", function(){
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(false);

                        let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "player");

                        expect(analyzers_obj.highlightTeamIdsContainsPlayerTeamId).toHaveBeenCalled();
                        expect(analyzers_obj.highlightDescriptionTextsContainPlayerName).not.toHaveBeenCalled();
                        expect(output).toEqual(false);
                    });

                    it("function highlightDescriptionTextsContainPlayerName() returns false", function(){
                        analyzers_obj.validators.objectHasRequiredKeys.and.returnValue(true);
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(true);
                        analyzers_obj.highlightDescriptionTextsContainPlayerName.and.returnValue(false);

                        let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "player");

                        expect(analyzers_obj.highlightTeamIdsContainsPlayerTeamId).toHaveBeenCalled();
                        expect(analyzers_obj.highlightDescriptionTextsContainPlayerName).toHaveBeenCalled();
                        expect(output).toEqual(false);
                    });
                });

                describe("function playerNameAssociatedToHighlightKeywordsList() returns a player name", function (){
                    beforeEach(function (){
                        analyzers_obj.playerNameAssociatedToHighlightKeywordsList.and.returnValue("Test player");
                    });

                    afterEach(function (){
                        analyzers_obj.normalizeString.calls.reset();
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.calls.reset(); 
                    });

                    it("function highlightTeamIdsContainsPlayerTeamId() returns false", function(){
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(false);

                        let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "player");

                        expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                        expect(analyzers_obj.highlightTeamIdsContainsPlayerTeamId).toHaveBeenCalled();
                        expect(analyzers_obj.highlightDescriptionTextsContainPlayerName).not.toHaveBeenCalled();
                        expect(output).toEqual(false);
                    });

                    it("the keywords asociated player does not match", function(){
                        analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(true);
                        analyzers_obj.normalizeString.and.returnValue("Non matching player");

                        let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test team"], "player");

                        expect(analyzers_obj.normalizeString).toHaveBeenCalled();
                        expect(analyzers_obj.highlightTeamIdsContainsPlayerTeamId).toHaveBeenCalled();
                        expect(analyzers_obj.highlightDescriptionTextsContainPlayerName).not.toHaveBeenCalled();
                        expect(output).toEqual(false);
                    });
                });
            });
        });

        describe("should return true", function (){
            beforeEach(function (){
                spyOn(analyzers_obj, "highlightTeamIdsContainsPlayerTeamId");
                spyOn(analyzers_obj, "highlightDescriptionTextsContainPlayerName");
            });

            describe("function playerNameAssociatedToHighlightKeywordsList() returns false", function (){
                beforeEach(function (){
                    analyzers_obj.playerNameAssociatedToHighlightKeywordsList.and.returnValue(false);
                });

                it("functions highlightTeamIdsContainsPlayerTeamId() and highlightDescriptionTextsContainPlayerName() return true", function (){
                    analyzers_obj.validators.objectHasRequiredKeys.and.returnValue(true);
                    analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(true);
                    analyzers_obj.highlightDescriptionTextsContainPlayerName.and.returnValue(true);

                    let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test teams"], "player");
                    
                    expect(output).toEqual(true);
                    expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                });
            });

            describe("function playerNameAssociatedToHighlightKeywordsList() returns a player name", function (){
                beforeEach(function (){
                    analyzers_obj.playerNameAssociatedToHighlightKeywordsList.and.returnValue("Test player");
                });

                it("functions highlightTeamIdsContainsPlayerTeamId() returns true and player name matches the keywords player", function (){
                    analyzers_obj.validators.objectHasRequiredKeys.and.returnValue(true);
                    analyzers_obj.highlightTeamIdsContainsPlayerTeamId.and.returnValue(true);
                    analyzers_obj.normalizeString.and.returnValue("Test player")

                    let output = analyzers_obj.highlightBelongsToPlayer("highlight", ["Test teams"], "player");

                    expect(output).toEqual(true);
                    expect(analyzers_obj.highlightDescriptionTextsContainPlayerName).not.toHaveBeenCalled();
                });
            });
        });
    });

    describe("function playerNameAssociatedToHighlightKeywordsList()", function (){
        beforeEach(function (){
            spyOn(analyzers_obj, "normalizeString"); 
        });

        afterEach(function (){
            analyzers_obj.normalizeString.calls.reset();
        });

        describe("should return false", function (){
            it("highlight parameter is invalid", function (){
                let test_data = [false, true, undefined, null, "", "Test", 1234, [], ["Test"], {}, { key: "Test" }];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data[i]);

                    expect(output).toEqual(false);
                    expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                }
            });

            it("keywordsAll key is not an array", function (){
                let test_data = [{ keywordsAll: false }, { keywordsAll: "" }, { keywordsAll: 1234 }, { keywordsAll: { } }];

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data[i]);

                    expect(output).toEqual(false);
                    expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                }
            });

            it("keywordsAll key array is empty", function (){
                let test_data = { keywordsAll: [] };

                let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data);

                expect(output).toEqual(false);
                expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
            });

            it("keywordsAll key array values don't contain the type player_id", function (){
                let test_data = { keywordsAll: [{ type: "test" }, { key: "test" }, { type: "something" }] };

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data[i]);

                    expect(output).toEqual(false);
                    expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                }
            });

            it("keywordsAll key array values has type player_id but no displayName key", function (){
                let test_data = { keywordsAll: [{ type: "player_id" }, { type: "player_id", key: "test" }] };

                for (let i = 0; i < test_data.length; i++){
                    let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data[i]);

                    expect(output).toEqual(false);
                    expect(analyzers_obj.normalizeString).not.toHaveBeenCalled();
                }
            });
        });

        it("should return the player name", function (){
            analyzers_obj.normalizeString.and.returnValue("Test player");

            let test_data = { keywordsAll: [{ type: "player_id", displayName: "Test player" }] };

            let output = analyzers_obj.playerNameAssociatedToHighlightKeywordsList(test_data);

            expect(output).toBe("Test player");
            expect(analyzers_obj.normalizeString).toHaveBeenCalledWith("Test player");
        });
    });
});
