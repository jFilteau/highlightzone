#! /bin/bash

# Stashing changes that should not be tested
date=$(date +"%m-%d-%y")
message="pre-commit-$date"
git stash push --keep-index --include-untracked --quiet --message $message

reset_stash(){
    stashes=`git stash list`
    if [[ $stashes == *$message* ]];
    then
        git stash pop -q
    fi
}

# Run tests and make sure they pass
result=`karma start --single-run --log-level "disable" | grep -c "FAILED TESTS"`
if [ $result -gt 0 ];
then
    echo "Tests failed - Commit was aborted."
    reset_stash
    exit 1
else
    echo "Tests passed - Changes were committed."
    reset_stash
    exit 0
fi

