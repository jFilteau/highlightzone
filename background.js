import { ApiInitializer } from "./scripts/apiControllers/initializer.js";


chrome.runtime.onMessage.addListener(function (request, sender, sendResponse){
    let api_initializer = new ApiInitializer(request.players, sendResponse);
    api_initializer.init();

    // Keep the channel to the content script open (important since we're doing asynchronous calls above) 
    return true;
});
