# Highlightzone
This chrome extension adds highlight videos to the Yahoo! fantasy baseball team page. Highlights include hitting, pitching, defense and interviews with the option of disabling defense and/or interviews.  
  
The project structure:  

* Main scripts are in **scripts/**  
* Tests are all in **tests/** with file names ending in **.test.js**  
* Stylesheets are in **css/**  
* Data shared amongst scripts are in **constants/**  
* All data fetching is done in the background page via the **background.js** file  
* All DOM manipulation is done via the **content.js** file  
   
The required data is fetched from Major League Baseball's API **statsapi.com/api/v1/**.   
The API docs:  

* **http://statsapi-default-elb-prod-876255662.us-east-1.elb.amazonaws.com/docs/**  
* **https://github.com/bradleyhurley/PyBall/wiki/Implemented-Endpoints**  

# Development
## Dependencies
### Main

* [Webpack](https://github.com/webpack/webpack)
* [jQuery](https://github.com/jquery/jquery)
* [Magnific-Popup](https://github.com/dimsemenov/Magnific-Popup)
* [Moment.js](https://momentjs.com/)

### Testing

* [Karma](https://karma-runner.github.io/latest/index.html)
* [Jasmine](https://jasmine.github.io/)

### Browser

* [Google chrome](https://www.google.com/chrome/)

### Tools

* [npm](https://www.npmjs.com/)

## Cloning the repositiory

**HTTPS**   
```bash
git clone https://jFilteau@bitbucket.org/jFilteau/highlightzone.git
```
**SSH**   
```bash
git clone git@bitbucket.org:jFilteau/highlightzone.git
```

## Setup

### Installing dependencies
Navigate to the project directory and install the NPM dependencies by running
```bash
npm install
```

### Building the project
To build the project for development, run:
```bash
npm run build
```
  
To build the project for production, run:
```bash
npm run build:prod
```
   
The build, for both development and production, will create the files in the **dist/** directory.  
  
To automatically build the project on every code change and avoid having to run the build command everytime, run:  
```bash
npm run watch
```

And for production:   
```bash
npm run watch:prod
```

### Browser setup
To load the project into the chrome browser:  

* In the url, enter **chrome://extensions**  
* Enable the **Developper mode** button.  
* Click **Load unpacked**  
* Navigate to the project folder (**highlightzone/**) and select it. (**Note:** make sure you're in the folder that contains the **manifest.json** file)  
* Go to your Yahoo! fantasy team page.  

### Running tests
To run tests:
```bash
npm run test
```
  
This will open a new chrome tab for DOM manipulation testing purposes.  

### Packaging the project
To package the project, run:
```bash
npm run zip
```  
  
All zipped files will be found in the **archivedVersions/** directory.  

### Git hooks
The project has git hooks in the **hooks/** directory that can be added. To do so, **cd** into **.git/hooks/** and create a sysmlink pointing to the hook you want. (**Note:** make sure the path to the hook is relative to the **.git/hooks/** directory)  
   
e.g. If you want to create a symlink to the **hooks/pre-commit.sh** script, run from within the **.git/hooks/** directory:
```bash
ln -s ../../hooks/pre-commit.sh pre-commit
```
